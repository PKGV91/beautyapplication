//
//  AppDelegate.m
//  BeautyApp
//
//  Created by Pramod on 25/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"

@interface AppDelegate ()

@property (nonatomic, strong) REFrostedViewController *frostedViewController;
@property (nonatomic, strong) NSTimer *timer;
@end

@implementation AppDelegate

static AppDelegate *appDelegate = nil;
+ (AppDelegate *)sharedAppDelegate {
    if (!appDelegate) {
        appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    }
    return appDelegate;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [IQKeyboardManager sharedManager].enable = YES;
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    self.loadingView = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.loadingView.backgroundColor = [UIColor blackColor];
    self.loadingView.alpha = 0.4f;
    self.uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [[UITabBar appearance] setItemWidth:[UIScreen mainScreen].bounds.size.width/5];
    [[UITabBar appearance] setItemPositioning:UITabBarItemPositioningFill];
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    [self buildSideMenuViewController : 0];

    self.categoriesArray = @[];
    [self getAllPromotionsList];
    [self getAllCategoryLists];
    [application setStatusBarHidden:NO];
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    [self performSelector:@selector(loadSampleTemplates) withObject:nil afterDelay:10];
    [FIRApp configure];
    return YES;
}

- (void)getAllPromotionsList {
    [[APIWrappers sharedAPIWrapperInstance] getPromotionsListWithURLString:GET_ALL_PROMOTIONS_LIST withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        if (response) {
            self.promotionsArray = response;
        }
    }];
}

- (void)loadSampleTemplates {
    [[APIWrappers sharedAPIWrapperInstance] getSampleTemplatesListWithURLString:GET_SAMPLE_TEMPLATES withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        if (response) {
            self.sampleTemplatesArray = response;
        }
    }];
}
- (void)getAllCategoryLists {
    [[APIWrappers sharedAPIWrapperInstance] getCategoryListWithURLString:GET_ALL_CATEGORY withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        if (response) {
            self.categoriesArray = response;
        }
    }];
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
    UIApplication *app = [UIApplication sharedApplication];
    
    //create new uiBackgroundTask
    __block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
    
    //and create new timer with async call:
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //run function methodRunAfterBackground
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1800 target:self selector:@selector(methodRunAfterBackground) userInfo:nil repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
        [[NSRunLoop currentRunLoop] run];
    });
    
}

- (void)methodRunAfterBackground {
    [self.timer invalidate];
    self.timer = nil;
    exit(0);
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self.timer invalidate];
    self.timer = nil;
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (UIViewController *)buildViewController{
    
    LoginViewController *rootVC = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil ];
    return rootVC;
    // }
}
- (void)buildSideMenuViewController:(int)index{
    UIViewController *viewController = [self createTabbarWithIndex:index];
//    if ([[Constants sharedConstants] getstoredStringValuewithKey:USER_MOBILE_NUMBER].length > 0 && [[Constants sharedConstants] getstoredStringValuewithKey:USER_MOBILE_NUMBER].length >= 10) {
//        viewController = [self createTabbarWithIndex:index];
//        index = 1;
//    }else {
//        LoginViewController *firstVC = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
//        viewController = firstVC;
//    }
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    if (index == 0) {
        UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:viewController];
        navigation.navigationBarHidden = YES;
        self.window.rootViewController = navigation;
    }else {
        self.window.rootViewController = viewController;
    }
    [self.window makeKeyAndVisible];
}





- (void)showDummyForMe {
//    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
//    OurProductsController *firstVC = [[OurProductsController alloc] initWithNibName:@"OurProductsController" bundle:nil];
//    self.window.rootViewController = firstVC;
//    [self.window makeKeyAndVisible];
}



- (UIViewController *)createTabbarWithIndex:(int)index{
    
    //JobTabVC *jobTabObj = [[JobTabVC alloc] initWithNibName:@"JobTabVC" bundle:nil];
    //[self.window setRootViewController:jobTabObj];
    UIViewController *viewController;
    SideMenuController *menuVC = [[SideMenuController alloc] initWithNibName:@"SideMenuController" bundle:nil];
    HomeViewController *rootObj = [[HomeViewController  alloc]initWithNibName:@"HomeViewController" bundle:nil];
    rootObj.title = @"Home";
    self.frostedViewController = [[REFrostedViewController alloc] initWithContentViewController:rootObj menuViewController:menuVC];
    
    self.frostedViewController.direction = REFrostedViewControllerDirectionLeft;
    self.frostedViewController.liveBlurBackgroundStyle = REFrostedViewControllerLiveBackgroundStyleLight;
    self.frostedViewController.liveBlur = YES;
    
    UINavigationController *frostNavi = [[UINavigationController alloc] initWithRootViewController:self.frostedViewController];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
//    [jobWebNavi.navigationBar setBarTintColor:Dark_Blue_Color];
//    [jobWebNavi.navigationBar setTintColor:Dark_Blue_Color];
//    [jobWebNavi.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
//    [jobWebNavi.navigationBar setTranslucent:YES];
    frostNavi.navigationBarHidden = YES;
    
    AppointmentController *scheduling = [[AppointmentController alloc] initWithNibName:@"AppointmentController" bundle:nil];
    scheduling.title = @"Scheduling";
//    if (self.categoriesArray.count == 0) {
//        [self performSelector:@selector(getAppointmentController:) withObject:scheduling afterDelay:5];
//    }
    
    UINavigationController *scheduleNavi = [[UINavigationController alloc] initWithRootViewController:scheduling];
    scheduleNavi.navigationBarHidden = YES;
    
    PostPicController *postPic = [[PostPicController alloc] initWithNibName:@"PostPicController" bundle:nil];
    postPic.title = @"Post pic";
    UINavigationController *postPicNavi = [[UINavigationController alloc] initWithRootViewController:postPic];
    postPicNavi.navigationBarHidden = YES;
    
    PayBillController *payBill = [[PayBillController alloc] initWithNibName:@"PayBillController" bundle:nil];
    payBill.title = @"Membership";
    UINavigationController *payBillNavi = [[UINavigationController alloc] initWithRootViewController:payBill];
    payBillNavi.navigationBarHidden = YES;
    
    OfferViewController *ourProducts = [[OfferViewController alloc] initWithNibName:@"OfferViewController" bundle:nil];
    ourProducts.title = @"Offers";
    UINavigationController *ourProdNavi = [[UINavigationController alloc] initWithRootViewController:ourProducts];
    ourProdNavi.navigationBarHidden = YES;
    
    self.tabbarController = [[UITabBarController alloc] init];
    self.tabbarController.viewControllers = @[frostNavi,scheduleNavi,postPicNavi,payBillNavi,ourProdNavi];
    
    [self addImagesForTabbarController:self.tabbarController];
    self.tabbarController.delegate = (id<UITabBarControllerDelegate>)self;
    
    
    if (index == 0) {
        self.tabbarController.selectedIndex = 0;
    } else if (index == 1) {
        self.tabbarController.selectedIndex = 1;
    } else if (index == 2) {
        self.tabbarController.selectedIndex = 2;
    } else if (index == 3) {
        self.tabbarController.selectedIndex = 3;
    }else if (index == 4) {
        self.tabbarController.selectedIndex = 4;
    }
    viewController = self.tabbarController;
    
    CGRect tabFram1 = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width/5, 49);
    
    UIImageView *bgImage1 = [[UIImageView alloc] initWithFrame:tabFram1];
    bgImage1.backgroundColor = Dark_Blue_Color;
    bgImage1.tag = 1001;
    [self.tabbarController.tabBar insertSubview:bgImage1 atIndex:0];
    
    
    CGRect tabFram2 = tabFram1;
    tabFram2.origin.x = tabFram1.size.width+tabFram1.origin.x;
    
    UIImageView *bgImage2 = [[UIImageView alloc] initWithFrame:tabFram2];
    bgImage2.backgroundColor = Dark_Green_Color;
    bgImage2.tag = 1002;
    [self.tabbarController.tabBar insertSubview:bgImage2 atIndex:1];
    
    CGRect tabFram3 = tabFram2;
    tabFram3.origin.x = tabFram2.size.width+tabFram2.origin.x;
    
    UIImageView *bgImage3 = [[UIImageView alloc] initWithFrame:tabFram3];
    bgImage3.backgroundColor = Dark_Green_Color;
    bgImage3.tag = 1003;
    [self.tabbarController.tabBar insertSubview:bgImage3 atIndex:2];

    CGRect tabFram4 = tabFram3;
    tabFram4.origin.x = tabFram3.size.width+tabFram3.origin.x;
    
    UIImageView *bgImage4 = [[UIImageView alloc] initWithFrame:tabFram4];
    bgImage4.backgroundColor = Dark_Green_Color;
    bgImage4.tag = 1004;
    [self.tabbarController.tabBar insertSubview:bgImage4 atIndex:3];
    
    CGRect tabFram5 = tabFram4;
    tabFram5.origin.x = tabFram4.size.width+tabFram4.origin.x;
    
    UIImageView *bgImage5 = [[UIImageView alloc] initWithFrame:tabFram5];
    bgImage5.backgroundColor = Dark_Green_Color;
    bgImage5.tag = 1005;
    [self.tabbarController.tabBar insertSubview:bgImage5 atIndex:0];
    
    
    [self.tabbarController.tabBar.items enumerateObjectsUsingBlock:^(UITabBarItem *tabItem, NSUInteger idx, BOOL *stop) {
        
        if (idx == 0) {
            tabItem.title = @"Home";
        }
        
        [tabItem setTitlePositionAdjustment:UIOffsetMake(0, -2)];
        
        [tabItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],
                                          NSFontAttributeName:[UIFont fontWithName:@"Verdana" size:12.0]} forState:UIControlStateNormal];
        
        [tabItem setTitleTextAttributes:@{NSForegroundColorAttributeName:Light_Gray_Color,
                                          NSFontAttributeName:[UIFont fontWithName:@"Verdana" size:12.0]} forState:UIControlStateSelected];
        
    }];
    return self.tabbarController;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    
    [self.frostedViewController hideMenuViewController];
    if ([[(UINavigationController *)viewController topViewController] isKindOfClass:[REFrostedViewController class]]) {
        
        [self tabBarController:tabBarController selectAndDeselectTabColorsWithTag:1001];
        
    }else if ([[(UINavigationController *)viewController topViewController] isKindOfClass:[ShedulingViewController class]]) {
        
        [self tabBarController:tabBarController selectAndDeselectTabColorsWithTag:1002];
        
    }else if ([[(UINavigationController *)viewController topViewController] isKindOfClass:[PostPicController class]]) {
        
        [self tabBarController:tabBarController selectAndDeselectTabColorsWithTag:1003];
        
    }else if ([[(UINavigationController *)viewController topViewController] isKindOfClass:[PayBillController class]]) {
        
        [self tabBarController:tabBarController selectAndDeselectTabColorsWithTag:1004];
    }else if ([[(UINavigationController *)viewController topViewController] isKindOfClass:[OfferViewController class]]) {
        
        [self tabBarController:tabBarController selectAndDeselectTabColorsWithTag:1005];
    }
    
    return YES;
    
}


- (void)tabBarController:(UITabBarController *)tabBarController selectAndDeselectTabColorsWithTag:(int)tag {
    
    UIImageView *image1 = [tabBarController.tabBar viewWithTag:1001];
    image1.backgroundColor = tag == 1001 ? Dark_Blue_Color : Dark_Green_Color;
    
    UIImageView *image2 = [tabBarController.tabBar viewWithTag:1002];
    image2.backgroundColor = tag == 1002 ? Dark_Blue_Color : Dark_Green_Color;
    
    UIImageView *image3 = [tabBarController.tabBar viewWithTag:1003];
    image3.backgroundColor = tag == 1003 ? Dark_Blue_Color : Dark_Green_Color;
    
    UIImageView *image4 = [tabBarController.tabBar viewWithTag:1004];
    image4.backgroundColor = tag == 1004 ? Dark_Blue_Color : Dark_Green_Color;
    UIImageView *image5 = [tabBarController.tabBar viewWithTag:1005];
    image5.backgroundColor = tag == 1005 ? Dark_Blue_Color : Dark_Green_Color;
}

- (void)addImagesForTabbarController:(UITabBarController *)tabbarController {
    UITabBar *tabBar = tabbarController.tabBar;
    UITabBarItem *tabItem1 = [tabBar.items objectAtIndex:0];
    [tabItem1 setSelectedImage:[UIImage imageNamed:@"home"]];
    tabItem1.selectedImage = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabItem1.image = [[UIImage imageNamed:@"home"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UITabBarItem *tabItem2 = [tabBar.items objectAtIndex:1];
    [tabItem2 setSelectedImage:[UIImage imageNamed:@"scheduling"]];
    tabItem2.selectedImage = [[UIImage imageNamed:@"scheduling"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabItem2.image = [[UIImage imageNamed:@"scheduling"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UITabBarItem *tabItem3 = [tabBar.items objectAtIndex:2];
    [tabItem3 setSelectedImage:[UIImage imageNamed:@"camera"]];
    tabItem3.selectedImage = [[UIImage imageNamed:@"camera"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabItem3.image = [[UIImage imageNamed:@"camera"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UITabBarItem *tabItem4 = [tabBar.items objectAtIndex:3];
    [tabItem4 setSelectedImage:[UIImage imageNamed:@"membership"]];
    tabItem4.selectedImage = [[UIImage imageNamed:@"membership"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabItem4.image = [[UIImage imageNamed:@"membership"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UITabBarItem *tabItem5 = [tabBar.items objectAtIndex:4];
    [tabItem5 setSelectedImage:[UIImage imageNamed:@"cart"]];
    tabItem5.selectedImage = [[UIImage imageNamed:@"cart"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabItem5.image = [[UIImage imageNamed:@"cart"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
}

- (void)hideTabbar:(BOOL)boolValue {
    CGRect rect = CGRectZero;
    CGFloat tabBarHeight = self.tabbarController.tabBar.frame.size.height;
    if (boolValue) {
        rect = CGRectMake(0, [UIScreen mainScreen].bounds.size.height + tabBarHeight, [UIScreen mainScreen].bounds.size.width, tabBarHeight);
        [UIView animateWithDuration:0.5 animations:^{
            self.tabbarController.tabBar.frame = rect;
        }];
    }else {
        rect = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - tabBarHeight, [UIScreen mainScreen].bounds.size.width, tabBarHeight);
        [UIView animateWithDuration:0.5 animations:^{
            self.tabbarController.tabBar.frame = rect;
        }];
    }
    
}

- (void)getAppointmentController:(AppointmentController *)viewController {
    if (self.categoriesArray.count > 0) {
        viewController.greetingsArray = self.categoriesArray;
    }else {
        if (viewController) {
            [self getAppointmentController:viewController];
        }
    }
}


//MARK: LOGIN, SIGNUP, OTP CONTROLLERS
- (void)showLoginController {
    LoginViewController *login = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    login.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    login.signUPController = ^{
        [self showSignUpController];
    };
    login.loginOTPController = ^(NSString *mobileNumber) {
        [self showOTPControllerWithMobileNumber:mobileNumber fromType:@""];
    };
    dispatch_async(dispatch_get_main_queue(), ^{
        [ROOTVIEW presentViewController:login animated:NO completion:nil];
    });
}
- (void)showSignUpController {
    SignUpController *login = [[SignUpController alloc] initWithNibName:@"SignUpController" bundle:nil];
    login.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    login.signINController = ^{
        [self showLoginController];
    };
    login.otpController = ^(NSString *mobileNumber) {
        [self showOTPControllerWithMobileNumber:mobileNumber fromType:@"sign"];
    };
    dispatch_async(dispatch_get_main_queue(), ^{
        [ROOTVIEW presentViewController:login animated:NO completion:nil];
    });
}

- (void)showOTPControllerWithMobileNumber:(NSString *)number fromType:(NSString *)type{
    OTPController *login = [[OTPController alloc] initWithNibName:@"OTPController" bundle:nil];
    login.mobileNumber = number;
    login.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    login.dismissOTPController  = ^(NSDictionary *userDetails){
        [[Constants sharedConstants] storeDictionary:userDetails forKey:USER_MOBILE_NUMBER];
        [self.frostedViewController hideMenuViewController];
    };
    dispatch_async(dispatch_get_main_queue(), ^{
        [ROOTVIEW presentViewController:login animated:NO completion:nil];
    });
}


@end
/*
 
 */
