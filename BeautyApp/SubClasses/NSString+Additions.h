//
//  NSString+Additions.h
//  Rxpress
//
//  Created by way2online on 20/02/17.
//  Copyright © 2017 rxpress.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (BOOL)isEmailValidate;
- (BOOL)isValidateMobileNumber;

- (NSString *)convertingNumberToString;
@end
