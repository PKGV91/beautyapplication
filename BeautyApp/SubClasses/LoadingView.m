//
//  LoadingView.m
//  BeautyApp
//
//  Created by Pramod on 01/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView


- (void)drawRect:(CGRect)rect {
    
    
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self load];
    }
    return self;
}
    
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self load];
        
    }
    return self;
}
    
- (void)load {
    UIView *view = [[[NSBundle bundleForClass:[self class]] loadNibNamed:@"LoadingView" owner:self options:nil] firstObject];
    view.frame = self.bounds;
    view.backgroundColor = [UIColor clearColor];
    [self addSubview:view];
    [self setNeedsUpdateConstraints];
    [self setNeedsLayout];
    [self layoutIfNeeded];
}
- (void)setLoaderOnCurrentView {
    self.loaderIamge.hidden = NO;
    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2.0 * 6 * 1000];
    rotationAnimation.duration = 10000;
    [rotationAnimation setCumulative: YES];
    rotationAnimation.repeatCount = 5;
    [self.loaderIamge.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}
- (void)remveLoaderFromCurrentView {
    [self.loaderIamge.layer removeAnimationForKey:@"rotationAnimation"];
    self.loaderIamge.hidden = YES;
    [self removeFromSuperview];
}

@end
