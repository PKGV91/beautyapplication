//
//  NSString+Additions.m
//  Rxpress
//
//  Created by way2online on 20/02/17.
//  Copyright © 2017 rxpress.com. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (BOOL)isEmailValidate{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}
- (BOOL)isValidateMobileNumber{
    NSString *phoneRegex = @"^((0091)|(\\+91)|0?)[789]{1}\\d{9}$";//^\\+[1-9]{1}[0-9]{3,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:self];
    
}

- (NSString *)convertingNumberToString:(long)number {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    return [formatter stringFromNumber:[NSNumber numberWithLong:number]];
}
@end
//^((0091)|(\\+91)|0?)[789]{1}\\d{9}$
