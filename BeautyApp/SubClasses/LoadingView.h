//
//  LoadingView.h
//  BeautyApp
//
//  Created by Pramod on 01/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

@property (nonatomic, weak) IBOutlet UIImageView *loaderIamge;
    
    
- (id)initWithFrame:(CGRect)frame;
- (void)setLoaderOnCurrentView;
- (void)remveLoaderFromCurrentView;
@end
