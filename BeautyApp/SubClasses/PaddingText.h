//
//  PaddingText.h
//  GuardGrabber
//
//  Created by cbl73 on 9/6/16.
//  Copyright © 2016 cbl73. All rights reserved.


#import <UIKit/UIKit.h>

@protocol MyTextFieldDelegate <NSObject>
@optional
- (void)textFieldDidDelete;
@end

@interface PaddingText : UITextField

@property (nonatomic, assign) id<MyTextFieldDelegate> myDelegate;
@end
