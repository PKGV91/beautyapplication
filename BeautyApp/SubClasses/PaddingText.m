//
//  PaddingText.m
//  EyeSpot
//
//  Created by cbl73 on 12/06/16.
//  Copyright © 2016 DCC. All rights reserved.
//

#import "PaddingText.h"

@implementation PaddingText

/*
-(CGRect) textRectForBounds:(CGRect)bounds{
    
    return CGRectInset(bounds, 10, 0);
}
-(CGRect) editingRectForBounds:(CGRect)bounds{
    return  CGRectInset(bounds, 10, 0);
}*/

- (void)deleteBackward {
    BOOL shouldDismiss = [self.text length] == 0;
    
    [super deleteBackward];
    
    if (shouldDismiss) {
        if ([self.delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
            [self.delegate textField:self shouldChangeCharactersInRange:NSMakeRange(0, 0) replacementString:@""];
        }
    }
}

- (BOOL)keyboardInputShouldDelete:(UITextField *)textField {
    BOOL shouldDelete = YES;
    if ([UITextField instancesRespondToSelector:_cmd]) {
        BOOL (*keyboardInputShouldDelete)(id, SEL, UITextField *) = (BOOL (*)(id, SEL, UITextField *))[UITextField instanceMethodForSelector:_cmd];
        
        if (keyboardInputShouldDelete) {
            shouldDelete = keyboardInputShouldDelete(self, _cmd, textField);
        }
    }
    
    if (![textField.text length]) {
        [self deleteBackward];
    }
    return shouldDelete;
}
@end
