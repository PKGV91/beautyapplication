//
//  UIColor+_Additions.h
//  North
//
//  Created by way2online on 01/11/16.
//  Copyright © 2016 Way2Online. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (_Additions)


+ (UIColor *)colorFromHexString:(NSString *)hex;
@end
