//
//  UIView+Additions.m
//  EyeSpot
//
//  Created by administrator on 11/06/16.
//  Copyright © 2016 DCC. All rights reserved.
//

#import "UIView+Additions.h"

@implementation UIView (Additions)

@dynamic cornerRadius, borderColor, borderWidth, shadowColor, shadowOffset, shadowRadius, shadowOpacity, masksToBounds, degrees;

- (void)setBorderColor:(UIColor *)borderColor{
    [self.layer setBorderColor:borderColor.CGColor];
}

- (void)setBorderWidth:(CGFloat)borderWidth{
    [self.layer setBorderWidth:borderWidth];
}

- (void)setCornerRadius:(CGFloat)cornerRadius{
    [self.layer setCornerRadius:cornerRadius];
}

- (void)setShadowColor:(UIColor *)shadowColor{
    [self.layer setShadowColor:shadowColor.CGColor];
}

- (void)setShadowOpacity:(float)shadowOpacity{
    [self.layer setShadowOpacity:shadowOpacity];
}

- (void)setShadowOffset:(CGSize)shadowOffset{
    
    [self.layer setShadowOffset:CGSizeMake(shadowOffset.width, shadowOffset.height)];
}
- (void)setShadowRadius:(CGFloat)shadowRadius{
    [self.layer setShadowRadius:shadowRadius];
}
- (void)setMasksToBounds:(BOOL)masksToBounds{
    [self.layer setMasksToBounds:masksToBounds];
}

- (void)setStartColor:(UIColor *)startColor{
    [self.layer setBorderColor:startColor.CGColor];
}
- (void)setEndColor:(UIColor *)endColor{
    [self.layer setShadowColor:endColor.CGColor];
}

- (void)setDegrees:(CGFloat)degrees{
    self.transform = CGAffineTransformMakeRotation(degrees);
}

- (void)setTopRightLeftAndBottomRightLeftCornerRadiusForIsTop:(BOOL)isTop {
    UIBezierPath *maskPath;
    if (isTop) {
        
        maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                         byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                               cornerRadii:CGSizeMake(20, 20)];
    }else {
        maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                         byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                               cornerRadii:CGSizeMake(5.0, 5.0)];
    }
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shapelayer as the mask for the image view's layer
    self.layer.mask = maskLayer;
}
/*
 -(void)setShadowColorIB:(UIColor*)color
 {
 self.shadowColor = color.CGColor;
 }
 
 -(UIColor*)shadowColorIB
 {
 return [UIColor colorWithCGColor:self.shadowColor];
 }
 */
@end
