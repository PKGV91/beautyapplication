//
//  main.m
//  BeautyApp
//
//  Created by Pramod on 25/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
