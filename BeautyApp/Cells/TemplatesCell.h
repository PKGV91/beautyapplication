//
//  TemplatesCell.h
//  BeautyApp
//
//  Created by Pramod on 23/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>
#define TemplatesIdentifier @"TemplateIdentity"

@interface TemplatesCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *templateLabel;


+ (UINib *)nib;
@end
