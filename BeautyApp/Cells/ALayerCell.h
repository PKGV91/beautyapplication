//
//  ALayerCell.h
//  BeautyApp
//
//  Created by Pramod on 17/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>
#define ANewLayerCellIdentifier @"ANewLayerCellIdentity"
@interface ALayerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *itemTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemDateExpLabel;

+ (UINib *)nib;
@end
