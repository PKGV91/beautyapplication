//
//  BookTrnCell.h
//  BeautyApp
//
//  Created by Srikanthreddy on 17/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookTrnCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitel;
@property (strong, nonatomic) IBOutlet UILabel *lblAmountPaid;
@property (strong, nonatomic) IBOutlet UILabel *lblScheduled;
@property (strong, nonatomic) IBOutlet UILabel *lblPaid;
@property (strong, nonatomic) IBOutlet UILabel *lblBranch;
@property (strong, nonatomic) IBOutlet UILabel *lblInvoice;
@property (strong, nonatomic) IBOutlet UILabel *lblDiscount;

@end
