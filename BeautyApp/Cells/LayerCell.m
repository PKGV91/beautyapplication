//
//  LayerCell.m
//  BeautyApp
//
//  Created by Pramod on 16/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "LayerCell.h"

@implementation LayerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (UINib *)nib {
    return [UINib nibWithNibName:@"LayerCell" bundle:nil];
}
@end
