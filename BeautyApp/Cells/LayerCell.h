//
//  LayerCell.h
//  BeautyApp
//
//  Created by Pramod on 16/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>
#define LayerCellIdentifier @"LayerCellIdentity"

@interface LayerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *itemTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemDateExpLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemPriceLabel;

+ (UINib *)nib;
@end
