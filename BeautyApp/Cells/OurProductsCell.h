//
//  OurProductsCell.h
//  BeautyApp
//
//  Created by way2online on 18/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

#define OurProductCellIdentifier @"OurProductCellIdentity"
@interface OurProductsCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *productImageView;
@property (nonatomic, weak) IBOutlet UILabel *productNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *productPriceLabel;

@property (nonatomic, strong) NSLayoutConstraint *cellWidthConstraint;

+ (UINib *)nib;
//- (void)setCellWidth:(CGFloat)width;


@end
