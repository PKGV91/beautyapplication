//
//  BillingCell.h
//  BeautyApp
//
//  Created by Srikanthreddy on 17/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillingCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitel;
@property (strong, nonatomic) IBOutlet UILabel *lblAmountPaid;
@property (strong, nonatomic) IBOutlet UILabel *lbAvailed;
@property (strong, nonatomic) IBOutlet UILabel *lblPaid;
@property (strong, nonatomic) IBOutlet UILabel *lblBranch;
@property (strong, nonatomic) IBOutlet UILabel *lblInvoice;
@property (strong, nonatomic) IBOutlet UILabel *lblDiscount;
@property (strong, nonatomic) IBOutlet UILabel *lblStylist;

@end
