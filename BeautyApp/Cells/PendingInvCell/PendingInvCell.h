//
//  PendingInvCell.h
//  BeautyApp
//
//  Created by Srikanthreddy on 17/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PendingInvCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblDueAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblAvailedon;
@property (strong, nonatomic) IBOutlet UILabel *lblBranch;
@property (strong, nonatomic) IBOutlet UILabel *lblStylist;

@end
