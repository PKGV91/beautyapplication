//
//  ALayerCell.m
//  BeautyApp
//
//  Created by Pramod on 17/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "ALayerCell.h"

@implementation ALayerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+ (UINib *)nib {
    return [UINib nibWithNibName:@"ALayerCell" bundle:nil];
}
@end
