//
//  ServicesCell.m
//  BeautyApp
//
//  Created by Srikanthreddy on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "ServicesCell.h"

@implementation ServicesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (UINib *)nib {
    return [UINib nibWithNibName:@"ServicesCell" bundle:nil];
}
@end
