//
//  BookingCell.h
//  BeautyApp
//
//  Created by Srikanthreddy on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *dateStringLabel;
@end
