//
//  ServicesCell.h
//  BeautyApp
//
//  Created by Srikanthreddy on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

#define WallBAL_IDENITITY @"WallBAL_IDENITIFIER"
@interface ServicesCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *walletAmtLabel;
@property (nonatomic, weak) IBOutlet UILabel *walletBalLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *walletAmountTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *walletAmountHeightConstraint;

@property (nonatomic, weak) IBOutlet UIButton *walletButton;
+ (UINib *)nib;
@end
