//
//  OurProductsCell.m
//  BeautyApp
//
//  Created by way2online on 18/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "OurProductsCell.h"

@implementation OurProductsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.productNameLabel.preferredMaxLayoutWidth = 50;
//    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
//    if (@available(iOS 9.0, *)) {
//        self.cellWidthConstraint = [self.contentView.widthAnchor constraintEqualToConstant:0.f];
//    } else {
//        // Fallback on earlier versions
//    }
}

+ (UINib *)nib {
    return [UINib nibWithNibName:@"OurProductsCell" bundle:nil];
}

//- (void)setCellWidth:(CGFloat)width {
//    self.cellWidthConstraint.constant = width;
//    self.cellWidthConstraint.active = YES;
//}
//- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
//    UICollectionViewLayoutAttributes *attributes = [[super preferredLayoutAttributesFittingAttributes:layoutAttributes] copy];
//    return attributes;
//}


@end
