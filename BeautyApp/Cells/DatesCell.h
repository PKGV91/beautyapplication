//
//  DatesCell.h
//  BeautyApp
//
//  Created by Pramod on 03/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DaysIdentifier @"DaysIdentity"

@interface DatesCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *todayLabel;
@property (nonatomic, weak) IBOutlet UILabel *dayLabel;

+ (UINib *)nib;
@end
