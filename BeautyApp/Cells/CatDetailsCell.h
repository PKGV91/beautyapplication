//
//  CatDetailsCell.h
//  BeautyApp
//
//  Created by way2online on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CatCellIdentifier @"CatCellIdentity"


@interface CatDetailsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *subCatTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *subCatDescLabel;
@property (nonatomic, weak) IBOutlet UILabel *subCatPriceLabel;
@property (nonatomic, weak) IBOutlet UILabel *subCatTimeLabel;
@property (nonatomic, weak) IBOutlet UILabel *disPriceLabel;
@property (nonatomic, weak) IBOutlet UIImageView *pImageView;
@property (nonatomic, weak) IBOutlet UIButton *selectedButton;
@property (weak, nonatomic) IBOutlet UIButton *servicesButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *servicesButtonWidthConstraint;

+ (UINib *)nib;
@end
