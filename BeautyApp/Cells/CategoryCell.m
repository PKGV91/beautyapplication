//
//  CategoryCell.m
//  BeautyApp
//
//  Created by Pramod on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "CategoryCell.h"

@implementation CategoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (UINib *)nib{
    return [UINib nibWithNibName:@"CategoryCell" bundle:nil];
}

@end
