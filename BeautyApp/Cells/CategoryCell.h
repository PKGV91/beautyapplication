//
//  CategoryCell.h
//  BeautyApp
//
//  Created by Pramod on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CategoryCellIdenitifier @"CategoryCellIdentity"
@interface CategoryCell : UICollectionViewCell


@property (nonatomic, strong) IBOutlet UILabel *headingLabel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *headingLabelWidthConstraint;
+ (UINib *)nib;
@end
