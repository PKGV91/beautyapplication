//
//  MyNewTestiCell.h
//  BeautyApp
//
//  Created by way2online on 14/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>
#define MY_NEW_TESTMONIAL_IDENTITY @"MY_NEW_TESTMONIAL_IDENTIFIER"
@interface MyNewTestiCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *testimonialImageView;
@property (nonatomic, weak) IBOutlet UILabel *likesCountLabel;
@property (nonatomic, weak) IBOutlet UIButton *likesButton;
+ (UINib *)nib;
@end
