//
//  BeautyCell.h
//  BeautyApp
//
//  Created by Pramod on 04/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

#define BEAUTY_IDENTITY @"BEAUTY_IDENTIFIER"
@interface BeautyCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *beautyImage;
@property (strong, nonatomic) IBOutlet UILabel *lblBTitle;

+ (UINib *)nib;
@end
