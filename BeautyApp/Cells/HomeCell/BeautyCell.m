//
//  BeautyCell.m
//  BeautyApp
//
//  Created by Pramod on 04/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BeautyCell.h"

@implementation BeautyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (UINib *)nib {
    return [UINib nibWithNibName:@"BeautyCell" bundle:nil];
}
@end
