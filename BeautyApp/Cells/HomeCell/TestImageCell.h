//
//  TestImageCell.h
//  BeautyApp
//
//  Created by Pramod on 04/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>
#define TESTMONIAL_IDENTITY @"TESTMONIAL_IDENTIFIER"

@interface TestImageCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UIImageView *testimonialImageView;
@property (nonatomic, weak) IBOutlet UILabel *likesCountLabel;
@property (nonatomic, weak) IBOutlet UIButton *likesButton;
+ (UINib *)nib;
@end
