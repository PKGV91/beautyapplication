//
//  NewTestMonielCell.h
//  BeautyApp
//
//  Created by way2online on 08/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
//#import <UIImageView+WebCache.h>
#import "TestImageCell.h"
#import "BeautyCell.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "APIWrappers.h"


#define NEW_TEST_IDENTIFIER @"NEW_TEST_IDENTITY"


typedef void(^ShowBeautyTipsDetails)(NSDictionary *beautyDetails);
typedef void(^ShowTestimonialsDetails)(NSDictionary *testimonial);
@interface NewTestMonielCell : UITableViewCell<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIImageView *sideIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *headingTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewAllButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *collectionTestMonialArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewBottomConstraint;
@property (nonatomic, strong) NSString *fromType;
@property (nonatomic, strong) ShowBeautyTipsDetails showBeautyDetials;
@property (nonatomic, strong) ShowTestimonialsDetails showTestimonialDetials;
+ (UINib *)nib;
@end
