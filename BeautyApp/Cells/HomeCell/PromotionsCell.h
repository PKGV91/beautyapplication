//
//  PromotionsCell.h
//  BeautyApp
//
//  Created by Pramod on 07/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>
#define PromotionCellIdentifier @"PromotionCellIdentity"
@interface PromotionsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *promotionCellImageView;
@property (nonatomic, weak) IBOutlet UIButton *vieAllButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewAllTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewAllHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewAllBottomConstraint;
+ (UINib *)nib;
@end
