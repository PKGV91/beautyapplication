//
//  NewTestMonielCell.m
//  BeautyApp
//
//  Created by way2online on 08/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "NewTestMonielCell.h"

@implementation NewTestMonielCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.collectionView registerNib:[TestImageCell nib] forCellWithReuseIdentifier:TESTMONIAL_IDENTITY];
    [self.collectionView registerNib:[BeautyCell nib] forCellWithReuseIdentifier:BEAUTY_IDENTITY];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+ (UINib *)nib {
    return [UINib nibWithNibName:@"NewTestMonielCell" bundle:nil];
}

//MARK: - CollectionView Datasource & Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.collectionTestMonialArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell;
    
    if ([self.fromType isEqual:@"testM"]) {
        TestImageCell *tCell = [collectionView dequeueReusableCellWithReuseIdentifier:TESTMONIAL_IDENTITY forIndexPath:indexPath];
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [indicator startAnimating];
        [indicator setCenter:tCell.testimonialImageView.center];
        [tCell.testimonialImageView addSubview:indicator];
        NSURL *currentImageURL;
        NSString *currentImageSTR = self.collectionTestMonialArray[indexPath.row][@"after_img"];
        if ([currentImageSTR containsString:@"https://"]) {
            currentImageURL = [NSURL URLWithString:currentImageSTR];
        }else {
            currentImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:currentImageSTR]];
        }
        [tCell.testimonialImageView sd_setImageWithURL:currentImageURL placeholderImage:[UIImage imageNamed:@"splash2"] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
            if (!error) {
                [indicator removeFromSuperview];
                tCell.testimonialImageView.image = image;
                
            }else{
                tCell.testimonialImageView.image = [UIImage imageNamed:@"splash2"];
                [indicator removeFromSuperview];
            }
        }];

        tCell.likesCountLabel.text = [self.collectionTestMonialArray[indexPath.row][@"likes"] stringValue];
        tCell.likesButton.tag = indexPath.row;
        [tCell.likesButton addTarget:self action:@selector(likeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell = tCell;
    }else {
        BeautyCell *bCell = [collectionView dequeueReusableCellWithReuseIdentifier:BEAUTY_IDENTITY forIndexPath:indexPath];
        bCell.lblBTitle.text = self.collectionTestMonialArray[indexPath.row][@"tip_title"];
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [indicator startAnimating];
        [indicator setCenter:bCell.beautyImage.center];
        [bCell.beautyImage addSubview:indicator];
        NSURL *currentImageURL;
        NSString *currentImageSTR = self.collectionTestMonialArray[indexPath.row][@"tip_img"];
        if ([currentImageSTR containsString:@"https://"]) {
            currentImageURL = [NSURL URLWithString:currentImageSTR];
        }else {
            currentImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:currentImageSTR]];
        }
        [bCell.beautyImage sd_setImageWithURL:currentImageURL placeholderImage:[UIImage imageNamed:@"splash2"] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
            if (!error) {
                [indicator removeFromSuperview];
                bCell.beautyImage.image = image;
                
            }else{
                bCell.beautyImage.image = [UIImage imageNamed:@"splash2"];
                [indicator removeFromSuperview];
            }
        }];
        cell = bCell;
    }
    return cell;
}

- (void)likeButtonAction:(UIButton *)sender {
    if ([[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER]) {
        NSMutableDictionary *mutDic = [self.collectionTestMonialArray[sender.tag] mutableCopy];
        __block NSInteger likeCount = [mutDic[@"likes"] integerValue];
        if (![mutDic[@"user_id"] isKindOfClass:[NSNull class]]) {
            if ([[[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER][@"user_id"] integerValue] == [mutDic[@"user_id"] integerValue] ) {
               [self showAlertWithErrorTitle:@"" andWithErrorMessage:@"You already liked this post"];
                return;
            }
        }
        NSDictionary *params = @{@"user_id" : [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER][@"user_id"],
                                 @"testimonial_id" : mutDic[@"testimonial_id"]
                                 };
        [[AppDelegate sharedAppDelegate].window addSubview:[AppDelegate sharedAppDelegate].loadingView];
        [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
        [[APIWrappers sharedAPIWrapperInstance] userLikeTestimonialWithURLString:TESTIMONIAL_LIKE_UPDATE withParameters:params withComplitionBlock:^(NSDictionary *response, NSString *errorStr) {
            [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
            if (response) {
                
                likeCount = likeCount + 1;
                NSNumber *number = [NSNumber numberWithInteger:likeCount];
                [mutDic setValue:number forKey:@"likes"];
                [mutDic setValue:[[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER][@"user_id"] forKey:@"user_id"];
                [self.collectionTestMonialArray replaceObjectAtIndex:sender.tag withObject:mutDic];
                [self.collectionView reloadData];
                [self showAlertWithErrorTitle:@"" andWithErrorMessage:response[@"message"]];
            }else {
                [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
            }
        }];
    }else {
        [self showAlertWithErrorTitle:@"" andWithErrorMessage:@"Please login"];
    }
}

- (NSInteger)getLineNumberOfLabel:(UILabel *)label {
    NSInteger lineCount = 0;
    CGSize textSize = CGSizeMake(label.frame.size.width, MAXFLOAT);
    int rHeight = lroundf([label sizeThatFits:textSize].height);
    int charSize = lroundf(label.font.lineHeight);
    lineCount = rHeight/charSize;
    return lineCount;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.fromType isEqual:@"testM"]) {
        self.showTestimonialDetials(self.collectionTestMonialArray[indexPath.row]);
    }else {
        self.showBeautyDetials(self.collectionTestMonialArray[indexPath.row]);
    }
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.fromType isEqual:@"testM"]) {
        return CGSizeMake(120.0f, 150.f);
    }else {
        return CGSizeMake(92, 120);
    }
    return CGSizeZero;
}

- (void)showAlertWithErrorTitle:(NSString *)title andWithErrorMessage:(NSString *)errorMsg {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertController animated:YES completion:nil];
}
@end
