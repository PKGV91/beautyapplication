//
//  PromotionsCell.m
//  BeautyApp
//
//  Created by Pramod on 07/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "PromotionsCell.h"

@implementation PromotionsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (UINib *)nib {
    return [UINib nibWithNibName:@"PromotionsCell" bundle:nil];
}

@end
