//
//  OursServicesCell.h
//  BeautyApp
//
//  Created by Pramod on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

#define OursCellIdentifier @"OursCellIdentity"

@interface OursServicesCell : UICollectionViewCell
@property (nonatomic, weak) IBOutlet UILabel *categoryTitleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *categoryImageView;
+ (UINib *)nib;
@end
