//
//  pDetailsCell.m
//  BeautyApp
//
//  Created by way2online on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "pDetailsCell.h"

@implementation pDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (UINib *)nib {
    return [UINib nibWithNibName:@"pDetailsCell" bundle:nil];
}
@end
