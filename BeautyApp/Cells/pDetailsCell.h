//
//  pDetailsCell.h
//  BeautyApp
//
//  Created by way2online on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

#define pDetailsIdentifier @"pDetailsIdentity"

@interface pDetailsCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UILabel *catTitleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *arrowImageView;


+ (UINib *)nib;
@end
