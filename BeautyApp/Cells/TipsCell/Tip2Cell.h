//
//  Tip2Cell.h
//  BeautyApp
//
//  Created by Pramod on 04/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Tip2Cell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *bg2Image;
@property (strong, nonatomic) IBOutlet UIImageView *tip2Image;
@property (strong, nonatomic) IBOutlet UILabel *lbl2Title;
@property (strong, nonatomic) IBOutlet UILabel *lbl2Desc;

@end
