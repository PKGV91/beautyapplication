//
//  TipsController.m
//  BeautyApp
//
//  Created by Pramod on 04/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "TipsController.h"

@interface TipsController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation TipsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_tableTips registerNib:[UINib nibWithNibName:@"TipCell" bundle:nil] forCellReuseIdentifier:@"TipCell"];
    [_tableTips registerNib:[UINib nibWithNibName:@"Tip2Cell" bundle:nil] forCellReuseIdentifier:@"Tip2Cell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:NO];
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[APIWrapper sharedAPIWrapper] getTipsWithURLString:GET_TIPS withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[AppDelegate sharedAppDelegate].loadingView removeFromSuperview];
            if (response) {
                self.tipssArray = response;
                [self.tableTips reloadData];
            }else {
                [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
            }
        });
    }];
}
#pragma mark - table Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _tipssArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row % 2 == 0) {
        TipCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TipCell"];
        
        cell.lblTitle.text = self.tipssArray[indexPath.row][@"tip_title"];
        cell.lblDesc.text = self.tipssArray[indexPath.row][@"tip_description"];
        [cell.bgImageView sd_setImageWithURL:[NSURL URLWithString:self.tipssArray[indexPath.row][@"tip_img"]] placeholderImage:[UIImage imageNamed:@""]];
        [cell.bgImageView sd_setImageWithURL:[NSURL URLWithString:self.tipssArray[indexPath.row][@"tip_video"]] placeholderImage:[UIImage imageNamed:@""]];
        return cell;
    }else {
        Tip2Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"Tip2Cell"];
        
        cell.lbl2Title.text = self.tipssArray[indexPath.row][@"tip_title"];
        cell.lbl2Desc.text = self.tipssArray[indexPath.row][@"tip_description"];
        [cell.bg2Image sd_setImageWithURL:[NSURL URLWithString:self.tipssArray[indexPath.row][@"tip_img"]] placeholderImage:[UIImage imageNamed:@""]];
        [cell.tip2Image sd_setImageWithURL:[NSURL URLWithString:self.tipssArray[indexPath.row][@"tip_video"]] placeholderImage:[UIImage imageNamed:@""]];
        return cell;
    }
    return nil;
}
@end
