//
//  TipsController.m
//  BeautyApp
//
//  Created by Pramod on 04/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "TipsController.h"

@interface TipsController ()

@end

@implementation TipsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSURL *currentImageURL;
    NSURL *videoImageURL;
    NSString *videoUrl = self.tipsDictionary[@"video_thumbnail"];
    NSString *imageUrl = self.tipsDictionary[@"tip_img"];
    self.videoImageView.image = [UIImage imageNamed:@"splash2"];
    if (videoUrl) {
        if ([videoUrl containsString:@"https://"]) {
            videoImageURL = [NSURL URLWithString:videoUrl];
        }else {
            videoImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:videoUrl]];
        }
        
        [self.videoImageView sd_setImageWithURL:videoImageURL placeholderImage:[UIImage imageNamed:@"splash2"] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
            if (!error) {
                self.videoImageView.image = image;
            }else{
                self.videoImageView.image = [UIImage imageNamed:@"splash2"];
            }
        }];
    }
    
    
    if ([imageUrl containsString:@"https://"]) {
        currentImageURL = [NSURL URLWithString:imageUrl];
    }else {
        currentImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:imageUrl]];
    }
    
    [self.imageImageView sd_setImageWithURL:currentImageURL placeholderImage:[UIImage imageNamed:@"splash2"] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
        if (!error) {
            self.imageImageView.image = image;
        }else{
            self.imageImageView.image = [UIImage imageNamed:@"splash2"];
        }
    }];
    self.beautyTitleLabel.text = self.tipsDictionary[@"tip_title"];
    self.videoDescLabel.text = self.tipsDictionary[@"tip_description"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];

}

- (IBAction)backBtnTapped:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}

- (IBAction)videoPlayButtonAction:(id)sender {
    if (![self.tipsDictionary[@"tip_video"] isEqual:@""]) {
        NSString *vidoURL = self.tipsDictionary[@"tip_video"];
        if (![vidoURL containsString:@"https://"]) {
            vidoURL = [BASE_IMAGE_URL stringByAppendingString:vidoURL];
        }
        // grab a local URL to our video
        vidoURL = [vidoURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *videoURL = [NSURL URLWithString:vidoURL];
        
        // create an AVPlayer
        AVPlayer *player = [AVPlayer playerWithURL:videoURL];
        
        // create a player view controller
        AVPlayerViewController *controller = [[AVPlayerViewController alloc]init];
        controller.player = player;
        [player play];
        
        // show the view controller
        [self presentViewController:controller animated:YES completion:nil];
    }else {
        [self showAlertWithErrorTitle:@"" andWithErrorMessage:@"No video available to play"];
    }
}
@end
