//
//  TipsController.h
//  BeautyApp
//
//  Created by Pramod on 04/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Constants.h"

@interface TipsController : BaseViewController


@property (nonatomic, strong) NSDictionary *tipsDictionary;

@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *imageImageView;
@property (weak, nonatomic) IBOutlet UILabel *videoDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *beautyTitleLabel;
@end
