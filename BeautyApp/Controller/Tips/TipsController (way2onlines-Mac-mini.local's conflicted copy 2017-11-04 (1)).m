//
//  TipsController.m
//  BeautyApp
//
//  Created by Pramod on 04/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "TipsController.h"

@interface TipsController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation TipsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:NO];
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[APIWrapper sharedAPIWrapper] getTipsWithURLString:Ge withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
            if (response) {
                self.tipssArray = response;
                [self.tableTips reloadData];
            }else {
                [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
            }
        });
    }];
}
#pragma mark - table Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TipCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TipCell"];
    return cell;
    
}
@end
