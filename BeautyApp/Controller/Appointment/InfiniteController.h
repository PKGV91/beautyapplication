//
//  InfiniteController.h
//  BeautyApp
//
//  Created by way2online on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "pDetailsCell.h"
#import "CatDetailsCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
//#import <UIImageView+WebCache.h>
#import "AppDelegate.h"

@interface InfiniteController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *greetingsTableObj;
@property (nonatomic, strong) NSArray *greetingsArrayObj;
@property (assign, nonatomic) NSInteger index;
@property (nonatomic, strong) NSString *selectedString;
@property (nonatomic, assign) NSInteger isSelectedRow;
@property (nonatomic, strong) NSArray *listArray;
@property (nonatomic, strong) NSMutableArray *selectedArray;
@property (nonatomic, strong) NSMutableArray *indexPathsArray;
@property (nonatomic, strong) NSString *orderID;
@property (nonatomic, assign) NSInteger selectedNewIndex;
@property (nonatomic, strong) NSArray *packagesArray;

@property (nonatomic, weak) IBOutlet UILabel *priceServiceLabel;

@end
