//
//  AppointmentController.m
//  BeautyApp
//
//  Created by Pramod on 01/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "AppointmentController.h"

@interface AppointmentController ()

@end

@implementation AppointmentController
int presentIndex;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.horizotalCollection registerNib:[CategoryCell nib] forCellWithReuseIdentifier:CategoryCellIdenitifier];
    
    self.underLineView = [[UIView alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(proceedAction:) name:@"PROCEEDACTION" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectedIndexDictionary:) name:@"SELECTED_DICTIONARY" object:nil];
    self.selectedArray = [[NSMutableArray alloc] init];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.myIndex = [NSIndexPath indexPathForRow:0 inSection:0];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];
    if (self.greetingsArray.count == 0) {
        [self getAllCategoryLists];
    }else {
        [self pageViewControllerSetup];
    }
    [self.horizotalCollection selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.isNotProceedCalled = NO;
    if (self.greetingsArray.count == 0) {
        self.greetingsArray = [AppDelegate sharedAppDelegate].categoriesArray;
    }
}
- (void)getAllCategoryLists {
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] getCategoryListWithURLString:GET_ALL_CATEGORY withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.greetingsArray = response;
            [self.horizotalCollection reloadData];
            [self pageViewControllerSetup];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}
- (void)pageViewControllerSetup {
    
    self.pageController.view.frame = CGRectMake(0, 124, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 124);
    self.pageController.view.autoresizesSubviews = YES;
    InfiniteController *greetings = [self viewControllerAtIndex:0];
    if ([self.fromType isEqual:@"package"]) {
        greetings.packagesArray = self.packagesArray;
    }
    [self.pageController setViewControllers:@[greetings] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    [self.horizotalCollection reloadData];
//    for (UIScrollView *view in self.pageController.view.subviews) {
//        if ([view isKindOfClass:[UIScrollView class]]) {
//            view.scrollEnabled = NO;
//        }
//    }
    [self addChildViewController:self.pageController];
    [self.view addSubview:self.pageController.view];
    [self.pageController didMoveToParentViewController:self];
}
- (void)proceedAction:(NSNotification *)notification {
    if ([notification.object isEqual:@"error"]) {
        [self showAlertWithErrorTitle:@"" andWithErrorMessage:@"Please select any one to proceed"];
        return;
    }
    if (!self.isNotProceedCalled) {
        self.isNotProceedCalled = YES;
        NSArray *pricesArray;
        if ([notification.object count] == 3) {
            pricesArray = [notification.object[2] componentsSeparatedByString:@"₹ "];
        }else if ([notification.object count] == 2) {
            pricesArray = [notification.object[1] componentsSeparatedByString:@"₹ "];
        }
        NSDictionary *dic = [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER];
            if (dic) {
                NSDictionary *dictionary = @{@"order_id" : self.orderID.length == 0 ? @"0" : self.orderID,
                                             @"user_id" : dic[@"user_id"],
                                             @"services" : self.selectedArray,
                                             @"packages" : self.packagesArray.count > 0 ? self.packagesArray : @[],
                                             @"products" : @[],
                                             @"order_date" : [self getDateString],
                                             @"branch_id" : @"1",
                                             @"coupon_id" : @"",
                                             @"payable_amount" : [pricesArray lastObject]
                                             };
                [self webServicesCallWithDictionary:[dictionary mutableCopy]];
                
            }else {
                [[AppDelegate sharedAppDelegate] showLoginController];
                return;
            }
    }
}

- (NSString *)getDateString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    return [formatter stringFromDate:[NSDate date]];
}

- (double)getFinalAmountDiscount {
    int disCountAmount = 0;
    if (self.packagesArray.count > 0) {
        for (NSDictionary *dic in self.packagesArray) {
            
            disCountAmount = disCountAmount + [dic[@"package_on_other_services"] intValue];
        }
    }
    
    return disCountAmount/100.0;
}
- (void)webServicesCallWithDictionary:(NSMutableDictionary *)dictionary {
    if ([self.fromType isEqual:@"package"]) {
        [dictionary setValue:[NSString stringWithFormat:@"%.1f", [self getFinalAmountDiscount]] forKey:@"discount_amount"];
    }else {
        [dictionary setValue:@"0" forKey:@"discount_amount"];
    }
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] confirmBookingWithURLString:POST_CONFIRM_BOOKING withParameters:dictionary withComplitionBlock:^(NSDictionary *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.orderID = [NSString stringWithFormat:@"%@", response[@"order_id"]];
            [self showNavigationController];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}

- (void)showNavigationController {
    UIViewController *viewController;
    if ([self.fromType isEqual:@"package"]) {
        self.isNotProceedCalled = YES;
        ConfirmController *confirm = [[ConfirmController alloc] initWithNibName:@"ConfirmController" bundle:nil];
        confirm.servicesArray = self.selectedArray;
        confirm.packagesArray = self.packagesArray;
        confirm.orderID = self.orderID;
        viewController = confirm;
    }else if (!self.isNotProceedCalled){
        DateTimeController *dateTime = [[DateTimeController alloc] initWithNibName:@"DateTimeController" bundle:nil];
        dateTime.servicesArray = self.selectedArray;
        dateTime.orderID = self.orderID;
        viewController = dateTime;
    }
    [self.navigationController pushViewController:viewController animated:YES];
}
- (InfiniteController *)viewControllerAtIndex:(NSUInteger)index {
    
    InfiniteController *childViewController = [[InfiniteController alloc] initWithNibName:@"InfiniteController" bundle:nil];
    if (self.greetingsArray.count > 0) {
        childViewController.orderID = [NSString stringWithFormat:@"%@", [self.greetingsArray objectAtIndex:index][@"cat_id"]];
    }else {
        childViewController.greetingsArrayObj = @[];
    }
    if ([self.fromType isEqual:@"package"]) {
        childViewController.packagesArray = self.packagesArray;
    }
    childViewController.index = index;
    childViewController.indexPathsArray = self.selectedArray;
    childViewController.view.frame = self.pageController.view.frame;
    return childViewController;
}

#pragma mark CollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.greetingsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CategoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CategoryCellIdenitifier forIndexPath:indexPath];
    cell.headingLabel.text = [[self.greetingsArray objectAtIndex:indexPath.row][@"category_name"] uppercaseString];
    cell.headingLabelWidthConstraint.constant = [self widthForMenuTitle:[[self.greetingsArray objectAtIndex:indexPath.row][@"category_name"] uppercaseString] buttonEdgeInsets:kDefaultEdgeInsets];
    cell.headingLabel.tag = indexPath.row;
    [[cell.contentView  viewWithTag:100] removeFromSuperview];
    if (self.myIndex.row == indexPath.row) {
        cell.headingLabel.textColor = [UIColor blackColor];
        cell.headingLabel.font = [UIFont fontWithName:@"Verdana" size:16.0];
        self.underLineView.tag = 100;
        self.underLineView.frame = CGRectMake(0, cell.headingLabel.frame.origin.y + cell.headingLabel.frame.size.height + 2,  cell.headingLabelWidthConstraint.constant + 20, 3);
        self.underLineView.backgroundColor = [UIColor redColor];
        [cell.contentView addSubview:self.underLineView];
        
    }else{
        cell.headingLabel.textColor = [UIColor colorWithRed:190/255.0f green:190/255.0f blue:190/255.0f alpha:1];
        cell.headingLabel.font = [UIFont fontWithName:@"Verdana" size:16.0];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *widthString = [[self.greetingsArray objectAtIndex:indexPath.row][@"category_name"] uppercaseString];
    CGFloat width = [self widthForMenuTitle:widthString buttonEdgeInsets:kDefaultEdgeInsets];
    return CGSizeMake(width + 20, 44);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.myIndex == indexPath) {
        return;
    }
    self.myIndex = indexPath;
    [self.horizotalCollection scrollToItemAtIndexPath:indexPath
                                     atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    [self.horizotalCollection reloadData];
    InfiniteController *initialViewController = [self viewControllerAtIndex:indexPath.row];
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    
    initialViewController.orderID = [NSString stringWithFormat:@"%ld", [[self.greetingsArray objectAtIndex:indexPath.row][@"cat_id"] integerValue]];
    if (presentIndex < indexPath.row){
        [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
    else{
        [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    }
    
    presentIndex = (int)indexPath.row;
}
- (IBAction)backBtnTapped:(id)sender {
    if ([self.fromType isEqual:@"package"] || [self.fromType isEqual:@"service"]) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
    if (completed){
        NSUInteger currentIndex = [[self.pageController.viewControllers lastObject] index];
        self.myIndex = [NSIndexPath indexPathForRow:currentIndex inSection:0];
        [self.horizotalCollection selectItemAtIndexPath:self.myIndex animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
        [self.horizotalCollection reloadData];
        presentIndex = (int)currentIndex;
    }
}

#pragma mark Get Height String
- (CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGSize size = CGSizeZero;
    if (text) {
        //iOS 7
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:font } context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 1);
    }
    return size;
}

- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets{
    
    CGSize size = [title sizeWithAttributes:
                   @{NSFontAttributeName: [UIFont systemFontOfSize:18.0f]}];
    
    return CGSizeMake(size.width + buttonEdgeInsets.left + buttonEdgeInsets.right, size.height + buttonEdgeInsets.top + buttonEdgeInsets.bottom).width;
}


#pragma Pagecontroller Delegates

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(InfiniteController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(InfiniteController *)viewController index];
    
    index++;
    
    if (index == [self.greetingsArray count]) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

- (void)selectedIndexDictionary:(NSNotification *)notification {
    if (![self.selectedArray containsObject:notification.object]) {
        [self.selectedArray insertObject:notification.object atIndex:0];
    }else {
        [self.selectedArray removeObject:notification.object];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SHOW_LABEL_STRING" object:self.selectedArray];
}
@end
