//
//  AppointmentController.h
//  BeautyApp
//
//  Created by Pramod on 01/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryCell.h"
#import "InfiniteController.h"
#import "DateTimeController.h"
#import "BaseViewController.h"

#define kDefaultEdgeInsets UIEdgeInsetsMake(6, 12, 6, 12)

@interface AppointmentController : BaseViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIPageViewControllerDataSource,UIPageViewControllerDelegate>



@property (nonatomic, strong) IBOutlet UICollectionView *horizotalCollection;
@property (nonatomic, strong) UIView *underLineView;
@property (nonatomic, strong) NSIndexPath *myIndex;
@property (nonatomic, strong) NSArray *greetingsArray;
@property (nonatomic, strong) NSString *categoryID;
@property (nonatomic, strong) IBOutlet UIPageViewController *pageController;
@property (nonatomic, strong) NSString *fromType;
@property (nonatomic, strong) NSMutableArray *selectedArray;
@property (nonatomic, assign) BOOL isFromAssign;
@property (nonatomic, strong) NSArray *packagesArray;
@property (nonatomic, assign) BOOL isNotProceedCalled;
@property (nonatomic, strong) NSString *orderID;
@end
