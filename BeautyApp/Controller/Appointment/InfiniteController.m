//
//  InfiniteController.m
//  BeautyApp
//
//  Created by way2online on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "InfiniteController.h"

@interface InfiniteController ()

@end

@implementation InfiniteController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.greetingsTableObj registerNib:[pDetailsCell nib] forCellReuseIdentifier:pDetailsIdentifier];
    [self.greetingsTableObj registerNib:[CatDetailsCell nib] forCellReuseIdentifier:CatCellIdentifier];
    self.selectedArray = [[NSMutableArray alloc] init];
    self.greetingsTableObj.estimatedRowHeight = 100;
    self.greetingsTableObj.rowHeight = UITableViewAutomaticDimension;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showServiceStringNotification:) name:@"SHOW_LABEL_STRING" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    self.listArray = @[[[_selectedString uppercaseString] length] > 0 ? [_selectedString uppercaseString] : @"TEST"];
//    for (int i=0; i<[self.listArray count]; i++) {
//        [self.selectedArray addObject:[NSNumber numberWithBool:NO]];
//    }
    [self showServiceString];
    [self getSubCategoryListWithCategoryID:_orderID];
    
}

- (void)showServiceStringNotification:(NSNotification *)notification {
    _indexPathsArray = notification.object;
    [self showServiceString];
}
- (void)showServiceString {
    NSString *labelString;
    if (_indexPathsArray.count > 0 && self.packagesArray.count == 0) {
        NSInteger i = 0;
        for (NSDictionary *dic in self.indexPathsArray) {
            NSArray *priceArray = [dic[@"service_price"] componentsSeparatedByString:@" "];
            i = i + [priceArray[0] integerValue];
        }
        
        labelString = [NSString stringWithFormat:@"%d Services ₹ %ld", (int)_indexPathsArray.count, (long)i];
    }else if(self.indexPathsArray.count > 0 && self.packagesArray.count > 0) {
        NSInteger i = 0;
        for (NSDictionary *dic in self.indexPathsArray) {
            NSArray *priceArray = [dic[@"service_price"] componentsSeparatedByString:@" "];
            i = i + [priceArray[0] intValue];
        }
        
        NSInteger j = 0;
        double disCountAmount = 0;
        for (NSDictionary *dic in self.packagesArray) {
            NSArray *priceArray = [dic[@"package_price"] componentsSeparatedByString:@" "];
            j = j + [priceArray[0] intValue];
            disCountAmount = disCountAmount + [dic[@"package_on_other_services"] intValue];
        }
        double discount = i * (disCountAmount/100.0);
        
        i = i - discount;
        labelString = [NSString stringWithFormat:@"%d pakcges+%d services ₹ %ld", (int)self.packagesArray.count, (int)self.indexPathsArray.count, i+j];
    }else if (self.packagesArray.count > 0 && self.indexPathsArray.count == 0) {
        NSInteger j = 0;
        for (NSDictionary *dic in self.packagesArray) {
            NSArray *priceArray = [dic[@"package_price"] componentsSeparatedByString:@" "];
            j = j + [priceArray[0] intValue];
        }
        labelString = [NSString stringWithFormat:@"%d pakcges ₹ %ld", (int)self.packagesArray.count, (long)j];
    }else {
        labelString = @"Select Services";
    }
    self.priceServiceLabel.text = labelString;
}
- (void)getSubCategoryListWithCategoryID:(NSString *)categoryID {
    [[AppDelegate sharedAppDelegate].window addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] getSubCategoryListWithURLString:GET_ALL_SUB_CATEGORY withParameters:@{@"cat_id" : categoryID} withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.listArray = response;
            for (int i=0; i<[self.listArray count]; i++) {
                [self.selectedArray addObject:[NSNumber numberWithBool:NO]];
            }
            [self.greetingsTableObj reloadData];
        }else {
//            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}
#pragma mark Tableview Datasource & Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([[self.selectedArray objectAtIndex:section] boolValue]) {
        NSArray *array = self.listArray[section][@"services"];
        return array.count;
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
   /* if (indexPath.row == 0) {
        
        pDetailsCell *pCell = (pDetailsCell *)[tableView dequeueReusableCellWithIdentifier:pDetailsIdentifier];
        pCell.catTitleLabel.text = self.listArray[indexPath.section];
        pCell.arrowImageView.image = [UIImage imageNamed:@""];
        cell = pCell;
        
    }else if (indexPath.section == 0) {
        
        CatDetailsCell *dCell = (CatDetailsCell *)[tableView dequeueReusableCellWithIdentifier:CatCellIdentifier];
        NSDictionary *dictionary = self.greetingsArrayObj[indexPath.row];
        
        dCell.subCatTitleLabel.text = dictionary[@"service_name"];
        dCell.subCatDescLabel.text = dictionary[@"service_description"];
        dCell.subCatPriceLabel.text = dictionary[@"service_price"];
        dCell.subCatTimeLabel.text = dictionary[@"service_duration"];
        [dCell.pImageView sd_setImageWithURL:[NSURL URLWithString:dictionary[@"service_img"]] placeholderImage:[UIImage imageNamed:@""]];
        dCell.selectedButton.tag = indexPath.row;
        [dCell.selectedButton addTarget:self action:@selector(selectButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        if ([self.indexPathsArray containsObject:dictionary]) {
            dCell.selectedButton.selected = YES;
        }else {
            dCell.selectedButton.selected = NO;
        }
        cell = dCell;
        
    }else if (indexPath.section == 1) {
        
        
        //        FillData *fillObj = indexPath.section == 1 ? [self.direccionesArray objectAtIndex:(indexPath.row-1)] : [self.telefonosArray objectAtIndex:(indexPath.row-1)];
        //        AddressCell *aCell = (AddressCell *)[tableView dequeueReusableCellWithIdentifier:AddressCellIdentity];
        //        [aCell displayCellDataWith:fillObj withIdentifier:[self.listArray objectAtIndex:indexPath.section]];
        //        aCell.editCallBack = ^(NSString *identity,NSString *editStr,FillData *dataObj){
        //            self.selectedCellIdentity = identity;
        //            self.isNewOrEdit = editStr;
        //            self.editFillData = dataObj;
        //            [self addButtpnTapped];
        //        };
        //        cell = aCell;
    }
    */
    
    
    BOOL manyCells  = [[self.selectedArray objectAtIndex:indexPath.section] boolValue];
    
    /********** If the section supposed to be closed *******************/
    if(!manyCells) {
        pDetailsCell *pCell = (pDetailsCell *)[tableView dequeueReusableCellWithIdentifier:pDetailsIdentifier];
        pCell.catTitleLabel.text = self.listArray[indexPath.section][@"sub_category_name"];
        pCell.arrowImageView.image = [UIImage imageNamed:@""];
        cell = pCell;
    }
    /********** If the section supposed to be Opened *******************/
    else {
        CatDetailsCell *dCell = (CatDetailsCell *)[tableView dequeueReusableCellWithIdentifier:CatCellIdentifier];
        NSArray *array = self.listArray[indexPath.section][@"services"];
        NSDictionary *dictionary = array[indexPath.row];
        dCell.subCatTitleLabel.text = dictionary[@"service_name"];
        dCell.subCatDescLabel.text = dictionary[@"service_description"];
        dCell.subCatPriceLabel.text = dictionary[@"service_price"];
        dCell.subCatPriceLabel.textColor = [UIColor blackColor];
        dCell.subCatTimeLabel.text = [NSString stringWithFormat:@"Duration : %@",dictionary[@"service_duration"]];
        [dCell.pImageView sd_setImageWithURL:[NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:dictionary[@"service_img"]]] placeholderImage:[UIImage imageNamed:@""]];
        dCell.selectedButton.tag = indexPath.row;
        dCell.servicesButtonWidthConstraint.constant = 0;
        dCell.disPriceLabel.hidden = YES;
        if (self.packagesArray.count > 0) {
            dCell.disPriceLabel.hidden = NO;
            
            double disAmount = [dictionary[@"service_price"] doubleValue] * [self getFinalAmountDiscount];
            dCell.disPriceLabel.text = [NSString stringWithFormat:@"%.1f INR", [dictionary[@"service_price"] doubleValue] - disAmount];
            dCell.subCatPriceLabel.attributedText = [self getAttributedStringForGivenString:dCell.subCatPriceLabel.text withUILabel:dCell.subCatPriceLabel];
        }
        [dCell.selectedButton addTarget:self action:@selector(selectButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        if ([self.indexPathsArray containsObject:dictionary]) {
            dCell.selectedButton.selected = YES;
            [dCell.selectedButton setImage:[UIImage imageNamed:@"check-selected"] forState:UIControlStateNormal];
        }else {
            dCell.selectedButton.selected = NO;
            [dCell.selectedButton setImage:[UIImage imageNamed:@"check-box-empty"] forState:UIControlStateNormal];
        }
        cell = dCell;
    }
   
    return cell;
}

- (NSAttributedString *)getAttributedStringForGivenString:(NSString *)string withUILabel:(UILabel *)label{
    NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:string attributes:@{NSStrikethroughStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle], NSFontAttributeName :label.font}];
    return str2;
}
- (double)getFinalAmountDiscount {
    int disCountAmount = 0;
    for (NSDictionary *dic in self.packagesArray) {

        disCountAmount = disCountAmount + [dic[@"package_on_other_services"] intValue];
    }
    return disCountAmount/100.0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *array = self.listArray[indexPath.section][@"services"];
    NSDictionary *dictionary = array[indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SELECTED_DICTIONARY" object:dictionary];
    //    [_greetingsTableObj reloadData];
    //    if (indexPath.section) {
    //        [self.selectedArray replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
    
    [_greetingsTableObj reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    //    }
}

- (void)selectButtonAction:(UIButton *)sender {
    NSArray *array = self.listArray[self.selectedNewIndex][@"services"];
    NSDictionary *dictionary = array[sender.tag];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SELECTED_DICTIONARY" object:dictionary];
}
- (IBAction)proceedButtonAction:(id)sender {
    if (_indexPathsArray.count > 0 && self.packagesArray.count > 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PROCEEDACTION" object:@[_indexPathsArray, self.packagesArray,self.priceServiceLabel.text]];
    }if (_indexPathsArray.count > 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PROCEEDACTION" object:@[_indexPathsArray, self.priceServiceLabel.text]];
    }else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PROCEEDACTION" object:@"error"];
    }
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.listArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 48;
}

#pragma mark - Creating View for TableView Section

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,40)];
    sectionView.backgroundColor = [UIColor whiteColor];
    sectionView.tag = section;
    UILabel *viewLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, tableView.frame.size.width-10, 40)];
    viewLabel.backgroundColor = [UIColor clearColor];
    viewLabel.textColor = [UIColor blackColor];
    viewLabel.font = [UIFont fontWithName:@"Verdana" size:16.0];
    viewLabel.text = [self.listArray objectAtIndex:section][@"sub_category_name"];
    [sectionView addSubview:viewLabel];
    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, self.greetingsTableObj.frame.size.width, 0.75)];
    separatorLineView.backgroundColor = [UIColor lightGrayColor];
    [sectionView addSubview:separatorLineView];
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionView addGestureRecognizer:headerTapped];
    
    return  sectionView;
}

#pragma mark - Table header gesture tapped

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    self.selectedNewIndex = indexPath.section;
    if (indexPath.row == 0) {
        BOOL collapsed  = [[self.selectedArray objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[self.listArray count]; i++) {
            if (indexPath.section==i) {
                [self.selectedArray replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
        }
        [self.greetingsTableObj reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if ([[self.selectedArray objectAtIndex:indexPath.section] boolValue]) {
//        return 40;
//    }
//    return UITableViewAutomaticDimension;
//
//}

- (IBAction)selectedServicesList:(id)sender {
    if (_indexPathsArray.count > 0) {
        LayerController *login = [[LayerController alloc] initWithNibName:@"LayerController" bundle:nil];
        login.cellsArray = _indexPathsArray;
        login.typeOfController = FromTypeAppointment;
        login.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        dispatch_async(dispatch_get_main_queue(), ^{
            [ROOTVIEW presentViewController:login animated:NO completion:nil];
        });
    }
}
@end
