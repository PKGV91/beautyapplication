//
//  OurProductsController.m
//  BeautyApp
//
//  Created by way2online on 18/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "OurProductsController.h"

@interface OurProductsController ()

@end

@implementation OurProductsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.collectionView registerNib:[OurProductsCell nib] forCellWithReuseIdentifier:OurProductCellIdentifier];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];
    [self getAllProductsList];
}

- (void)getAllProductsList {
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] getAllProductListWithURLString:GET_ALL_PRODUCT_LIST withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.productsArray = response;
            [self.collectionView reloadData];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}
#pragma mark - CollectionView Methods


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.isFromSearch) {
        return self.searchArray.count;
    }
    return self.productsArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    OurProductsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:OurProductCellIdentifier forIndexPath:indexPath];
    NSDictionary *dictionary;
    if (self.isFromSearch) {
        dictionary = self.searchArray[indexPath.row];
    }else {
        dictionary = self.productsArray[indexPath.row];
    }
    NSURL *currentImageURL;
    NSString *currentImageSTR = dictionary[@"product_img"];
    if ([currentImageSTR containsString:@"https://"]) {
        currentImageURL = [NSURL URLWithString:currentImageSTR];
    }else {
        currentImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:currentImageSTR]];
    }
    [cell.productImageView sd_setImageWithURL:currentImageURL placeholderImage:[UIImage imageNamed:@"2"]];
    cell.productNameLabel.text = dictionary[@"product_name"];
    cell.productPriceLabel.text = [NSString stringWithFormat:@"₹ %@",dictionary[@"product_price"]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellWidth = (collectionView.frame.size.width) / 2.f;
    return CGSizeMake(cellWidth, cellWidth + 20);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    OurProductsCell *cell = (OurProductsCell *)[collectionView cellForItemAtIndexPath:indexPath];
    DetailProductController *dProduct = [[DetailProductController alloc] initWithNibName:@"DetailProductController" bundle:nil];
    NSDictionary *dictionary;
    if (self.isFromSearch) {
        dictionary = self.searchArray[indexPath.row];
    }else {
        dictionary = self.productsArray[indexPath.row];
    }
    dProduct.productDict = dictionary;
    dProduct.imageProduct = cell.productImageView.image;
    [self.navigationController pushViewController:dProduct animated:YES];
}

//MARK: - Button Actions
- (IBAction)backBtnTapped:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}

//MARK: - TextField Delegates

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *search = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (search.length > 0) {
        self.isFromSearch = YES;
    }else {
        self.isFromSearch = NO;
    }
    [self getAllSearchResultWithString:search];
    return YES;
}

- (void)getAllSearchResultWithString:(NSString *)searchStr {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"product_name CONTAINS[C] %@", searchStr, searchStr];
    self.searchArray = [self.productsArray filteredArrayUsingPredicate:predicate];
    [self.collectionView reloadData];
}
@end
