//
//  DetailProductController.m
//  BeautyApp
//
//  Created by way2online on 13/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "DetailProductController.h"

@interface DetailProductController ()

@end

@implementation DetailProductController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.productInfoLabel.text = self.productDict[@"product_description"];
    self.productImageView.image = self.imageProduct;
    self.productNameLabel.text = self.productDict[@"product_name"];
    self.productPriceLabel.text = [NSString stringWithFormat:@"Rs. %@", self.productDict[@"product_price"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Button Actions

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)addToCartAction:(id)sender {
}

- (IBAction)typeButtonSelectedAction:(UIButton *)sender {
    
    if (sender.tag == 1) {
        //share
    }else if (sender.tag == 2) {
        //similer
    }else if (sender.tag == 3) {
        //whishlist
    }
}

@end
