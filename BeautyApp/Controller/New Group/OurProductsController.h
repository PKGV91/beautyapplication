//
//  OurProductsController.h
//  BeautyApp
//
//  Created by way2online on 18/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "OurProductsCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppDelegate.h"
#import "DetailProductController.h"


@interface OurProductsController : BaseViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSArray *productsArray;
@property (nonatomic, assign) BOOL isFromSearch;
@property (nonatomic, strong) NSArray *searchArray;
@end
