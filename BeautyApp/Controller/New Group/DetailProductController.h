//
//  DetailProductController.h
//  BeautyApp
//
//  Created by way2online on 13/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"

@interface DetailProductController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratReviewsLabel;
@property (weak, nonatomic) IBOutlet UILabel *productInfoLabel;

@property (nonatomic, strong) NSDictionary *productDict;
@property (nonatomic, strong) UIImage *imageProduct;
@end
