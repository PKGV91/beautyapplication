//
//  NotificationController.m
//  BeautyApp
//
//  Created by Srikanthreddy on 23/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "NotificationController.h"
#import "AppDelegate.h"
@interface NotificationController ()

@end

@implementation NotificationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];
}

#pragma mark ButtonActions

- (IBAction)sideMenuButtonAction:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}
@end
