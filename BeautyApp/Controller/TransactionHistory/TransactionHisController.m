//
//  TransactionHisController.m
//  BeautyApp
//
//  Created by Srikanthreddy on 23/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "TransactionHisController.h"

@interface TransactionHisController ()
{
    CGRect frameRect;
}


@end

@implementation TransactionHisController
int presentmyIndex;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.underLineView = [[UILabel alloc] init];
    frameRect = CGRectMake(0, 106, [UIScreen mainScreen].bounds.size.width / 3, 2);
    self.underLineView.frame = frameRect;
    [self.view addSubview:self.underLineView];
    self.underLineView.backgroundColor = [UIColor redColor];
    presentmyIndex = 0;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
        [self pageViewControllerSetup];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];
}
- (void)pageViewControllerSetup {
   
    self.pageController.view.frame = CGRectMake(0, 108, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 108);
    self.pageController.view.autoresizesSubviews = YES;
   BillingController *billing = [self viewControllerAtIndex:0];
    [self.pageController setViewControllers:@[billing] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    for (UIScrollView *view in self.pageController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.scrollEnabled = NO;
        }
    }
    [self addChildViewController:self.pageController];
    [self.view addSubview:self.pageController.view];
    [self.pageController didMoveToParentViewController:self];
}
- (BillingController *)viewControllerAtIndex:(NSUInteger)index {
    
    BillingController *childViewController = [[BillingController alloc] initWithNibName:@"BillingController" bundle:nil];
    
    if (index == 0) {
        
    }else if(index ==1) {
        
    }else if (index ==2){
        
    }
    childViewController.index = index;
    childViewController.view.frame = self.pageController.view.frame;
    return childViewController;
}



#pragma Pagecontroller Delegates

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(TestimonialController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(BillingController *)viewController index];
    
    index++;
    
    if (index >= 1) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

#pragma mark ButtonActions

- (IBAction)BillingButtonActions:(UIButton *)sender {
    if (presentmyIndex == (int)sender.tag) {
        return;
    }
    frameRect.origin.x = sender.frame.origin.x;
    self.underLineView.frame = frameRect;
    
    BillingController *initialViewController = [self viewControllerAtIndex:sender.tag];
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    if (presentmyIndex < sender.tag){
        [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
    else{
        [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    }
    
    presentmyIndex = (int)sender.tag;
}

- (IBAction)sideMenuButtonAction:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}
@end
