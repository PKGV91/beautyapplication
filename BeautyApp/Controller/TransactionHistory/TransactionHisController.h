//
//  TransactionHisController.h
//  BeautyApp
//
//  Created by Srikanthreddy on 23/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "BillingController.h"
#import "BookingsController.h"
#import "PendingInvoiceController.h"
#import "AppDelegate.h"

@interface TransactionHisController : BaseViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate>
@property (nonatomic, strong) UILabel *underLineView;
@property (nonatomic, strong) IBOutlet UIPageViewController *pageController;

@end
