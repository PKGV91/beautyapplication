//
//  PicController.h
//  BeautyApp
//
//  Created by Srikanthreddy on 17/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^SendImageToBack)(UIImage *image);

@interface PicController : BaseViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *uploadedImageView;

@property (assign, nonatomic) NSInteger index;
@property (nonatomic, strong) SendImageToBack backImageBlock;


@property (nonatomic, strong) UIImage *uploadedImage;
@end
