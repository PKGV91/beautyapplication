//
//  TemplatesController.m
//  BeautyApp
//
//  Created by Pramod on 23/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "TemplatesController.h"

@interface TemplatesController ()

@end

@implementation TemplatesController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.templatesTable registerNib:[TemplatesCell nib] forCellReuseIdentifier:TemplatesIdentifier];
    self.templatesTable.estimatedRowHeight = 100;
    self.templatesTable.rowHeight = UITableViewAutomaticDimension;
    if ([AppDelegate sharedAppDelegate].sampleTemplatesArray.count == 0) {
        [self loadSampleTemplates];
        return;
    }
    self.templatesArray = [AppDelegate sharedAppDelegate].sampleTemplatesArray;
}

- (void)loadSampleTemplates {
    [[APIWrappers sharedAPIWrapperInstance] getSampleTemplatesListWithURLString:GET_SAMPLE_TEMPLATES withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        if (response) {
            self.templatesArray = response;
            [self.templatesTable reloadData];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelButtonAction:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.templatesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TemplatesCell *tCell = [tableView dequeueReusableCellWithIdentifier:TemplatesIdentifier];
    tCell.templateLabel.text = self.templatesArray[indexPath.row][@"description"];
    return tCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectTemp(self.templatesArray[indexPath.row][@"description"]);
    [self cancelButtonAction:nil];
}
@end
