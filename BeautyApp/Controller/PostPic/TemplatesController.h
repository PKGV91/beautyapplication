//
//  TemplatesController.h
//  BeautyApp
//
//  Created by Pramod on 23/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"
#import "TemplatesCell.h"


typedef void(^SelectedTemplate)(NSString *templates);
@interface TemplatesController : BaseViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *templatesTable;
@property (nonatomic, strong) NSArray *templatesArray;

@property (nonatomic, strong) SelectedTemplate selectTemp;
@end
