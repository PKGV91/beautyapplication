//
//  PostVideoController.h
//  BeautyApp
//
//  Created by Srikanthreddy on 17/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
typedef void(^SendVideoToBack)(UIImage *image, NSData *videoData, NSURL *vdURL);

@interface PostVideoController : BaseViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (assign, nonatomic) NSInteger index;

@property (weak, nonatomic) IBOutlet UIImageView *uploadedVideoImageView;
@property (nonatomic, strong) UIImage *uploadedVideoImage;
@property (nonatomic, strong) NSURL *videoURL;
@property (nonatomic, strong) SendVideoToBack backVideoDataBlock;
@property (weak, nonatomic) IBOutlet UIButton *uploadButton1;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *uploadButton2;
@end
