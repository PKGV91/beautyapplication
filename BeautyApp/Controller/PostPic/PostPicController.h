//
//  PostPicController.h
//  BeautyApp
//
//  Created by Pramod on 23/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "PostVideoController.h"
#import "PicController.h"
#import "AppDelegate.h"
#import "TemplatesController.h"

@import FirebaseStorage;
@interface PostPicController : BaseViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate, UITextViewDelegate>

@property (nonatomic, strong) UILabel *underLineView;
@property (nonatomic, strong) IBOutlet UIPageViewController *pageController;
@property (nonatomic, assign) BOOL isAlreadyLoaded;
@property (nonatomic, assign) BOOL isThatImage;

@property (nonatomic, strong) UIImage *uploadImage;
@property (nonatomic, strong) UIImage *uploadVideoImage;
@property (nonatomic, strong) NSString *uploadImageURL;
@property (nonatomic, strong) NSString *uploadVideoURL;
@property (nonatomic, strong) NSString *uploadVThumpURL;
@property (nonatomic, strong) NSURL *cvdURL;


@property (weak, nonatomic) IBOutlet UIButton *enterDescButton;
@property (weak, nonatomic) IBOutlet UITextView *enterDescTextView;

@property (nonatomic, strong) IBOutlet UIView *uploadDescView;
@property (nonatomic, strong) NSData *videoFullData;
@end
