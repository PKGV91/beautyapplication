//
//  PostVideoController.m
//  BeautyApp
//
//  Created by Srikanthreddy on 17/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "PostVideoController.h"
#import "SDAVAssetExportSession.h"
@interface PostVideoController ()

@end

@implementation PostVideoController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (self.uploadedVideoImage && self.videoURL) {
        self.uploadedVideoImageView.image = self.uploadedVideoImage;
        self.uploadButton1.hidden = YES;
        self.uploadButton2.hidden = NO;
        self.playButton.hidden = NO;
        self.uploadedVideoImageView.backgroundColor = [UIColor blackColor];
    }else {
        self.uploadButton1.hidden = NO;
        self.uploadButton2.hidden = YES;
        self.playButton.hidden = YES;
        self.uploadedVideoImageView.backgroundColor = [UIColor whiteColor];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//

- (IBAction)uploadImageAction:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.videoMaximumDuration = 30;
        picker.delegate = self;
        picker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
        picker.videoQuality = UIImagePickerControllerQualityTypeHigh;
        [self presentViewController:picker animated:YES completion:nil];
    }else {
        [self showAlertWithErrorTitle:@"" andWithErrorMessage:@"Device does not have Camera"];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
   self.videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
    NSData *data_Page = [NSData dataWithContentsOfURL:self.videoURL];
    self.uploadedVideoImageView.image = [self setVideoImageWithURL:_videoURL];
    self.uploadButton1.hidden = YES;
    self.uploadButton2.hidden = NO;
    self.playButton.hidden = NO;
    self.uploadedVideoImageView.backgroundColor = [UIColor blackColor];
    self.backVideoDataBlock(self.uploadedVideoImageView.image, data_Page, self.videoURL);
   /* NSString *itemPathString = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:@"sbb.mov"];
    // NSURL *itemPathURL = [NSURL URLWithString:itemPathString];
    
    
    NSURL *itemPathURL = [NSURL fileURLWithPath:itemPathString];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:itemPathString]){
        
        NSLog(@"File Exists");
    }
    
    
    AVAsset *videoAsset = (AVAsset *)[AVAsset assetWithURL:_videoURL];
    SDAVAssetExportSession *encoder = [SDAVAssetExportSession.alloc initWithAsset:videoAsset];
    encoder.outputFileType = AVFileTypeMPEG4;
    encoder.outputURL = itemPathURL;
    encoder.videoSettings = @{
    AVVideoCodecKey: AVVideoCodecH264,
    AVVideoWidthKey: @720,
    AVVideoHeightKey: @480,
    AVVideoCompressionPropertiesKey: @
        {
        AVVideoAverageBitRateKey: @3000000,
        AVVideoProfileLevelKey: AVVideoProfileLevelH264High40,
        },
    };
    encoder.audioSettings = @
    {
    AVFormatIDKey: @(kAudioFormatMPEG4AAC),
    AVNumberOfChannelsKey: @2,
    AVSampleRateKey: @44100,
    AVEncoderBitRateKey: @128000,
    };
    
    [encoder exportAsynchronouslyWithCompletionHandler:^
     {
         if (encoder.status == AVAssetExportSessionStatusCompleted) {
             NSLog(@"Video export succeeded");
             _videoURL = itemPathURL;
             NSData *data_Page = [NSData dataWithContentsOfURL:self.videoURL];
             self.backVideoDataBlock(self.uploadedVideoImageView.image, data_Page, self.videoURL);
             
         }
         else if (encoder.status == AVAssetExportSessionStatusCancelled) {
             NSLog(@"Video export cancelled");
         }
         else {
             NSLog(@"Video export failed with error: %@ (%ld)", encoder.error.localizedDescription, (long)encoder.error.code);
         }
     }];*/
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (UIImage *)setVideoImageWithURL:(NSURL *)url{
    AVAsset *avAsset = [AVURLAsset URLAssetWithURL:url options:nil];
    
    UIImage *thumbNailImage;
    if ([[avAsset tracksWithMediaType:AVMediaTypeVideo] count] > 0){
        AVAssetImageGenerator *imageGenerator =[AVAssetImageGenerator assetImageGeneratorWithAsset:avAsset];
        Float64 durationSeconds = CMTimeGetSeconds([avAsset duration]);
        CMTime midpoint = CMTimeMakeWithSeconds(durationSeconds/2.0, 600);
        NSError *error;
        CMTime actualTime;
        
        CGImageRef halfWayImage = [imageGenerator copyCGImageAtTime:kCMTimeZero actualTime:&actualTime error:&error];
        
        if (halfWayImage != NULL)
        {
            
            NSString *actualTimeString = (NSString *)CFBridgingRelease(CMTimeCopyDescription(NULL, actualTime));
            NSString *requestedTimeString = (NSString *)CFBridgingRelease(CMTimeCopyDescription(NULL, midpoint));
            NSLog(@"Got halfWayImage: Asked for %@, got %@", requestedTimeString, actualTimeString);
            
            thumbNailImage = [UIImage imageWithCGImage:halfWayImage];
            
        }
        
    }
//    thumbNailImage = [self scaleAndRotateImage:thumbNailImage];
    return thumbNailImage;
}


- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    int kMaxResolution = [UIScreen mainScreen].bounds.size.width; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

- (IBAction)playButtonAction:(id)sender {
    
    
    // create an AVPlayer
    AVPlayer *player = [AVPlayer playerWithURL:self.videoURL];
    
    // create a player view controller
    AVPlayerViewController *controller = [[AVPlayerViewController alloc]init];
    controller.player = player;
    [player play];
    
    // show the view controller
    [self presentViewController:controller animated:YES completion:nil];
}

@end
