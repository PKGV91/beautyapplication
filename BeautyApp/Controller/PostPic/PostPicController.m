//
//  PostPicController.m
//  BeautyApp
//
//  Created by Pramod on 23/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "PostPicController.h"

@interface PostPicController () {
    CGRect frameRect;
}


@end

@implementation PostPicController
int presentPostIndex;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.underLineView = [[UILabel alloc] init];
    frameRect = CGRectMake(0, 106, [UIScreen mainScreen].bounds.size.width / 2, 2);
    self.underLineView.frame = frameRect;
    [self.view addSubview:self.underLineView];
    self.underLineView.backgroundColor = [UIColor redColor];
    presentPostIndex = 0;
    self.uploadDescView.frame = [UIScreen mainScreen].bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!self.isAlreadyLoaded) {
        [[AppDelegate sharedAppDelegate] hideTabbar:YES];
        self.isAlreadyLoaded = YES;
        [self pageViewControllerSetup];
    }
}
- (void)pageViewControllerSetup {
    
    self.pageController.view.frame = CGRectMake(0, 108, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 108);
    self.pageController.view.autoresizesSubviews = YES;
    PostVideoController *greetings = (PostVideoController *)[self viewControllerAtIndex:0];
    [self.pageController setViewControllers:@[greetings] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    for (UIScrollView *view in self.pageController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.scrollEnabled = NO;
        }
    }
    [self addChildViewController:self.pageController];
    [self.view addSubview:self.pageController.view];
    [self.pageController didMoveToParentViewController:self];
}
- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {
    if (index == 0) {
        PostVideoController *childViewController = [[PostVideoController alloc] initWithNibName:@"PostVideoController" bundle:nil];
        childViewController.uploadedVideoImage = self.uploadVideoImage;
        childViewController.videoURL = self.cvdURL;
        self.isThatImage = NO;
        childViewController.backVideoDataBlock = ^(UIImage *image, NSData *videoData, NSURL *vdURL) {
            self.uploadVideoImage = image;
            self.videoFullData = videoData;
            self.cvdURL = vdURL;
            [self saveVideoToFirebase];
            [self saveThumbNailImages];
        };
        
        childViewController.index = index;
        childViewController.view.frame = self.pageController.view.frame;
        return childViewController;
    }else {
        self.isThatImage = YES;
        PicController *childViewController = [[PicController alloc] initWithNibName:@"PicController" bundle:nil];
        childViewController.uploadedImage = self.uploadImage;
        childViewController.backImageBlock = ^(UIImage *image) {
            self.uploadImage = image;
            [self uploadSelectedImageToFirebase];
        };
        childViewController.index = index;
        childViewController.view.frame = self.pageController.view.frame;
        return childViewController;
    }
    return nil;
}
//MARK: - TextView Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqual:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (textView.text.length == 0) {
        self.enterDescButton.hidden = NO;
    }else {
        self.enterDescButton.hidden = YES;
    }
}
//MARK: - ButtonActions


- (IBAction)selectTemplatesAction:(id)sender {
    TemplatesController *login = [[TemplatesController alloc] initWithNibName:@"TemplatesController" bundle:nil];
    login.selectTemp = ^(NSString *templates) {
        self.enterDescTextView.text = templates;
        [self textViewDidEndEditing:self.enterDescTextView];
    };
    login.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    dispatch_async(dispatch_get_main_queue(), ^{
        [ROOTVIEW presentViewController:login animated:NO completion:nil];
    });
}

- (IBAction)uploadActionImageVideo:(id)sender {
    [self.view addSubview:self.uploadDescView];
}
- (IBAction)backAction:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}

- (IBAction)uploadServiceAction:(UIButton *)sender {
    [self.uploadDescView removeFromSuperview];
    if (![[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER]) {
        [[AppDelegate sharedAppDelegate] showLoginController];
        return;
    }
    if (sender.tag == 1) {
        if (self.enterDescTextView.text.length > 0) {
//            "user_id:1
//        :Description saneepd dkjfhgkdfg dfgkjdjfk gdfkjgh
//        :http://pahttoimg.com/path
//        :http://pahttoimg.com/path
//        :http://pahttoimg.com/path
//        title:title dhs fsjkdhfkjsdf,sdhfkjs
//        :2"
            NSString *userId = [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER][@"user_id"];
            NSDictionary *params = @{@"user_id" : userId,
                                     @"description" : self.enterDescTextView.text,
                                     @"before_img" : @"",
                                     @"after_img" : self.uploadImageURL.length > 0 ? self.uploadImageURL : @"",
                                     @"video" : self.uploadVideoURL.length > 0 ? self.uploadVideoURL : @"",
                                     @"testimonial_id" : @"0",
                                     @"video_thumbnail" : self.uploadVThumpURL.length > 0 ? self.uploadVThumpURL : @""
                                     };
            [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
            [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
            [[APIWrappers sharedAPIWrapperInstance] postAddMyTestimonialListWithURLString:POST_ADD_TESTIMONIALS withParameters:params withComplitionBlock:^(NSDictionary *response, NSString *errorStr) {
                [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
                if (response) {
                    [self showAlertWithErrorTitle:@"" andWithErrorMessage:response[@"message"]];
                }else {
                    [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
                }
            }];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:@"Please enter description"];
        }
    }
}
- (IBAction)pleaseEnterDescAction:(id)sender {
    self.enterDescButton.hidden = YES;
    [self.enterDescTextView becomeFirstResponder];
}

- (void)uploadSelectedImageToFirebase {
    if (self.uploadImage ) {
        [[AppDelegate sharedAppDelegate].window addSubview:[AppDelegate sharedAppDelegate].loadingView];
        [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
        NSString *fileName = [NSString stringWithFormat:@"ShapeBrowBar/Images/ShapeBrowBar_%d.png",(int)[[NSDate date] timeIntervalSince1970]];
        FIRStorage *storage = [FIRStorage storage];
        FIRStorageReference *storageRef = [storage reference];
        
        // Data in memory
        NSData *data = UIImagePNGRepresentation(self.uploadImage);
        
        // Create a reference to the file you want to upload
        FIRStorageReference *riversRef = [storageRef child:fileName];
        
        
        FIRStorageUploadTask *uploadTask = [riversRef putData:data
                                                     metadata:nil
                                                   completion:^(FIRStorageMetadata *metadata,
                                                                NSError *error) {
                                                       [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
                                                       if (error != nil) {
                                                           // Uh-oh, an error occurred!
                                                           [self showAlertWithErrorTitle:@"" andWithErrorMessage:error.localizedDescription];
                                                       } else {
                                                           // Metadata contains file metadata such as size, content-type, and download URL.
                                                           self.uploadImageURL = metadata.downloadURL.absoluteString;
                                                       }
                                                   }];
    }
}

- (void)saveVideoToFirebase {
    
    if (self.videoFullData ) {
        [[AppDelegate sharedAppDelegate].window addSubview:[AppDelegate sharedAppDelegate].loadingView];
        [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
        NSString *fileName = [NSString stringWithFormat:@"ShapeBrowBar/Videos/ShapeBrowBar_%d.mp4",(int)[[NSDate date] timeIntervalSince1970]];
        FIRStorage *storage = [FIRStorage storage];
        FIRStorageReference *storageRef = [storage reference];
        
        // Data in memory
        NSData *data = UIImagePNGRepresentation(self.uploadImage);
        
        // Create a reference to the file you want to upload
        FIRStorageReference *riversRef = [storageRef child:fileName];
        
        
        FIRStorageUploadTask *uploadTask = [riversRef putData:self.videoFullData
                                                     metadata:nil
                                                   completion:^(FIRStorageMetadata *metadata,
                                                                NSError *error) {
                                                       [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
                                                       if (error != nil) {
                                                           // Uh-oh, an error occurred!
                                                           [self showAlertWithErrorTitle:@"" andWithErrorMessage:error.localizedDescription];
                                                       } else {
                                                           // Metadata contains file metadata such as size, content-type, and download URL.
                                                           self.uploadVideoURL = metadata.downloadURL.absoluteString;
                                                       }
                                                   }];
    }
    
}

- (void)saveThumbNailImages {
    
    if (self.uploadVideoImage ) {
        
        NSString *fileName = [NSString stringWithFormat:@"ShapeBrowBar/ThumbNail/ShapeBrowBar_%d.png",(int)[[NSDate date] timeIntervalSince1970]];
        FIRStorage *storage = [FIRStorage storage];
        FIRStorageReference *storageRef = [storage reference];
        
        // Data in memory
        NSData *data = UIImagePNGRepresentation(self.uploadVideoImage);
        
        // Create a reference to the file you want to upload
        FIRStorageReference *riversRef = [storageRef child:fileName];
        
        
        FIRStorageUploadTask *uploadTask = [riversRef putData:data
                                                     metadata:nil
                                                   completion:^(FIRStorageMetadata *metadata,
                                                                NSError *error) {
                                                       if (error != nil) {
                                                           // Uh-oh, an error occurred!
                                                           [self showAlertWithErrorTitle:@"" andWithErrorMessage:error.localizedDescription];
                                                       } else {
                                                           // Metadata contains file metadata such as size, content-type, and download URL.
                                                           self.uploadVThumpURL = metadata.downloadURL.absoluteString;
                                                       }
                                                   }];
    }
    
}
- (IBAction)PostButtonActions:(UIButton *)sender {
    if (presentPostIndex == (int)sender.tag) {
        return;
    }
    frameRect.origin.x = sender.frame.origin.x;
    self.underLineView.frame = frameRect;
    
    UIViewController *initialViewController = [self viewControllerAtIndex:sender.tag];
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    if (presentPostIndex < sender.tag){
        [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
    else{
        [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    }
    
    presentPostIndex = (int)sender.tag;
}

#pragma Pagecontroller Delegates

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(PostVideoController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(PostVideoController *)viewController index];
    
    index++;
    
    if (index >= 1) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}


@end
