//
//  OffersCell.m
//  BeautyApp
//
//  Created by pullayya Bandaru on 12/23/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "OffersCell.h"

@implementation OffersCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
