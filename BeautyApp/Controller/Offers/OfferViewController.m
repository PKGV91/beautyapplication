//
//  OfferViewController.m
//  BeautyApp
//
//  Created by pullayya Bandaru on 12/23/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "OfferViewController.h"
#import "OffersCell.h"
#import "AppDelegate.h"

@interface OfferViewController () <UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *offersArray;
}
@property (weak, nonatomic) IBOutlet UITableView *tableViewObj;
    

@end

@implementation OfferViewController
    
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableViewObj registerNib:[UINib nibWithNibName:@"OffersCell" bundle:nil] forCellReuseIdentifier:@"OffersCell"];
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];
    if ([AppDelegate sharedAppDelegate].promotionsArray.count == 0) {
        [self getAllPromotionsList];
    }else {
        offersArray = [[AppDelegate sharedAppDelegate].promotionsArray mutableCopy];
    }
}

- (void)getAllPromotionsList {
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] getPromotionsListWithURLString:GET_ALL_PROMOTIONS_LIST withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            offersArray = [response mutableCopy];
            [self.tableViewObj reloadData];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}
- (IBAction)backBtnTapped:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}
    
# pragma mark - Table Dalegates
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return offersArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OffersCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OffersCell"];
    NSDictionary *dict = [offersArray objectAtIndex:indexPath.row];
    NSURL *currentImageURL;
    NSString *currentImageSTR = dict[@"promotion_img"];
    if ([currentImageSTR containsString:@"https://"]) {
        currentImageURL = [NSURL URLWithString:currentImageSTR];
    }else {
        currentImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:currentImageSTR]];
    }
   [cell.offerImageObj sd_setImageWithURL:currentImageURL placeholderImage:[UIImage imageNamed:@"new-logo1"]];
    return cell;
}
    
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 200;
}
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *offerDic = offersArray[indexPath.row];
    if ([offerDic[@"promotion_for"] isEqual:@"Packages"]) {
        PackagesController *mainVC = [[PackagesController alloc] initWithNibName:@"PackagesController" bundle:nil];
        mainVC.fromType = @"";
        [self.navigationController pushViewController:mainVC animated:YES];
    }else {
        AppointmentController *appointment = [[AppointmentController alloc] initWithNibName:@"AppointmentController" bundle:nil];
        appointment.categoryID = @"1";//[NSString stringWithFormat:@"%d", (int)self.categoriesArray[indexPath.row][@"cat_id"]];
        appointment.fromType = @"service";
        appointment.greetingsArray = [AppDelegate sharedAppDelegate].categoriesArray;
        [self.navigationController pushViewController:appointment animated:YES];
    }
}
@end
