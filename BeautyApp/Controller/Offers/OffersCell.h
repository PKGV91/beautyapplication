//
//  OffersCell.h
//  BeautyApp
//
//  Created by pullayya Bandaru on 12/23/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *offerImageObj;
    
@end
