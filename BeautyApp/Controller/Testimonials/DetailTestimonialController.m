//
//  DetailTestimonialController.m
//  BeautyApp
//
//  Created by Pramod on 04/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "DetailTestimonialController.h"

@interface DetailTestimonialController ()


@end

@implementation DetailTestimonialController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.underLineView = [[UILabel alloc] init];
    frameRect = CGRectMake(0, 106, [UIScreen mainScreen].bounds.size.width / 2, 2);
    self.underLineView.frame = frameRect;
    [self.view addSubview:self.underLineView];
    self.underLineView.backgroundColor = [UIColor redColor];
    NSURL *currentImageURL;
    NSURL *videoImageURL;
    NSString *videoUrl = self.detailsDic[@"video_thumbnail"];
    NSString *imageUrl = self.detailsDic[@"after_img"];
    
    
    if ([videoUrl containsString:@"https://"]) {
        videoImageURL = [NSURL URLWithString:videoUrl];
    }else {
        videoImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:videoUrl]];
    }
    
    [self.videoImageView sd_setImageWithURL:videoImageURL placeholderImage:[UIImage imageNamed:@"splash2"] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
        if (!error) {
            self.videoImageView.image = image;
        }else{
            self.videoImageView.image = [UIImage imageNamed:@"splash2"];
        }
    }];
    
    if ([imageUrl containsString:@"https://"]) {
        currentImageURL = [NSURL URLWithString:imageUrl];
    }else {
        currentImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:imageUrl]];
    }
    
    [self.imageImageView sd_setImageWithURL:currentImageURL placeholderImage:[UIImage imageNamed:@"splash2"] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
        if (!error) {
            self.imageImageView.image = image;
        }else{
            self.imageImageView.image = [UIImage imageNamed:@"splash2"];
        }
    }];
    
    self.videoDescLabel.text = self.detailsDic[@"description"];
    self.imageDescLabel.text = self.detailsDic[@"description"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.view needsUpdateConstraints];
    [self.view layoutIfNeeded];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];
    self.videoView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.scrollViewObject.frame.size.height);
    
    self.imageView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width, 0, [UIScreen mainScreen].bounds.size.width, self.scrollViewObject.frame.size.height);
    [self.scrollViewObject addSubview:self.videoView];
    [self.scrollViewObject addSubview:self.imageView];
    self.scrollViewObject.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * 2, self.scrollViewObject.frame.size.height);
    
}
//MARK: - ButtonActions

- (IBAction)videoImageButtonActions:(UIButton *)sender {
    
    frameRect.origin.x = sender.frame.origin.x;
    self.underLineView.frame = frameRect;
    [self selectCurrentPage:sender.tag];
}

- (void)selectCurrentPage:(NSInteger)pageNo{
    
    CGRect frame;
    frame.origin.x = self.scrollViewObject.frame.size.width * pageNo;
    frame.origin.y = 0;
    frame.size = self.scrollViewObject.bounds.size;
    [self.scrollViewObject  scrollRectToVisible:frame animated:YES];
}
- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)videoPlayButtonAction:(id)sender {
    if (![self.detailsDic[@"video"] isEqual:@""]) {
        // grab a local URL to our video
        NSURL *videoURL = [NSURL URLWithString:self.detailsDic[@"video"]];
        
        // create an AVPlayer
        AVPlayer *player = [AVPlayer playerWithURL:videoURL];
        
        // create a player view controller
        AVPlayerViewController *controller = [[AVPlayerViewController alloc]init];
        controller.player = player;
        [player play];
        
        // show the view controller
        [self presentViewController:controller animated:YES completion:nil];
    }else {
        [self showAlertWithErrorTitle:@"" andWithErrorMessage:@"No video available to play"];
    }
}
@end
