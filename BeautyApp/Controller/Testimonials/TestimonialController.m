//
//  TestimonialController.m
//  BeautyApp
//
//  Created by Srikanthreddy on 23/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "TestimonialController.h"
#import "AppDelegate.h"
@interface TestimonialController ()

@end

@implementation TestimonialController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.collTest registerNib:[MyNewTestiCell nib] forCellWithReuseIdentifier:MY_NEW_TESTMONIAL_IDENTITY];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//MARK: - CollectionView Datasource & Delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.testiArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MyNewTestiCell *tCell = [collectionView dequeueReusableCellWithReuseIdentifier:MY_NEW_TESTMONIAL_IDENTITY forIndexPath:indexPath];
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator startAnimating];
    [indicator setCenter:tCell.testimonialImageView.center];
    [tCell.testimonialImageView addSubview:indicator];
    NSURL *currentImageURL;
    NSString *currentImageSTR;
    if ([self.testiArray[indexPath.row][@"video_thumbnail"] isEqual:@""]) {
       currentImageSTR = self.testiArray[indexPath.row][@"after_img"];
    }else {
        currentImageSTR = self.testiArray[indexPath.row][@"video_thumbnail"];
    }
    
    if ([currentImageSTR containsString:@"https://"]) {
        currentImageURL = [NSURL URLWithString:currentImageSTR];
    }else {
        currentImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:currentImageSTR]];
    }
    [tCell.testimonialImageView sd_setImageWithURL:currentImageURL placeholderImage:[UIImage imageNamed:@"splash2"] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
        if (!error) {
            [indicator removeFromSuperview];
            tCell.testimonialImageView.image = image;
        }else{
            tCell.testimonialImageView.image = [UIImage imageNamed:@"splash2"];
            [indicator removeFromSuperview];
        }
    }];
    
    tCell.likesCountLabel.text = [self.testiArray[indexPath.row][@"likes"] stringValue];
    tCell.likesButton.tag = indexPath.row;
    [tCell.likesButton addTarget:self action:@selector(likeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    return tCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake([UIScreen mainScreen].bounds.size.width / 2, [UIScreen mainScreen].bounds.size.width / 2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *dictionary = [self.testiArray[indexPath.row] mutableCopy];
//    self.detailTestBlock(dictionary);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DETAIL_TEST_OBJECT" object:dictionary];
}

- (void)likeButtonAction:(UIButton *)sender {
    if ([[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER]) {
        NSMutableDictionary *mutDic = [self.testiArray[sender.tag] mutableCopy];
        __block NSInteger likeCount = [mutDic[@"likes"] integerValue];
        if (![mutDic[@"user_id"] isKindOfClass:[NSNull class]]) {
            if ([[[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER][@"user_id"] integerValue] == [mutDic[@"user_id"] integerValue] ) {
                [self showAlertWithErrorTitle:@"" andWithErrorMessage:@"You already liked this post"];
                return;
            }
        }
        NSDictionary *params = @{@"user_id" : [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER][@"user_id"],
                                 @"testimonial_id" : mutDic[@"testimonial_id"]
                                 };
        [[AppDelegate sharedAppDelegate].window addSubview:[AppDelegate sharedAppDelegate].loadingView];
        [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
        [[APIWrappers sharedAPIWrapperInstance] userLikeTestimonialWithURLString:TESTIMONIAL_LIKE_UPDATE withParameters:params withComplitionBlock:^(NSDictionary *response, NSString *errorStr) {
            [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
            if (response) {
                
                likeCount = likeCount + 1;
                NSNumber *number = [NSNumber numberWithInteger:likeCount];
                [mutDic setValue:number forKey:@"likes"];
                [mutDic setValue:[[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER][@"user_id"] forKey:@"user_id"];
                [self.testiArray replaceObjectAtIndex:sender.tag withObject:mutDic];
                [self.collTest reloadData];
                [self showAlertWithErrorTitle:@"" andWithErrorMessage:response[@"message"]];
            }else {
                [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
            }
        }];
    }else {
        [self showAlertWithErrorTitle:@"" andWithErrorMessage:@"Please login"];
    }
}


@end
