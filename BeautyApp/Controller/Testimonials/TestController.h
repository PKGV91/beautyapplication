//
//  TestController.h
//  BeautyApp
//
//  Created by Pramod on 05/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"
#import "TestImageCell.h"
#import "TestimonialController.h"
#import "DetailTestimonialController.h"

@interface TestController : BaseViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate>


@property (nonatomic, strong) NSArray *testimonialsArray;
@property (nonatomic, strong) NSArray *imagesArray;
@property (nonatomic, strong) NSArray *videossArray;
@property (nonatomic, strong) UILabel *underLineView;
@property (nonatomic, strong) IBOutlet UIPageViewController *pageController;
@end
