//
//  TestController.m
//  BeautyApp
//
//  Created by Pramod on 05/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "TestController.h"

@interface TestController () {
    CGRect frameRect;
}

@end

@implementation TestController
int presentNewIndex;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showDetailsTestMonialControllerWithDic:) name:@"DETAIL_TEST_OBJECT" object:nil];
    self.underLineView = [[UILabel alloc] init];
    frameRect = CGRectMake(0, 106, [UIScreen mainScreen].bounds.size.width / 2, 2);
    self.underLineView.frame = frameRect;
    [self.view addSubview:self.underLineView];
    self.underLineView.backgroundColor = [UIColor redColor];
    presentNewIndex = 0;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.testimonialsArray.count > 0) {
        [self pageViewControllerSetup];
    }
}
- (void)pageViewControllerSetup {
    NSPredicate *imagePredicate = [NSPredicate predicateWithFormat:@"video == %@", @""];
    NSPredicate *videoPredicate = [NSPredicate predicateWithFormat:@"video CONTAINS[C] %@", @"https://"];
    self.imagesArray = [self.testimonialsArray filteredArrayUsingPredicate:imagePredicate];
    self.videossArray = [self.testimonialsArray filteredArrayUsingPredicate:videoPredicate];
    self.pageController.view.frame = CGRectMake(0, 108, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 108);
    self.pageController.view.autoresizesSubviews = YES;
    TestimonialController *greetings = [self viewControllerAtIndex:presentNewIndex];
    greetings.detailTestBlock = ^(NSDictionary *detailDic) {
//        [self showDetailsTestMonialControllerWithDic:detailDic];
    };
    [self.pageController setViewControllers:@[greetings] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    for (UIScrollView *view in self.pageController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.scrollEnabled = NO;
        }
    }
    [self addChildViewController:self.pageController];
    [self.view addSubview:self.pageController.view];
    [self.pageController didMoveToParentViewController:self];
}
- (void)showDetailsTestMonialControllerWithDic:(NSNotification *)dic {
    DetailTestimonialController *controller = [[DetailTestimonialController alloc] initWithNibName:@"DetailTestimonialController" bundle:nil];
    controller.detailsDic = dic.object;
    [self.navigationController pushViewController:controller animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];
    
    if (self.testimonialsArray.count == 0) {
        [self getAllTestimonialsList];
    }
}


- (void)getAllTestimonialsList {
    NSDictionary *dictionary = [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER];
    NSDictionary *params;
    if (dictionary) {
        params = @{@"user_id" : dictionary[@"user_id"]};
        //        params = @{};
    }else {
        params = @{};
    }
    [[AppDelegate sharedAppDelegate].window addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] getTestimonialsWithURLString:GET_All_Testimonials withParametes:params withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.testimonialsArray = [response mutableCopy];
            [self pageViewControllerSetup];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}
- (IBAction)sideMenuButtonAction:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}

- (TestimonialController *)viewControllerAtIndex:(NSUInteger)index {
    
    TestimonialController *childViewController = [[TestimonialController alloc] initWithNibName:@"TestimonialController" bundle:nil];

    if (index == 0) {
        childViewController.testiArray = [self.videossArray mutableCopy];
    }else {
        childViewController.testiArray = [self.imagesArray mutableCopy];
    }
    childViewController.index = index;
    childViewController.view.frame = self.pageController.view.frame;
    return childViewController;
}

//MARK: - ButtonActions

- (IBAction)videoImageButtonActions:(UIButton *)sender {
    if (presentNewIndex == (int)sender.tag) {
        return;
    }
    frameRect.origin.x = sender.frame.origin.x;
    self.underLineView.frame = frameRect;
    
    TestimonialController *initialViewController = [self viewControllerAtIndex:sender.tag];
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    if (presentNewIndex < sender.tag){
        [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
    else{
        [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    }
    
    presentNewIndex = (int)sender.tag;
}

#pragma Pagecontroller Delegates

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(TestimonialController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(TestimonialController *)viewController index];
    
    index++;
    
    if (index >= 1) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}
@end
