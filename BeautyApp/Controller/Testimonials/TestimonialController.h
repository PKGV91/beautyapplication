//
//  TestimonialController.h
//  BeautyApp
//
//  Created by Srikanthreddy on 23/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "MyNewTestiCell.h"

typedef void(^DetailTestMonial)(NSDictionary *detailDic);


@interface TestimonialController : BaseViewController<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) IBOutlet UICollectionView *collTest;

@property (assign, nonatomic) NSInteger index;
@property (nonatomic, strong) NSMutableArray *testiArray;
@property (nonatomic, strong) DetailTestMonial detailTestBlock;
@end
