//
//  DetailTestimonialController.h
//  BeautyApp
//
//  Created by Pramod on 04/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"
#import "Constants.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface DetailTestimonialController : BaseViewController{
    CGRect frameRect;
}
@property (weak, nonatomic) IBOutlet UILabel *imageDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoDescLabel;
@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *imageImageView;
@property (nonatomic, strong) UILabel *underLineView;
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UIView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewObject;
@property (nonatomic, strong) NSDictionary *detailsDic;

@end
