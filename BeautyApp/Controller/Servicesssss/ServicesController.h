//
//  ServicesController.h
//  BeautyApp
//
//  Created by Srikanthreddy on 21/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "ServiceCell.h"
#import "AppDelegate.h"
@interface ServicesController : BaseViewController
@property (strong, nonatomic) IBOutlet UITableView *tableServices;

@property (nonatomic, strong) NSArray *servicesArray;
@end
