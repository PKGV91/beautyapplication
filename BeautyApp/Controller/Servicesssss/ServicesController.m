//
//  ServicesController.m
//  BeautyApp
//
//  Created by Srikanthreddy on 21/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "ServicesController.h"
#import "TipCell.h"
#import "Tip2Cell.h"

@interface ServicesController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation ServicesController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [_tableServices registerNib:[UINib nibWithNibName:@"TipCell" bundle:nil] forCellReuseIdentifier:@"TipCell"];
    [_tableServices registerNib:[UINib nibWithNibName:@"Tip2Cell" bundle:nil] forCellReuseIdentifier:@"Tip2Cell"];
    self.servicesArray = [AppDelegate sharedAppDelegate].categoriesArray;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.servicesArray.count == 0) {
       [self getAllCategoryLists];
    }
}
- (void)getAllCategoryLists {
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] getCategoryListWithURLString:GET_ALL_CATEGORY withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.servicesArray = response;
            [self.tableServices reloadData];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - Table Dalegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.servicesArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    NSDictionary *dictionary = self.servicesArray[indexPath.row];
    NSUInteger row = indexPath.row;
    BOOL isOdd = row % 2;
    if (isOdd == false) {
         Tip2Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"Tip2Cell"];
        NSLog(@"%ldd",(long)indexPath.row);
        cell.lbl2Title.text = dictionary[@"category_name"];
        cell.lbl2Desc.text = @"need service name";
        NSURL *currentImageURL;
        NSString *currentImageSTR = dictionary[@"cat_img"];
        if ([currentImageSTR containsString:@"https://"]) {
            currentImageURL = [NSURL URLWithString:currentImageSTR];
        }else {
            currentImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:currentImageSTR]];
        }
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [indicator startAnimating];
        [indicator setCenter:cell.tip2Image.center];
        [cell.tip2Image addSubview:indicator];
        [cell.tip2Image sd_setImageWithURL:currentImageURL placeholderImage:[UIImage imageNamed:@"splash2"] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
            if (!error) {
                [indicator removeFromSuperview];
                cell.tip2Image.image = image;
                
            }else{
                cell.tip2Image.image = [UIImage imageNamed:@"splash2"];
                [indicator removeFromSuperview];
            }
        }];
        return cell;
    }else
    {
         TipCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TipCell"];
        NSLog(@"%ld",(long)indexPath.row);
        cell.lblTitle.text = dictionary[@"category_name"];
        cell.lblDesc.text = @"need service name";
        NSURL *currentImageURL;
        NSString *currentImageSTR = dictionary[@"cat_img"];
        if ([currentImageSTR containsString:@"https://"]) {
            currentImageURL = [NSURL URLWithString:currentImageSTR];
        }else {
            currentImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:currentImageSTR]];
        }
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [indicator startAnimating];
        [indicator setCenter:cell.tipImageView.center];
        [cell.tipImageView addSubview:indicator];
        [cell.tipImageView sd_setImageWithURL:currentImageURL placeholderImage:[UIImage imageNamed:@"splash2"] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
            if (!error) {
                [indicator removeFromSuperview];
                cell.tipImageView.image = image;
                
            }else{
                cell.tipImageView.image = [UIImage imageNamed:@"splash2"];
                [indicator removeFromSuperview];
            }
        }];
        return cell;
    }
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    AppointmentController *appointment = [[AppointmentController alloc] initWithNibName:@"AppointmentController" bundle:nil];
    appointment.categoryID = @"1";//[NSString stringWithFormat:@"%d", (int)self.categoriesArray[indexPath.row][@"cat_id"]];
    appointment.greetingsArray = [AppDelegate sharedAppDelegate].categoriesArray;
    appointment.fromType = @"service";
    [self.navigationController pushViewController:appointment animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

- (IBAction)backButtonAction:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}
@end
