//
//  ConfirmController.h
//  BeautyApp
//
//  Created by Srikanthreddy on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "BookingCell.h"
#import "ServicesCell.h"
#import "APIWrappers.h"
#import "AppDelegate.h"
#import <Razorpay/Razorpay.h>

static NSString *const KEY_ID =
@"rzp_live_FUvOIwfOBMSlWM"; // @"rzp_test_1DP5mmOlF5G5ag";
static NSString *const SUCCESS_TITLE = @"Yay!";
static NSString *const SUCCESS_MESSAGE =
@"Your payment was successful. The payment ID is %@";
static NSString *const FAILURE_TITLE = @"Uh-Oh!";
static NSString *const FAILURE_MESSAGE =
@"Your payment failed due to an error.\nCode: %d\nDescription: %@";
static NSString *const EXTERNAL_METHOD_TITLE = @"Umm?";
static NSString *const EXTERNAL_METHOD_MESSAGE =
@"You selected %@, which is not supported by Razorpay at the moment.\nDo "
@"you want to handle it separately?";
static NSString *const OK_BUTTON_TITLE = @"OK";


@interface ConfirmController : BaseViewController<UITextFieldDelegate, RazorpayPaymentCompletionProtocol>
@property (strong, nonatomic) IBOutlet UITableView *bookingTable;
@property (nonatomic, strong) NSArray *servicesArray;
@property (nonatomic, strong) NSString *orderID;
@property (nonatomic, strong) NSString *dateString;
@property (nonatomic, strong) NSArray *packagesArray;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UIView *footerView;
@property (nonatomic, strong) IBOutlet UIView *selectPayView;
@property (nonatomic, strong) NSMutableArray *psMutArray;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (weak, nonatomic) IBOutlet UILabel *netAmountLabel;
@property (weak, nonatomic) IBOutlet UITextField *couponCodeTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *couponIdHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFieldTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFieldHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineLabelHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *couponIdTopConstraint;

@property (weak, nonatomic) IBOutlet UILabel *couponLabel;
@property (nonatomic, assign) BOOL checkSelected;
@property (nonatomic, assign) NSInteger actualAmt;
@property (nonatomic, strong) NSString *walletAmount;
@property (nonatomic, strong) Razorpay *razorpay;


@end
//[NSString stringWithFormat:@"%@", dictionary[@"service_id"]]
