//
//  ConfirmController.m
//  BeautyApp
//
//  Created by Srikanthreddy on 02/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "ConfirmController.h"

@interface ConfirmController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *arrCat,*arrPrice;
}
@property (nonatomic, assign) NSInteger totalFullAmt;
@end

@implementation ConfirmController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.psMutArray = [[NSMutableArray alloc] init];
    self.totalFullAmt = 0;
    [self.bookingTable registerNib:[LayerCell nib] forCellReuseIdentifier:LayerCellIdentifier];
    [self.bookingTable registerNib:[ALayerCell nib] forCellReuseIdentifier:ANewLayerCellIdentifier];
    [self.bookingTable registerNib:[ServicesCell nib] forCellReuseIdentifier:WallBAL_IDENITITY];
    self.bookingTable.estimatedRowHeight = 100;
    self.bookingTable.rowHeight = UITableViewAutomaticDimension;
    self.headerView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44);
    self.footerView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 90);
    self.bookingTable.tableHeaderView = self.headerView;
    self.bookingTable.tableFooterView = self.footerView;
    [self.psMutArray addObjectsFromArray:self.packagesArray];
    [self.psMutArray addObjectsFromArray:self.servicesArray];
    
    
     self.razorpay = [Razorpay initWithKey:KEY_ID andDelegate:self];
    self.couponIdHeightConstraint.constant = self.textFieldTopConstraint.constant = self.textFieldHeightConstraint.constant = self.lineLabelHeightConstraint.constant = self.couponIdTopConstraint.constant = 0;
    self.selectPayView.frame = [UIScreen mainScreen].bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table Datasource
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//
//
//    return 3;
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    if (section == 0 ) {
//        return 1;
//
//    }else if (section == 1){
//        return self.servicesArray.count;
//
//    }
    return self.psMutArray.count + 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *cell;
    LayerCell *lCell = [tableView dequeueReusableCellWithIdentifier:LayerCellIdentifier];
    ALayerCell *aNewCell = [tableView dequeueReusableCellWithIdentifier:ANewLayerCellIdentifier];
    ServicesCell *sCell = [tableView dequeueReusableCellWithIdentifier:WallBAL_IDENITITY];
    if (indexPath.row <= self.psMutArray.count - 1 ) {
        NSDictionary *dictionary = self.psMutArray[indexPath.row];
        if ([[dictionary allKeys] containsObject:@"package_name"]) {
            lCell.itemTitleLabel.text = dictionary[@"package_name"];
            lCell.itemDescLabel.text = dictionary[@"package_description"];
            lCell.itemPriceLabel.text = [NSString stringWithFormat:@"₹ %@ INR", dictionary[@"package_price"]];
            self.totalFullAmt = self.totalFullAmt + [dictionary[@"package_price"] integerValue];
            lCell.itemDateExpLabel.text = [NSString stringWithFormat:@"%@ to %@", [self getDateStringFromString:dictionary[@"package_start_date"]], [self getDateStringFromString:dictionary[@"package_end_date"]]];
            lCell.itemDateExpLabel.text = [lCell.itemDateExpLabel.text stringByReplacingOccurrencesOfString:@"/" withString:@"'"];
            cell = lCell;
        }else {
            aNewCell.itemTitleLabel.text = dictionary[@"service_name"];
            aNewCell.itemDateExpLabel.text = dictionary[@"service_duration"];
            double disAmount = [dictionary[@"service_price"] doubleValue] * [self getFinalAmountDiscount];
            aNewCell.itemPriceLabel.text = [NSString stringWithFormat:@"₹ %.1f INR", [dictionary[@"service_price"] doubleValue] - disAmount];
            self.totalFullAmt = self.totalFullAmt + ([dictionary[@"service_price"] doubleValue] - disAmount);
            
            cell = aNewCell;
        }
        self.actualAmt = self.totalFullAmt;
    }else {
        
        NSDictionary *userDic = [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER];
        sCell.walletButton.tag = indexPath.row;
        sCell.walletAmtLabel.text = [NSString stringWithFormat:@"-%@", userDic[@"points"]];
        sCell.walletBalLabel.text = [NSString stringWithFormat:@"%@ points = ₹ %@", userDic[@"points"], userDic[@"points"]];
        [sCell.walletButton addTarget:self action:@selector(checkMarkSelectionAction:) forControlEvents:UIControlEventTouchUpInside];
        if (indexPath == self.selectedIndexPath) {
            [sCell.walletButton setImage:[UIImage imageNamed:@"check-selected"] forState:UIControlStateNormal];
            sCell.walletAmountTopConstraint.constant = 12;
            sCell.walletAmountHeightConstraint.constant = 16;
            self.totalFullAmt = self.totalFullAmt - [userDic[@"points"] integerValue];
            self.walletAmount = userDic[@"points"];
            
        }else {
           
            sCell.walletAmountTopConstraint.constant = sCell.walletAmountHeightConstraint.constant = 0;
            [sCell.walletButton setImage:[UIImage imageNamed:@"check-box-empty"] forState:UIControlStateNormal];
            
        }
        sCell.priceLabel.text = [NSString stringWithFormat:@"₹ %ld", self.totalFullAmt];
        self.netAmountLabel.text = sCell.priceLabel.text;
        cell = sCell;
    }
    
    return cell;
}

- (void)checkMarkSelectionAction:(UIButton *)sender {
    
    self.totalFullAmt = 0;
    if (_checkSelected) {
        _checkSelected = NO;
        self.selectedIndexPath = nil;
    }else {
        _checkSelected = YES;
        self.selectedIndexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    }
    [self.bookingTable reloadData];
}
- (NSString *)getDateStringFromString:(NSString *)dateStr {
    NSArray *splitArray = [dateStr componentsSeparatedByString:@"T"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [formatter dateFromString:splitArray[0]];
    [formatter setDateFormat:@"MMM/dd"];
    return [formatter stringFromDate:date];
}

- (double)getFinalAmountDiscount {
    int disCountAmount = 0;
    for (NSDictionary *dic in self.packagesArray) {
        
        disCountAmount = disCountAmount + [dic[@"package_on_other_services"] intValue];
    }
    return disCountAmount/100.0;
}


- (void)showAlertWithErrorTitle:(NSString *)title andWithErrorMessage:(NSString *)errorMsg andIsConfirm:(BOOL)confirm {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:errorMsg preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (confirm) {
            [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
        }
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)proceedButtonAction:(id)sender {
    [self.view addSubview:self.selectPayView];
}

- (IBAction)backButtonAction:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



- (IBAction)addCouponAction:(UIButton *)sender {
    if (sender.selected) {
        sender.selected = NO;
        self.couponIdHeightConstraint.constant = self.textFieldTopConstraint.constant = self.textFieldHeightConstraint.constant = self.lineLabelHeightConstraint.constant = self.couponIdTopConstraint.constant = 0;
        self.footerView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 100);
        self.couponLabel.text = @"DO YOU HAVE A COUPON?";
    }else {
        sender.selected = YES;
        self.couponLabel.text = @"NO COUPONS OR NOT TO APPLY";
        self.couponIdHeightConstraint.constant = 21;
        self.couponIdTopConstraint.constant = self.textFieldTopConstraint.constant = 12;
        self.textFieldHeightConstraint.constant = 30;
        self.lineLabelHeightConstraint.constant = 1;
        self.footerView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 170);
    }
    self.bookingTable.tableFooterView = self.footerView;
}

- (IBAction)codOnlinePaymentAction:(UIButton *)sender {
    [self removePaymentViewAction:nil];
    if (sender.tag == 0) {//cod
        [self webServiceCallWithDictionary:[@{@"payment_type" : @"COD"} mutableCopy]];
    }else {
        [self paymentMethod];
    }
}

- (IBAction)removePaymentViewAction:(id)sender {
    [self.selectPayView removeFromSuperview];
}

- (void)webServiceCallWithDictionary:(NSMutableDictionary *)dictionary {
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in self.packagesArray) {
        [array addObject:dic[@"package_id"]];
    }
    [dictionary setValue:[NSString stringWithFormat:@"%ld", self.totalFullAmt] forKey:@"payable_amount"];
    [dictionary setValue:[NSString stringWithFormat:@"%ld", self.actualAmt] forKey:@"actual_amount"];
    [dictionary setValue:[[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER][@"user_id"] forKey:@"user_id"];
    [dictionary setValue:self.couponCodeTextField.text.length > 0 ? self.couponCodeTextField.text : @"" forKey:@"coupon_id"];
    [dictionary setObject:self.servicesArray forKey:@"services"];
    [dictionary setObject:array forKey:@"packages"];
    if (self.selectedIndexPath.row >= 1) {
       [dictionary setValue:self.walletAmount forKey:@"wallet_points"];
    }else {
        [dictionary setValue:@"0" forKey:@"wallet_points"];
    }
    [dictionary setValue:self.orderID forKey:@"order_id"];
    
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] confirmBookingPayementWithURLString:POST_PAYMENT_CONFIRMATION withParameters:dictionary withComplitionBlock:^(NSDictionary *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            
        }else {
            
        }
    }];
}
- (IBAction)applyCouponAction:(id)sender {
    if (self.couponCodeTextField.text.length > 0) {
        [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
        [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
        [[APIWrappers sharedAPIWrapperInstance] couponVerifyWithUrlStr:POST_COUPON_VERIFY withParameters:@{@"coupon_code" : self.couponCodeTextField.text} withComplitionBlock:^(NSDictionary *response, NSString *errorStr) {
            [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
            if (response) {
                if ([response[@"results"] count] > 0) {
                    
                }else {
                    [self showAlertWithErrorTitle:@"" andWithErrorMessage:@"Enter coupon not valid"];
                }
            }else {
                [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
            }
        }];
    }else {
        [self showAlertWithErrorTitle:@"" andWithErrorMessage:@"Please enter coupon to apply"];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)paymentMethod {
    
    NSDictionary *dic = [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER];
    UIImage *logo = [UIImage imageNamed:@"logo"];
    NSDictionary *options = @{
                              @"amount" : [NSString stringWithFormat:@"%ld.00", self.totalFullAmt],
                              @"currency" : @"INR",
                              @"description" : @"Fine T-shirt",
                              @"image" : logo,
                              @"name" : @"Razorpay",
                              @"external" : @{@"wallets" : @[ @"paytm" ]},
                              @"prefill" :
                                  @{@"email" : dic[@"email_id"], @"contact" : dic[@"mobile"]},
                              @"theme" : @{@"color" : @"#3594E2"}
                              };
    
    [self.razorpay open:options];
}

- (void)onPaymentSuccess:(NSString *)payment_id {
    [self showAlertWithErrorTitle:SUCCESS_TITLE andWithErrorMessage:[NSString
                                                                     stringWithFormat:SUCCESS_MESSAGE, payment_id]];
}

- (void)onPaymentError:(int)code description:(NSString *)str {
    [self showAlertWithErrorTitle:FAILURE_TITLE andWithErrorMessage:[NSString
                                                                     stringWithFormat:FAILURE_MESSAGE, code, str]];
}

- (void)onExternalWalletSelected:(NSString *)walletName
                 WithPaymentData:(NSDictionary *)paymentData {
    NSLog(@"%@", paymentData);
}
@end
