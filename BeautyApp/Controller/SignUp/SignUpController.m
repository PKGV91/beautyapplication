//
//  SignUpController.m
//  BeautyApp
//
//  Created by way2online on 27/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "SignUpController.h"

@interface SignUpController ()

@end

@implementation SignUpController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.datePicker.maximumDate = [NSDate date];
    self.dateString = @"";
    self.selectedContainerView.hidden = YES;
    self.selectedContainerView.frame = CGRectMake(20, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width - 40, 430);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addUpdates{
    self.selectedContainerView.frame = CGRectMake(20, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width - 40, 430);
    [self.selectedContainerView setNeedsUpdateConstraints];
    self.selectedContainerView.hidden = NO;
    [UIView animateWithDuration:0.4 animations:^{
        self.selectedContainerView.frame = CGRectMake(20, 20, [UIScreen mainScreen].bounds.size.width - 40, 430);
        [self.selectedContainerView layoutIfNeeded];
        self.selectedContainerView.center = self.view.center;
    }];
}

- (void)removeUpdatesWithTag:(int)tagValue{
    [UIView animateWithDuration:0.4 animations:^{
        self.selectedContainerView.frame = CGRectMake(20, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width - 40, [UIScreen mainScreen].bounds.size.height);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];;
        if (tagValue == 1) {
            self.signINController();
        }else if (tagValue == 2) {
            self.otpController(self.mobileNoTextField.text);
        }
        
    }];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.view needsUpdateConstraints];
    [self.view layoutIfNeeded];
    
    self.view1.frame = CGRectMake(0, 0, self.scrollView.bounds.size.width, self.scrollView.frame.size.height);
    
    self.view2.frame = CGRectMake(self.scrollView.bounds.size.width, 0, self.scrollView.bounds.size.width, self.scrollView.frame.size.height);
    
     self.view3.frame = CGRectMake(self.scrollView.bounds.size.width * 2, 0, self.scrollView.bounds.size.width, self.scrollView.frame.size.height);
    [self.scrollView addSubview:self.view1];
    [self.scrollView addSubview:self.view2];
    [self.scrollView addSubview:self.view3];
    self.scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width * 3, self.scrollView.frame.size.height);
    [self addUpdates];
}

- (void)selectCurrentPage:(NSInteger)pageNo{
    
    CGRect frame;
    frame.origin.x = self.scrollView.frame.size.width * pageNo;
    frame.origin.y = 0;
    frame.size = self.scrollView.bounds.size;
    [self.scrollView  scrollRectToVisible:frame animated:YES];
}

#pragma mark Button Actions

- (IBAction)signUpButtonAction:(id)sender {
    NSDictionary *params = @{@"fullname" : self.firstNameTextField.text.length > 0 ? self.firstNameTextField.text : @"",
                             @"email_id" : self.emailTextField.text.length > 0 ? self.emailTextField.text : @"",
                             @"mobile" : self.mobileNoTextField.text.length > 0 ? self.mobileNoTextField.text : @"",
                                 @"gender" : self.maleFemailStr,
                             @"dob" : self.dateString.length > 0 ? self.dateString : @"",
                             @"referrer_id" : @"1"
                             };
    if ([self isValidDetails:params]) {
        [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
        [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
        [[APIWrappers sharedAPIWrapperInstance] userSignUpServiceWithURLString:SIGNUP_URL withParameters:params withComplitionBlock:^(NSDictionary *response, NSString *errorStr) {
                [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
               if (response) {
                   [self removeUpdatesWithTag:2];
                }else {
                   [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage:errorStr];
                }
        }];
    }
}

- (IBAction)showDatePickerButtonAction:(id)sender {
    
        [self.monthTextField becomeFirstResponder];
        [self.dateTextField becomeFirstResponder];
        [self.yearTextField becomeFirstResponder];
    
}

- (IBAction)signInButtonAction:(id)sender {
    
    [self removeUpdatesWithTag:1];
}

- (IBAction)closeCancelButtonAction:(id)sender {
    [self removeUpdatesWithTag:0];
}

- (IBAction)checkMarkButtonAction:(UIButton *)sender {
    if (self.isCheckMarkSelected) {
        self.isCheckMarkSelected = NO;
        [sender setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
    }else {
        self.isCheckMarkSelected = YES;
        [sender setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
    }
}

- (IBAction)maleFemaleButtonAction:(UIButton *)sender {
    for (UILabel *label in self.genderLabelsArray) {
        label.textColor = [UIColor blackColor];
    }
    if (sender.tag == 1) {
        UILabel *label = self.genderLabelsArray[sender.tag - 1];
        label.textColor = [UIColor colorFromHexString:@"FF2B64"];
        self.maleFemailStr = @"M";
    }else {
        UILabel *label = self.genderLabelsArray[sender.tag - 1];
        label.textColor = [UIColor colorFromHexString:@"FF2B64"];
        self.maleFemailStr = @"F";
    }
    [self selectCurrentPage:1];
}
- (IBAction)doneButtonAction:(id)sender {
    if (!self.isMobileTextField) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        self.dateString = [formatter stringFromDate:self.datePicker.date];
        NSArray *dateArray = [[formatter stringFromDate:self.datePicker.date] componentsSeparatedByString:@"-"];
        self.dateTextField.text = dateArray[2];
        self.monthTextField.text = dateArray[1];
        self.yearTextField.text = dateArray[0];
        [self.dateTextField resignFirstResponder];
        [self.monthTextField resignFirstResponder];
        [self.yearTextField resignFirstResponder];
        [self performSelector:@selector(moveToDetails) withObject:nil afterDelay:1];
        
    }
    NSInteger nextTag = self.isMobileTextField ? self.mobileNoTextField.tag + 1 : self.dobTextField.tag + 1;
    [self jumpToNextTextField:self.isMobileTextField ? self.mobileNoTextField : self.dobTextField withTag:nextTag];
}
- (void)moveToDetails {
    [self selectCurrentPage:2];
}
- (IBAction)nextFinishAction:(UIButton *)sender {
    if (sender.tag == 10) {
        [self selectCurrentPage:1];
    }else if (sender.tag == 11) {
        if (self.dateTextField.text.length == 0 || self.monthTextField.text.length == 0 || self.yearTextField.text.length == 0) {
            [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage:@"Please select your date of birth"];
            return;
        }
        [self selectCurrentPage:2];
    }
}



#pragma mark UITextField Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.mobileNoTextField) {
        self.toolBar.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44);
        textField.inputAccessoryView = self.toolBar;
        self.isMobileTextField = YES;
    }else if (textField == self.dateTextField || textField == self.monthTextField || textField == self.yearTextField){
        self.isMobileTextField = NO;
        self.toolBar.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44);
        self.datePicker.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 216);
        textField.inputView = self.datePicker;
        textField.inputAccessoryView = self.toolBar;
        
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    return NO;
}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag {
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        [nextResponder becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
}
    
- (BOOL)isValidDetails:(NSDictionary *)details {
    if ([details[@"fullname"] length] == 0){
        [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage: @"First Name must not empty"];
        return NO;
    }else if ([details[@"email_id"] length] == 0){
        [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage: @"Email ID must not empty"];
        return NO;
    }else if (![details[@"email_id"] isEmailValidate]){
        [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage: @"Email ID is invalid"];
        return NO;
    }else if ([details[@"mobile"] length] == 0){
        [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage: @"Mobile number must not empty"];
        return NO;
    }else if (![details[@"mobile"] isValidateMobileNumber]){
        [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage: @"Enter mobile number is invalid"];
        return NO;
    }else if ([details[@"dob"] length] == 0){
        [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage: @"Date of birth must not empty"];
        return NO;
    }
//    else if ([details[@"first_name"] length] == 0){
//        return NO;
//    }
    
        return YES;
}
@end
