//
//  SignUpController.h
//  BeautyApp
//
//  Created by way2online on 27/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "Constants.h"
#import "UIColor+_Additions.h"

typedef void(^ShowSignInController)(void);
typedef void(^ShowOTPController)(NSString *);

@interface SignUpController : BaseViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *genderLabelsArray;

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileNoTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *dobTextField;
@property (weak, nonatomic) IBOutlet UITextField *referalNumberTextField;
@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, assign) BOOL isMobileTextField;
@property (nonatomic, assign) BOOL isCheckMarkSelected;
@property (nonatomic, strong) NSString *maleFemailStr;

@property (nonatomic, strong) IBOutlet UIView *view1;
@property (nonatomic, strong) IBOutlet UIView *view2;
@property (nonatomic, strong) IBOutlet UIView *view3;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *dateTextField;
@property (weak, nonatomic) IBOutlet UITextField *monthTextField;
@property (weak, nonatomic) IBOutlet UITextField *yearTextField;
@property (nonatomic, strong) NSString *dateString;
@property (nonatomic, strong) NSDictionary *userDetails;

@property (strong, nonatomic) IBOutlet UIView *selectedContainerView;
@property (strong, nonatomic) ShowSignInController signINController;
@property (strong, nonatomic) ShowOTPController otpController;
@end
