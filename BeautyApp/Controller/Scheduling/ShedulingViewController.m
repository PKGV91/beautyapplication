//
//  ShedulingViewController.m
//  BeautyApp
//
//  Created by Srikanthreddy on 27/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "ShedulingViewController.h"
#import "ShedulingCell.h"
#import "InvoiceCell.h"

@interface ShedulingViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation ShedulingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_tableSchedule registerNib:[UINib nibWithNibName:@"ShedulingCell" bundle:nil] forCellReuseIdentifier:@"ShedulingCell"];
    [_tableSchedule registerNib:[UINib nibWithNibName:@"InvoiceCell" bundle:nil] forCellReuseIdentifier:@"InvoiceCell"];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - Table Dalegates
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }
    else{
        return 1;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShedulingCell *cell =[tableView dequeueReusableCellWithIdentifier:@"ShedulingCell"];
    InvoiceCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"InvoiceCell"];
    if (indexPath.section==1) {
        return cell1;
    }
    return cell;
}
- (IBAction)backbtnTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  230;
}


@end
