//
//  ShedulingViewController.h
//  BeautyApp
//
//  Created by Srikanthreddy on 27/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"

@interface ShedulingViewController : BaseViewController
@property (strong, nonatomic) IBOutlet UITableView *tableSchedule;
@property (assign, nonatomic) NSInteger index;
@end
