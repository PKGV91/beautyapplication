//
//  DateTimeController.m
//  BeautyApp
//
//  Created by Srikanthreddy on 01/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "DateTimeController.h"

@interface DateTimeController ()

@end
int count;
@implementation DateTimeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dropDown = [[DropDown alloc] init];
    [self.dateCollectionView registerNib:[DatesCell nib] forCellWithReuseIdentifier:DaysIdentifier];
    NSLog(@"%@\n\n\n%@", self.servicesArray, self.orderID);
    [self getBranchsList];
    count = 0;
    self.datesArray = [[NSMutableArray alloc] initWithCapacity:15];
    for (int i = 0; i < 15; i++) {
        NSDate *dateObj = [[Constants sharedConstants] addDays:i toDate:[NSDate date]];
        [self.datesArray addObject:dateObj];
    }
    [_dateCollectionView reloadData];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)getBranchsList {
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] getBranchsListWithURLString:GET_BRANCHS_LIST withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.servicesArray = [response mutableCopy];
            
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}
- (void)showDropDownWithArray:(NSArray *)datasource withButton:(UIButton *)button{
    if (datasource.count > 0) {
        NSMutableArray *stringArray = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in datasource) {
            [stringArray addObject:dic[@"branch_name"]];
        }
        self.dropDown.dataSource = stringArray;
        self.dropDown.anchorView = button;
        self.dropDown.bottomOffset = CGPointMake(0, button.bounds.size.height);
        __weak typeof(self) WeakSelf = self;
        self.dropDown.selectionAction = ^(NSInteger intValue, NSString *selectedStr){
            [button setTitle:selectedStr forState:UIControlStateNormal];
            [WeakSelf.dropDown hide];
        };
        [self.dropDown show];
    }
    
}


- (IBAction)firstBtnAction:(id)sender {
    [_view1 setHidden:NO];
    [_view2 setHidden:YES];
    [_view3 setHidden:YES];
    [_view4 setHidden:YES];
}
- (IBAction)secondBtnAction:(id)sender {
    [_view1 setHidden:YES];
    [_view2 setHidden:NO];
    [_view3 setHidden:YES];
    [_view4 setHidden:YES];
}
- (IBAction)thirdBtnAction:(id)sender {
    [_view1 setHidden:YES];
    [_view2 setHidden:YES];
    [_view3 setHidden:NO];
    [_view4 setHidden:YES];
}
- (IBAction)fourBtnAction:(id)sender {
    [_view1 setHidden:YES];
    [_view2 setHidden:YES];
    [_view3 setHidden:YES];
    [_view4 setHidden:NO];
}

- (IBAction)nextAndPreviousButtonAction:(UIButton *)sender {
    if (sender.tag == 1) {//previous
        count--;
        if (count < 0) {
            count = 0;
        }
    }else{// next
        count += 1;
        if (count >= 14) {
            count = 14;
        }
    }
    NSIndexPath *iPath = [NSIndexPath indexPathForItem:count
                                             inSection:0];
    [self.dateCollectionView scrollToItemAtIndexPath:iPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.datesArray.count;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    DatesCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:DaysIdentifier forIndexPath:indexPath];
    if (indexPath.row == 0) {
        cell.todayLabel.text = @"Today";
    }else if (indexPath.row == 1) {
        cell.todayLabel.text = @"Tomorrow";
    }else {
        cell.todayLabel.text = [self getDateStringFromDate:self.datesArray[indexPath.row] withIndex:1];
    }
    cell.dayLabel.text = [self getDayFromGivenDate:self.datesArray[indexPath.row]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.bounds.size.width, collectionView.bounds.size.height);
}


- (NSString *)getDayFromGivenDate:(NSDate *)date {
    NSDateComponents *component = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:date];
    NSString *dayString;
    switch ([component weekday]) {
        case 1:
            dayString = @"Sun";
            break;
        case 2:
            dayString = @"Mon";//Monday
            break;
        case 3:
            dayString = @"Tue";//Tuesday
            break;
        case 4:
            dayString = @"Wed";//Wednesday
            break;
        case 5:
            dayString = @"Thu";//Thursday
            break;
        case 6:
            dayString = @"Fri";//Friday
            break;
        case 7:
            dayString = @"Sat";//Saturday
            break;
        default:
            break;
    }
    return [NSString stringWithFormat:@"%@, %@", dayString, [self getDateStringFromDate:date withIndex:0]];
}

- (NSString *)getDateStringFromDate:(NSDate *)date withIndex:(int)index{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if (index == 0) {
        formatter.dateFormat=@"MMM yy";
    }else {
        formatter.dateFormat=@"dd-MMM-yy";
    }
    return [formatter stringFromDate:date];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)SelectBranchAction:(UIButton *)sender {
    [self showDropDownWithArray:_servicesArray withButton:sender];
}
- (IBAction)bookSlotAction:(id)sender {
    NSDictionary *dictionary = [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER];
    if (dictionary) {
        
    }else {
        [[AppDelegate sharedAppDelegate] showLoginController];
    }
    
}
@end
