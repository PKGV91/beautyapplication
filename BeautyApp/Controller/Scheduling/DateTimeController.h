//
//  DateTimeController.h
//  BeautyApp
//
//  Created by Srikanthreddy on 01/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "Constants.h"
#import "DatesCell.h"
#import "ConfirmController.h"
#import "BeautyApp-Swift.h"
#import "APIWrappers.h"
@interface DateTimeController : BaseViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) IBOutlet UIView *view1;
@property (strong, nonatomic) IBOutlet UIView *view2;
@property (strong, nonatomic) IBOutlet UIView *view3;
@property (strong, nonatomic) IBOutlet UIView *view4;

@property (nonatomic, strong) IBOutlet UICollectionView *dateCollectionView;
@property (nonatomic, strong) NSMutableArray *datesArray;
@property (nonatomic, strong) NSArray *servicesArray;
@property (nonatomic, strong) NSString *orderID;

@property (nonatomic, strong) DropDown *dropDown;
@end
