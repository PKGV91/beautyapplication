//
//  OTPController.m
//  BeautyApp
//
//  Created by way2online on 27/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "OTPController.h"

@interface OTPController ()


@property (nonatomic, strong) NSMutableArray *numbersArray;
@end

@implementation OTPController
int i;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (IS_IPHONEX) {
        self.deleteKeyHeightConstraint.constant = 20;
    }else {
        self.deleteKeyHeightConstraint.constant = 4;
    }
    i = 0;
    _numbersArray = [[NSMutableArray alloc] initWithCapacity:6];
    self.selectedContainerView.hidden = YES;
    self.selectedContainerView.frame = CGRectMake(20, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width - 40, 430);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.mobileNumberLabel.text = [NSString stringWithFormat:@"(%@)",_mobileNumber];
    self.bottomViewBottomConstraint.constant = -216;
}
    
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self addUpdates];
}
- (void)addUpdates{
    self.selectedContainerView.frame = CGRectMake(20, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width - 40, 430);
    [self.selectedContainerView setNeedsUpdateConstraints];
    self.selectedContainerView.hidden = NO;
    [UIView animateWithDuration:0.4 animations:^{
        self.selectedContainerView.frame = CGRectMake(20, 20, [UIScreen mainScreen].bounds.size.width - 40, 430);
        [self.selectedContainerView layoutIfNeeded];
        self.selectedContainerView.center = self.view.center;
    }];
}

- (void)removeUpdatesWithTag:(int)tagValue{
    [UIView animateWithDuration:0.4 animations:^{
        self.selectedContainerView.frame = CGRectMake(20, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width - 40, [UIScreen mainScreen].bounds.size.height);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];;
        if (tagValue == 1) {
            self.dismissOTPController(self.userDetails);
        }
    }];
}
#pragma mark UITextField Delegate Mothods
    
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSInteger nextTag = textField.tag + 1;
    NSInteger prevTag = textField.tag - 1;
    NSString *otpString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (otpString.length == 1) {
        textField.text = otpString;
        [self jumpToNextTextField:textField withTag:nextTag];
        return NO;
    }else if (otpString.length == 0) {
        textField.text = otpString;
        [self jumpToNextTextField:textField withTag:prevTag];
        return NO;
    }
    
    return YES;
}

- (BOOL)keyboardInputShouldDelete:(UITextField *)textField {
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    return NO;
}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag {
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        [nextResponder becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
}
    
#pragma mark UIButton Action

- (IBAction)showBottomKeyBoardView:(id)sender {
    [self.view setNeedsUpdateConstraints];
    self.bottomViewBottomConstraint.constant = 0;
    [UIView animateWithDuration:0.4 animations:^{
        [self.view layoutIfNeeded];
    }];
}
- (IBAction)hideKeyBoardViewToBottom:(id)sender {
    [self.view setNeedsUpdateConstraints];
    self.bottomViewBottomConstraint.constant = -216;
    [UIView animateWithDuration:0.4 animations:^{
        [self.view layoutIfNeeded];
    }];
}
- (IBAction)closeButtonAction:(UIButton *)sender {
    [self removeNumberSelection:0];
}
- (IBAction)verifyButtonAction:(id)sender {
    NSString *string = [self.numbersArray componentsJoinedByString:@""];
    NSLog(@"%@", string);
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
        NSDictionary *params = @{@"otp" : string, @"mobile" : self.mobileNumber,
                                 @"device_type" : @"iOS", @"device_id" : [UIDevice currentDevice].identifierForVendor.UUIDString
                                 };
        [self webServiceCallForSignUpWithOTP:params];
//    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}

- (void)webServiceCallForSignUpWithOTP:(NSDictionary *)params{
    [[APIWrappers sharedAPIWrapperInstance] userOTPServiceWithURLString:SIGN_UP_VERIFY_URL withParameters:params withComplitionBlock:^(NSDictionary *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.userDetails = response;
            [self removeUpdatesWithTag:1];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];;
}

- (IBAction)numberSelection:(UIButton *)sender {
    if (self.numbersArray.count < 6) {
        [self.numbersArray insertObject:[NSString stringWithFormat:@"%ld",(long)sender.tag] atIndex:i];
        
    }else return;
    switch (i) {
        case 0:
            NSLog(@"1 on");
            _pass1.text = sender.titleLabel.text;
            break;
        case 1:
            NSLog(@"2 on");
            _pass2.text = sender.titleLabel.text;
            break;
        case 2:
            NSLog(@"3 on");
            _pass3.text = sender.titleLabel.text;
            break;
        case 3:
            NSLog(@"4 on");
            _pass4.text = sender.titleLabel.text;
            break;
        case 4:
            NSLog(@"5 on");
            _pass5.text = sender.titleLabel.text;
            break;
        case 5:
            NSLog(@"6 on");
            _pass6.text = sender.titleLabel.text;
            if (self.numbersArray.count >= 6 ) {
                [self hideKeyBoardViewToBottom:nil];
                [self verifyButtonAction:nil];
            }
            break;
            
        default:
            
            break;
            
    }
    
    i++;
}

- (IBAction)removeNumberSelection:(id)sender {
    
    if (self.numbersArray.count > 0) {
        [self.numbersArray removeObjectAtIndex:i-1];
    }else{
        return;
    }
    i--;
    switch (i) {
        case 0:
            NSLog(@"1 ");
            _pass1.text = @"";
            break;
        case 1:
            NSLog(@"2 ");
            _pass2.text = @"";
            break;
        case 2:
            NSLog(@"3 ");
            _pass3.text = @"";
            break;
        case 3:
            NSLog(@"4 ");
            _pass4.text = @"";
            break;
        case 4:
            NSLog(@"5 ");
            _pass5.text = @"";
            break;
        case 5:
            NSLog(@"6 ");
            _pass6.text = @"";
            break;
        default:
            break;
    }
}
@end
