//
//  OTPController.h
//  BeautyApp
//
//  Created by way2online on 27/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "Constants.h"

typedef void(^DismissOTPController)(NSDictionary *);
@interface OTPController : BaseViewController<UITextFieldDelegate>
@property (nonatomic, strong) NSString *mobileNumber;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deleteKeyHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *pass1;
@property (weak, nonatomic) IBOutlet UILabel *pass2;
@property (weak, nonatomic) IBOutlet UILabel *pass3;
@property (weak, nonatomic) IBOutlet UILabel *pass4;
@property (weak, nonatomic) IBOutlet UILabel *pass5;
@property (weak, nonatomic) IBOutlet UILabel *pass6;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewBottomConstraint;
@property (nonatomic, strong) NSDictionary *userDetails;

@property (strong, nonatomic) IBOutlet UIView *selectedContainerView;
@property (strong, nonatomic) DismissOTPController dismissOTPController;
@end
