//
//  UpdateProfileController.m
//  BeautyApp
//
//  Created by Pramod on 23/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "UpdateProfileController.h"

@interface UpdateProfileController ()

@end

@implementation UpdateProfileController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *dictioanry = [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER];
    self.userNameTextField.text = dictioanry[@"fullname"];
    self.userEmailIDTextField.text = dictioanry[@"email_id"];
    self.userMobileTextField.text = dictioanry[@"mobile"];
    for (UIImageView *imageView in self.genderLabelsArray) {
        imageView.image = [UIImage imageNamed:@"check-box-empty"];
    }
    UIImageView *imageView;
    if ([dictioanry[@"gender"] isEqual:@"M"]) {
        imageView = self.genderLabelsArray[1];
        imageView.image = [UIImage imageNamed:@"check-selected"];
        self.maleFemailStr = @"Male";
    }else {
        imageView = self.genderLabelsArray[0];
        imageView.image = [UIImage imageNamed:@"check-selected"];
        self.maleFemailStr = @"Female";
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ButtonActions

- (IBAction)sideMenuButtonAction:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}


- (IBAction)maleFemaleButtonAction:(UIButton *)sender {
    for (UIImageView *imageView in self.genderLabelsArray) {
        imageView.image = [UIImage imageNamed:@"check-box-empty"];
    }
    UIImageView *imageView = self.genderLabelsArray[sender.tag - 1];
    imageView.image = [UIImage imageNamed:@"check-selected"];
    if (sender.tag == 1) {
        self.maleFemailStr = @"Female";
    }else {
        self.maleFemailStr = @"Male";
    }
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.userMobileTextField) {
        self.toolBar.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44);
        textField.inputAccessoryView = self.toolBar;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    return NO;
}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag {
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        [nextResponder becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
}

- (IBAction)doneButtonAction:(id)sender {
    [self.userMobileTextField resignFirstResponder];
}

- (IBAction)updateProfileAction:(id)sender {
    NSDictionary *params = @{@"fullname" : self.userNameTextField.text.length > 0 ? self.userNameTextField.text : @"",
                             @"email_id" : self.userEmailIDTextField.text.length > 0 ? self.userEmailIDTextField.text : @"",
                             @"mobile" : self.userMobileTextField.text.length > 0 ? self.userMobileTextField.text : @"",
                             @"gender" : self.maleFemailStr,
                             @"user_id" : [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER][@"user_id"]
                             };
    if ([self isValidDetails:params]) {
        [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
        [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
        [[APIWrappers sharedAPIWrapperInstance] updateUserProfileServiceWithURLString:UPDATE_USER_PROFILE withParameters:params withComplitionBlock:^(NSDictionary *response, NSString *errorStr) {
            [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
            if (response) {
                NSMutableDictionary *userDic = [[[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER] mutableCopy];
                [userDic setValue:self.userNameTextField.text forKey:@"fullname"];
                [userDic setValue:self.userMobileTextField.text forKey:@"mobile"];
                [userDic setValue:self.userEmailIDTextField.text forKey:@"email_id"];
                [userDic setValue:[self.maleFemailStr isEqual:@"Male"] ? @"M" : @"F" forKey:@"gender"];
                [[Constants sharedConstants] removeStoredValueForKey:USER_MOBILE_NUMBER];
                [[Constants sharedConstants] storeDictionary:userDic forKey:USER_MOBILE_NUMBER];
                [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage:response[@"message"]];
            }else {
                [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage:errorStr];
            }
        }];
    }
}

- (BOOL)isValidDetails:(NSDictionary *)details {
    if ([details[@"fullname"] length] == 0){
        [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage: @"First Name must not empty"];
        return NO;
    }else if ([details[@"email_id"] length] == 0){
        [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage: @"Email ID must not empty"];
        return NO;
    }else if (![details[@"email_id"] isEmailValidate]){
        [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage: @"Email ID is invalid"];
        return NO;
    }else if ([details[@"mobile"] length] == 0){
        [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage: @"Mobile number must not empty"];
        return NO;
    }else if (![details[@"mobile"] isValidateMobileNumber]){
        [self showAlertWithErrorTitle:@"Sign Up" andWithErrorMessage: @"Enter mobile number is invalid"];
        return NO;
    }
    
    return YES;
}
@end
