//
//  UpdateProfileController.h
//  BeautyApp
//
//  Created by Pramod on 23/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import <REFrostedViewController/REFrostedViewController.h>
#import "AppDelegate.h"
#import "AppointmentController.h"

@interface UpdateProfileController : BaseViewController<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *userNameTextField;
@property (nonatomic, weak) IBOutlet UITextField *userMobileTextField;
@property (nonatomic, weak) IBOutlet UITextField *userEmailIDTextField;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *genderLabelsArray;
@property (nonatomic, strong) NSString *maleFemailStr;

@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;
@end
