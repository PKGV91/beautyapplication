//
//  ProductViewController.h
//  BeautyApp
//
//  Created by Srikanthreddy on 27/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"

@interface ProductViewController : BaseViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UICollectionView *CollView;
@property (assign, nonatomic) NSInteger index;

@end
