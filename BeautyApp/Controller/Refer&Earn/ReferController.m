//
//  ReferController.m
//  BeautyApp
//
//  Created by Pramod on 05/11/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "ReferController.h"

@interface ReferController ()

@end

@implementation ReferController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backbtntapped:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
