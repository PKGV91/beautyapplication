//
//  LoginViewController.m
//  BeautyApp
//
//  Created by Pramod on 25/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    NSDictionary *params = @{@"first_name" : @"Srikanth",
//                             @"last_name" : @"Reddy",
//                             @"email_id" : @"ysreddy92@gmail.com",
//                             @"mobile_no" : @"9666522935",
//                             @"gender" : @"Male",
//                             @"city" : @"Hyderabad",
//                             @"dob" : @"18/03/1992"};
//    [[APIWrapper sharedAPIWrapper] userLoginServiceWithURLString:@"https://reema.digitalrupay.com/webservices/api/example/addUser" withParameters:params withComplitionBlock:^(NSDictionary *response, NSString *errorStr) {
//        
//    }];
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    //    [GIDSignIn sharedInstance].clientID = @"346992462460-ol85j5lbc0j68urec9leadjknkm31i6g.apps.googleusercontent.com";
    [GIDSignIn sharedInstance].clientID = @"294662580523-fej8b9q2qdlu78tubioslvck0lo46t8n.apps.googleusercontent.com";
    [GIDSignIn sharedInstance].shouldFetchBasicProfile = YES;
    
    [[GIDSignIn sharedInstance] setScopes:@[@"https://www.googleapis.com/auth/plus.stream.read", @"https://www.googleapis.com/auth/plus.me"]];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user
         withError:(NSError *)error {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"%@", user.profile.name);
        NSURL *imageURL;
        if ([GIDSignIn sharedInstance].currentUser.profile.hasImage) {
            imageURL = [user.profile imageURLWithDimension:200];
            NSLog(@"%@", imageURL);
        }
        NSData *data = [NSData dataWithContentsOfURL:imageURL];
//        self.parameters = @{@"email" : user.profile.email,
//                            @"name" : user.profile.name,
//                            @"fb_id":user.userID,
//                            @"image": [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength],
//                            @"gender" : @""};
//
//        [self checkEmailIDExistsORNotWithMailID:user.profile.email];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Button Actions

- (IBAction)doneButtonAction:(id)sender {
    [self.mobileNumberTextField resignFirstResponder];
}

- (IBAction)signInButtonAction:(UIButton *)sender {
    
    if ([self.mobileNumberTextField.text isValidateMobileNumber]) {
        [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
        [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
        [[APIWrapper sharedAPIWrapper] userLoginServiceWithURLString:LOGIN_URL withParameters:@{@"mobile_no" : self.mobileNumberTextField.text} withComplitionBlock:^(NSDictionary *response, NSString *errorStr) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[AppDelegate sharedAppDelegate].loadingView removeFromSuperview];
                if (response && [[response allKeys] containsObject:@"OTP"]) {
                    OTPController *otp = [[OTPController alloc] initWithNibName:@"OTPController" bundle:nil];
                    otp.mobileNumber = response[@"MobileNo"];
                    [self.navigationController pushViewController:otp animated:YES];
                }else if (response) {
                    
                }else {
                    [self showAlertWithErrorTitle:@"Login" andWithErrorMessage: errorStr];
                }
            });
            
        }];
    }else {
        [self showAlertWithErrorTitle:@"Login" andWithErrorMessage: MOBILE_NUMBER_ERROR];
    }
    
}

- (IBAction)signUpButtonAction:(UIButton *)sender {
    SignUpController *signUp = [[SignUpController alloc] initWithNibName:@"SignUpController" bundle:nil];
    [self presentViewController:signUp animated:YES completion:nil];
}

- (IBAction)signInWithFaceBookAction:(id)sender {
    [AppDelegate sharedAppDelegate].socailStr = @"FACEBOOK";
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"FB_TOKEN"] length] > 0) {
        [self otherDetails];
    }else {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        login.loginBehavior=FBSDKLoginBehaviorWeb;
        [login
         logInWithReadPermissions: @[@"public_profile",@"email",@"user_about_me",@"user_birthday",@"user_location",@"user_photos"]
         fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
             
             if (error) {
                 NSLog(@"Process error");
             } else if (result.isCancelled) {
                 NSLog(@"Cancelled");
             } else {
                 NSLog(@"Logged in");
             }
             FBSDKAccessToken *newToken = [FBSDKAccessToken currentAccessToken];
             [[NSUserDefaults standardUserDefaults] setObject:newToken.tokenString forKey:@"FB_TOKEN"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             if (![newToken.permissions containsObject:@"email"]) {
                 if (error) {
//                     [[ShowHUD sharedInstance] showHudUsingTextMessage:error.localizedDescription onView:self.view];
                     NSLog(@"Failed to login:%@", error);
                     
                 }
                 
                 FBSDKAccessToken *newToken = [FBSDKAccessToken currentAccessToken];
                 [[NSUserDefaults standardUserDefaults] setObject:newToken forKey:@"FB_TOKEN"];
                 if (![newToken.permissions containsObject:@"user_friends"]) {
//                     [[ShowHUD sharedInstance] showHudUsingTextMessage:@"You must login and grant access to your personal information and to use this feature" onView:self.view];
                     return;
                 }
             }
             [self otherDetails];
         }
         ];
    }
}

-(void)otherDetails{
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if ([FBSDKAccessToken currentAccessToken]) {
        
        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@"id,name,email,location,picture.type(large),cover,groups" forKey:@"fields"];
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters HTTPMethod:@"GET"]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 
                 NSLog(@"%@",result);
                 
                 if (result) {
                     NSData *imageData=[NSData dataWithContentsOfURL:[NSURL URLWithString:result[@"picture"][@"data"][@"url"]] options:NSDataReadingUncached error:&error];
                     if (![result valueForKey:@"email"]) {
//                         [MBProgressHUD hideHUDForView:self.view animated:YES];
//                         [[ShowHUD sharedInstance] showHudUsingTextMessage:@"Please login through email id" onView:self.view];
                         [self logoutFromFacebook];
                         return;
                     }
                     /*self.parameters = @{                                                   @"email":[result valueForKey:@"email"],
                                                                                            @"name":[result valueForKey:@"name"],
                                                                                            @"fb_id":[result valueForKey:@"id"],
                                                                                            @"image": [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]
                                                                                            };*/
                     
                     //                     [self loginWebServiceCall:YES withParameters:parameters withURL:LOGIN_VIA_SOCIAL];
//                     [self checkEmailIDExistsORNotWithMailID:[result valueForKey:@"email"]];
                     
                 }
             }else{
//                 [MBProgressHUD hideHUDForView:self.view animated:YES];
//                 [[ShowHUD sharedInstance] showHudUsingTextMessage:@"Failed to login through Facebook" onView:self.view];
                 NSLog(@"%@",[error localizedDescription]);
                 [self logoutFromFacebook];
             }
         }];
        
    }else {
        [self logoutFromFacebook];
    }
    
}

- (void)logoutFromFacebook {
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    [FBSDKAccessToken setCurrentAccessToken:nil];
}
- (IBAction)signInWithGoogleAction:(id)sender {
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [AppDelegate sharedAppDelegate].socailStr = @"FACEBOOK";
    [[GIDSignIn sharedInstance] hasAuthInKeychain];
    [[GIDSignIn sharedInstance] signIn];
}

#pragma mark UITextField Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.inputAccessoryView = self.toolBar;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}
@end
