//
//  LoginViewController.m
//  BeautyApp
//
//  Created by Pramod on 25/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.selectedContainerView.hidden = YES;
    self.selectedContainerView.frame = CGRectMake(20, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width - 40, 430);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self addUpdates];
}
- (void)addUpdates{
    self.selectedContainerView.frame = CGRectMake(20, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width - 40, 430);
    [self.selectedContainerView setNeedsUpdateConstraints];
    self.selectedContainerView.hidden = NO;
    [UIView animateWithDuration:0.4 animations:^{
        self.selectedContainerView.frame = CGRectMake(20, 20, [UIScreen mainScreen].bounds.size.width - 40, 430);
        [self.selectedContainerView layoutIfNeeded];
        self.selectedContainerView.center = self.view.center;
    }];
}

- (void)removeUpdatesWithTag:(int)tagValue{
    [UIView animateWithDuration:0.4 animations:^{
        self.selectedContainerView.frame = CGRectMake(20, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width - 40, [UIScreen mainScreen].bounds.size.height);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];
        if (tagValue == 1) {
            self.signUPController();
        }else if (tagValue == 2) {
            self.loginOTPController(self.mobileNumberTextField.text);
        }
    }];
}

#pragma mark Button Actions

- (IBAction)closeCancelButtonAction:(id)sender {
    [self removeUpdatesWithTag:0];
}
- (IBAction)doneButtonAction:(id)sender {
    [self.mobileNumberTextField resignFirstResponder];
}

- (IBAction)signInButtonAction:(UIButton *)sender {
    
    if ([self.mobileNumberTextField.text isValidateMobileNumber]) {
        [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
        [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
        [[APIWrappers sharedAPIWrapperInstance] userLoginServiceWithURLString:LOGIN_URL withParameters:@{@"mobile" : self.mobileNumberTextField.text} withComplitionBlock:^(NSDictionary *response, NSString *errorStr) {
            [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
            if (response) {
                [self removeUpdatesWithTag:2];
            }else {
                [self showAlertWithErrorTitle:@"Login" andWithErrorMessage: errorStr];
            }
        }];
    }else {
        [self showAlertWithErrorTitle:@"Login" andWithErrorMessage: MOBILE_NUMBER_ERROR];
    }
    
}

- (IBAction)signUpButtonAction:(UIButton *)sender {
//    SignUpController *signUp = [[SignUpController alloc] initWithNibName:@"SignUpController" bundle:nil];
//    [self.navigationController pushViewController:signUp animated:YES];
    [self removeUpdatesWithTag:1];
}


#pragma mark UITextField Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.inputAccessoryView = self.toolBar;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}
@end
