//
//  LoginViewController.h
//  BeautyApp
//
//  Created by Pramod on 25/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "SignUpController.h"
#import "OTPController.h"
#import "Constants.h"


typedef void(^ShowOTPControllerLogin)(NSString *);

typedef void(^ShowSignUpController)(void);
@interface LoginViewController : BaseViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTextField;
@property (strong, nonatomic) IBOutlet UIView *selectedContainerView;
@property (strong, nonatomic) ShowSignUpController signUPController;
@property (strong, nonatomic) ShowOTPControllerLogin loginOTPController;
@end
