//
//  PackagesController.h
//  BeautyApp
//
//  Created by Pramod on 14/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "CatDetailsCell.h"
#import "AppDelegate.h"
#import "LayerController.h"


@interface PackagesController : BaseViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tablePackage;
@property (nonatomic, weak) IBOutlet UILabel *priceServiceLabel;
@property (nonatomic, strong) NSMutableArray *packagesArray;
@property (nonatomic, strong) NSArray *searchArray;
@property (nonatomic, strong) NSMutableArray *indexPathsArray;
@property (nonatomic, assign) BOOL isFromSearch;
@property (nonatomic, strong) NSString *fromType;


@property (strong, nonatomic) IBOutlet UIView *myPackHideView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *myPackageHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *myPackageBottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *myPackageBottomHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *myPackageTopLabel;

@property (strong, nonatomic) IBOutlet UIView *packageCView;
@property (weak, nonatomic) IBOutlet UILabel *servicesListLabel;
@end
