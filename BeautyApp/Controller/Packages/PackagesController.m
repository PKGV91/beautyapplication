//
//  PackagesController.m
//  BeautyApp
//
//  Created by Pramod on 14/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "PackagesController.h"

@interface PackagesController ()

@end

@implementation PackagesController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_tablePackage registerNib:[UINib nibWithNibName:@"CatDetailsCell" bundle:nil] forCellReuseIdentifier:@"CatDetailsCell"];
    self.indexPathsArray = [[NSMutableArray alloc] init];
    if ([self.fromType isEqual:@"My"]) {
        self.myPackageTopLabel.text = @"My Packages";
        self.myPackHideView.hidden = self.myPackageBottomView.hidden = YES;
        self.myPackageHeightConstraint.constant = self.myPackageBottomHeightConstraint.constant = 0;
    }
    self.packageCView.frame = [UIScreen mainScreen].bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];
    if ([self.fromType isEqual:@""]) {
        [self getAllPackagesList];
    }else {
        [self postAllMyPackages:@{@"user_id" : [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER][@"user_id"]}];
    }
}
- (void)postAllMyPackages:(NSDictionary *)params {
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] postMyPackagesListWithURLString:POST_MY_PACKAGES_LIST withParameters:params withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.packagesArray = [response mutableCopy];
            [self.tablePackage reloadData];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}
- (void)getAllPackagesList {
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] getAllProductListWithURLString:GET_ALL_PACKAGES_LIST withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.packagesArray = [response mutableCopy];
            [self.tablePackage reloadData];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}


# pragma mark - Table Dalegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isFromSearch) {
        return self.searchArray.count;
    }
    return self.packagesArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CatDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CatDetailsCell"];
    NSDictionary *dictionary;
    if (self.isFromSearch) {
        dictionary = self.searchArray[indexPath.row];
    }else {
        dictionary = self.packagesArray[indexPath.row];
    }
    cell.subCatTitleLabel.text = dictionary[@"package_name"];
    cell.subCatDescLabel.text = dictionary[@"package_description"];
    cell.subCatPriceLabel.text = [NSString stringWithFormat:@"Rs. %@/-", dictionary[@"package_price"]];
    cell.subCatTimeLabel.text = [NSString stringWithFormat:@"%@ to %@", [self getDateStringFromString:dictionary[@"package_start_date"]], [self getDateStringFromString:dictionary[@"package_end_date"]]];
    cell.subCatTimeLabel.text = [cell.subCatTimeLabel.text stringByReplacingOccurrencesOfString:@"/" withString:@"'"];
    cell.servicesButton.tag = indexPath.row;
    [cell.servicesButton addTarget:self action:@selector(SButtonAction:) forControlEvents:UIControlEventTouchUpInside];
   /*UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator startAnimating];
    [indicator setCenter:cell.pImageView.center];
    [cell.pImageView addSubview:indicator];
    NSURL *currentImageURL;
    NSString *currentImageSTR = dictionary[@"after_img"];
    if ([currentImageSTR containsString:@"https://"]) {
        currentImageURL = [NSURL URLWithString:currentImageSTR];
    }else {
        currentImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:currentImageSTR]];
    }
    [cell.pImageView sd_setImageWithURL:currentImageURL placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
        if (!error) {
            [indicator removeFromSuperview];
            cell.pImageView.image = image;
        }else{
            [indicator removeFromSuperview];
        }
    }];
    cell.pImageView;*/
    if ([self.fromType isEqual:@"My"]) {
        cell.selectedButton.hidden = YES;
    }
    if ([self.indexPathsArray containsObject:dictionary]) {
        cell.selectedButton.selected = YES;
        [cell.selectedButton setImage:[UIImage imageNamed:@"check-selected"] forState:UIControlStateNormal];
    }else {
        cell.selectedButton.selected = NO;
        [cell.selectedButton setImage:[UIImage imageNamed:@"check-box-empty"] forState:UIControlStateNormal];
    }
    return cell;
}

- (NSString *)getDateStringFromString:(NSString *)dateStr {
    NSArray *splitArray = [dateStr componentsSeparatedByString:@"T"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [formatter dateFromString:splitArray[0]];
    [formatter setDateFormat:@"MMM/dd"];
    return [formatter stringFromDate:date];
}
- (void)SButtonAction:(UIButton *)sender {
    NSString *services = self.packagesArray[sender.tag][@"package_services"];
    
    NSArray *jsonObject = [NSJSONSerialization JSONObjectWithData:[services dataUsingEncoding:NSUTF8StringEncoding]
                                                          options:0 error:NULL];
    NSString *serString;
    for (NSDictionary *dic in jsonObject) {
        if (serString.length > 0) {
            serString = [NSString stringWithFormat:@"%@, %@", serString, dic[@"service_name"]];
        }else {
            serString = dic[@"service_name"];
        }
    }
    self.servicesListLabel.text = serString;
    [self.view addSubview:self.packageCView];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.fromType isEqual:@""]) {
        
        NSDictionary *dictionary;
        if (self.isFromSearch) {
            dictionary = self.searchArray[indexPath.row];
        }else {
            dictionary = self.packagesArray[indexPath.row];
        }
        if ([self.indexPathsArray containsObject:dictionary]) {
            [self.indexPathsArray removeObject:dictionary];
        }else {
            [self.indexPathsArray addObject:dictionary];
        }
        
        NSString *labelString;
        if (_indexPathsArray.count > 0) {
            int i = 0;
            for (NSDictionary *dic in self.indexPathsArray) {
                NSArray *priceArray = [dic[@"package_price"] componentsSeparatedByString:@" "];
                i = i + [priceArray[0] intValue];
            }
            labelString = [NSString stringWithFormat:@"%d Packages ₹ %d", (int)_indexPathsArray.count, i];
        }else {
            labelString = @"Select Services";
        }
        self.priceServiceLabel.text = labelString;
        [tableView beginUpdates];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [tableView endUpdates];
    }
}

//MARK: - TextField Delegates

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *search = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (search.length > 0) {
        self.isFromSearch = YES;
    }else {
        self.isFromSearch = NO;
    }
    [self getAllSearchResultWithString:search];
    return YES;
}

- (void)getAllSearchResultWithString:(NSString *)searchStr {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"package_name CONTAINS[C] %@ OR package_description CONTAINS[C] %@", searchStr, searchStr];
    self.searchArray = [self.packagesArray filteredArrayUsingPredicate:predicate];
    [self.tablePackage reloadData];
}
//MARK: - BUTTON ACTIONS
- (IBAction)backAction:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}

- (IBAction)selectedServicesList:(id)sender {
    if (_indexPathsArray.count > 0) {
        LayerController *login = [[LayerController alloc] initWithNibName:@"LayerController" bundle:nil];
        login.cellsArray = _indexPathsArray;
        login.typeOfController = FromTypePackages;
        login.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        dispatch_async(dispatch_get_main_queue(), ^{
            [ROOTVIEW presentViewController:login animated:NO completion:nil];
        });
    }
}
- (IBAction)proceedAction:(id)sender {
    NSDictionary *dictionary = [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER];
    if (dictionary) {
        AppointmentController *appointMent = [[AppointmentController alloc] initWithNibName:@"AppointmentController" bundle:nil];
        appointMent.greetingsArray = [AppDelegate sharedAppDelegate].categoriesArray;
        appointMent.isFromAssign = YES;
        appointMent.fromType = @"package";
        appointMent.packagesArray = self.indexPathsArray;
        [self.navigationController pushViewController:appointMent animated:YES];
        
    }else {
        [[AppDelegate sharedAppDelegate] showLoginController];
    }
}

- (IBAction)cancelButtonAction:(id)sender {
    [self.packageCView removeFromSuperview];
}

@end
