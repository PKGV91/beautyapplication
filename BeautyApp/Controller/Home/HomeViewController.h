//
//  HomeViewController.h
//  YallaPlay
//
//  Created by Pramod on 15/03/17.
//  Copyright © 2017 Pramod. All rights reserved.
//


//#import "MyBrowBarController.h"
#import "ShedulingViewController.h"
#import "ProductViewController.h"
//#import "UIImageView+Webcache.h"
#import <REFrostedViewController/REFrostedViewController.h>
#import "BaseViewController.h"
#import "AppDelegate.h"
#import "OursServicesCell.h"
#import "TestImageCell.h"
#import "BeautyCell.h"
#import "AppointmentController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PromotionsCell.h"
#import "APIWrappers.h"
#import "NewTestMonielCell.h"
#import "TipsController.h"
#import "MemberShipController.h"
#import "PackagesController.h"
#import "MarqueeLabel.h"
#import "DetailTestimonialController.h"
#import "ServicesController.h"


@interface HomeViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, weak) IBOutlet UITableView *typesOfTableView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet MarqueeLabel *footerLabel;
@property (nonatomic, weak) IBOutlet UIScrollView *headerScrollView;
@property (nonatomic, weak) IBOutlet UIScrollView *promotionsScrollView;
@property (weak, nonatomic) IBOutlet UIView *hideView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *scrollActivityIndicaterView;
@property (weak, nonatomic) IBOutlet UIImageView *headerBottomImageView;
@property (strong, nonatomic) IBOutlet UIView *promotionView;
@property (weak, nonatomic) IBOutlet UIImageView *promotionImageView;
@property (strong, nonatomic) IBOutlet UIView *sectionOneView;
@property (strong, nonatomic) IBOutlet UIView *sectionOneSubView;



@property (nonatomic, strong) NSArray *testiArray;
@property (nonatomic, strong) NSArray *beautyArray;
@property (nonatomic, strong) NSArray *imagesArray;
@property (nonatomic, strong) NSArray *promotionArray;
@property (nonatomic, strong) NSArray *hotDealsArray;

@end
