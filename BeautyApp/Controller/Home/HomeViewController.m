//
//  HomeViewController.m
//  YallaPlay
//
//  Created by Pramod on 15/03/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()


@property (nonatomic, strong) UILabel *lineLabel;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [_ourCollectView registerNib:[OursServicesCell nib] forCellWithReuseIdentifier:OursCellIdentifier];
//    [_testCollectionView registerNib:[UINib nibWithNibName:@"TestImageCell" bundle:nil] forCellWithReuseIdentifier:@"TestImageCell"];
//    [_BeautyCollectionView registerNib:[UINib nibWithNibName:@"BeautyCell" bundle:nil] forCellWithReuseIdentifier:@"BeautyCell"];
    
    self.headerView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 300);
    self.typesOfTableView.tableHeaderView = self.headerView;
    self.footerView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30);
    self.typesOfTableView.tableFooterView = self.footerView;
    self.promotionView.frame = [UIScreen mainScreen].bounds;
    [self.typesOfTableView registerNib:[PromotionsCell nib] forCellReuseIdentifier:PromotionCellIdentifier];
    [self.typesOfTableView registerNib:[NewTestMonielCell nib] forCellReuseIdentifier:NEW_TEST_IDENTIFIER];
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
//    [self.sectionOneView layoutIfNeeded];
//    CAShapeLayer *maskLayer = [CAShapeLayer layer];
//    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.sectionOneSubView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){10.0, 10.0}].CGPath;
//    self.sectionOneSubView.layer.mask = maskLayer;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.scrollActivityIndicaterView.hidden = YES;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"promotion_type == %@", @"4"];
    NSPredicate *promotionPredicate = [NSPredicate predicateWithFormat:@"promotion_type == %@", @"1"];
    NSPredicate *hotDealsPredicate = [NSPredicate predicateWithFormat:@"promotion_type == %@", @"2"];
    NSPredicate *scrollPredicate = [NSPredicate predicateWithFormat:@"promotion_type == %@", @"3"];
    NSSortDescriptor *sortDesripter = [[NSSortDescriptor alloc] initWithKey:@"promotion_id" ascending:NO];
    if ([AppDelegate sharedAppDelegate].promotionsArray.count > 0) {
        self.imagesArray = [[AppDelegate sharedAppDelegate].promotionsArray filteredArrayUsingPredicate:predicate];
        self.imagesArray = [self.imagesArray sortedArrayUsingDescriptors:@[sortDesripter]];
        self.promotionArray = [[AppDelegate sharedAppDelegate].promotionsArray filteredArrayUsingPredicate:promotionPredicate];
        self.hotDealsArray = [[AppDelegate sharedAppDelegate].promotionsArray filteredArrayUsingPredicate:hotDealsPredicate];
        self.promotionArray = [self.promotionArray sortedArrayUsingDescriptors:@[sortDesripter]];
        NSArray *array = [[AppDelegate sharedAppDelegate].promotionsArray filteredArrayUsingPredicate:scrollPredicate];
        NSString *scrollStr;
        for (NSDictionary *dictionaryScr in array) {
            if (scrollStr.length == 0) {
                scrollStr = dictionaryScr[@"promotion_description"];
            }else {
                scrollStr = [NSString stringWithFormat:@"%@, %@", scrollStr, dictionaryScr[@"promotion_description"]];
            }
        }
        self.footerLabel.text = scrollStr;
        [self setScrollWithImages];
        [self setPromotionScrollWithImages];
        [self.typesOfTableView reloadData];
    }else {
            self.scrollActivityIndicaterView.hidden = NO;
            [self.scrollActivityIndicaterView startAnimating];
            
            [[APIWrappers sharedAPIWrapperInstance] getPromotionsListWithURLString:GET_ALL_PROMOTIONS_LIST withComplitionBlock:^(NSArray *response, NSString *errorStr) {
                [self.scrollActivityIndicaterView stopAnimating];
                self.scrollActivityIndicaterView.hidesWhenStopped = YES;
                self.scrollActivityIndicaterView.hidden = YES;
                if (response) {
                    self.imagesArray = [response filteredArrayUsingPredicate:predicate];
                    self.promotionArray = [response filteredArrayUsingPredicate:promotionPredicate];
                    self.hotDealsArray = [response filteredArrayUsingPredicate:hotDealsPredicate];
                    self.imagesArray = [self.imagesArray sortedArrayUsingDescriptors:@[sortDesripter]];
                    self.promotionArray = [self.promotionArray sortedArrayUsingDescriptors:@[sortDesripter]];
                    NSArray *array = [[AppDelegate sharedAppDelegate].promotionsArray filteredArrayUsingPredicate:scrollPredicate];
                    NSString *scrollStr;
                    for (NSDictionary *dictionaryScr in array) {
                        if (scrollStr.length == 0) {
                            scrollStr = dictionaryScr[@"promotion_description"];
                        }else {
                            scrollStr = [NSString stringWithFormat:@"%@, %@", scrollStr, dictionaryScr[@"promotion_description"]];
                        }
                    }
                    self.footerLabel.text = scrollStr;
                    [self setScrollWithImages];
                    [self setPromotionScrollWithImages];
                    [self.typesOfTableView reloadData];
                }else {
                    [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
                }
            }];
    }
}


- (void)setScrollWithImages {
    
        // code here
        self.hideView.hidden = true;
        if (self.promotionArray.count > 0) {
            [self.promotionImageView sd_setImageWithURL:[NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:self.promotionArray[0][@"promotion_img"]]] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if (image) {
                    self.promotionImageView.image = image;
                    
                }
            }];
        }
        for (int i = 0; i < _imagesArray.count ; i++) {
            
            // create imageView
            UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((self.headerScrollView.frame.size.width * i ), 0, self.headerScrollView.frame.size.width, self.headerScrollView.frame.size.height)];
            // set scale to fill
            imgV.contentMode = UIViewContentModeScaleToFill;
            NSString *imageString = [NSString stringWithFormat:@"%@",_imagesArray[i][@"promotion_img"]];
            imageString =[BASE_IMAGE_URL stringByAppendingString:imageString];
            UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [indicator startAnimating];
            [indicator setCenter:imgV.center];
            [_headerScrollView addSubview:imgV];
            [imgV addSubview:indicator];
            [imgV sd_setImageWithURL:[NSURL URLWithString:imageString] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
                if (!error) {
                    [indicator removeFromSuperview];
                    imgV.image=image;
                }else{
                    [indicator removeFromSuperview];
                }
            }];
        }
        // set the content size to 10 image width
        [self.headerScrollView setContentSize:CGSizeMake( self.headerScrollView.frame.size.width * self.imagesArray.count, self.headerScrollView.frame.size.height)];
        // enable timer after each 2 seconds for scrolling.
        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
}

- (void)setPromotionScrollWithImages {
    [self.promotionsScrollView layoutIfNeeded];
    if (![[Constants sharedConstants] getstoredBoolFlagValuewithKey:SHOW_PROMOTION_FIRST_TIME]) {
        [[Constants sharedConstants] storeBoolFlagValue:YES withKey:SHOW_PROMOTION_FIRST_TIME];
        if (_promotionArray.count > 0) {
            [self performSelector:@selector(promotionAddUpdates) withObject:nil afterDelay:0.5];
        }
        for (int i = 0; i < _promotionArray.count ; i++) {
            
            // create imageView
            UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((self.promotionsScrollView.frame.size.width * i ), 0, self.promotionsScrollView.frame.size.width, self.promotionsScrollView.frame.size.height)];
            // set scale to fill
            UIButton *button = [[UIButton alloc] initWithFrame:imgV.frame];
            [button setTitle:@"" forState:UIControlStateNormal];
            [button addTarget:self action:@selector(showPackagesButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            imgV.contentMode = UIViewContentModeScaleAspectFit;
            imgV.backgroundColor = [UIColor blackColor];
            NSString *imageString = [NSString stringWithFormat:@"%@",_promotionArray[i][@"promotion_img"]];
            imageString =[BASE_IMAGE_URL stringByAppendingString:imageString];
            
            [_promotionsScrollView addSubview:imgV];
            [_promotionsScrollView addSubview:button];
            [imgV sd_setImageWithURL:[NSURL URLWithString:imageString] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
                if (!error) {
                    imgV.image = image;
                }else{
                }
            }];
        }
        
        // set the content size to 10 image width
        [self.promotionsScrollView setContentSize:CGSizeMake( self.promotionsScrollView.frame.size.width * self.promotionArray.count, self.promotionsScrollView.frame.size.height)];
        // enable timer after each 2 seconds for scrolling.
        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(promotionScrollTimer) userInfo:nil repeats:YES];
    }
    
}
//MARK: - PromotionView Animations
- (void)promotionAddUpdates {
    self.promotionsScrollView.hidden = NO;
    [[AppDelegate sharedAppDelegate].window addSubview:self.promotionView];
    [UIView animateWithDuration:0.5 delay:0.2 usingSpringWithDamping:0.6f initialSpringVelocity:1.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.promotionsScrollView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
    } completion:^(BOOL finished) {
        self.promotionsScrollView.transform = CGAffineTransformIdentity;
        [self.promotionView layoutIfNeeded];
    }];
}

- (void)promotionRemoveUpdates {
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.promotionsScrollView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
                     } completion:^(BOOL finished) {
                         self.promotionsScrollView.hidden = YES;
                         [self.promotionView removeFromSuperview];
                     }];
}

//MARK: - ScrollView Timer
- (void)promotionScrollTimer {
    // access the scroll view with the tag
    
    CGFloat contentOffset = self.promotionsScrollView.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/self.promotionsScrollView.frame.size.width) + 1 ;
    // if page is not 10, display it
    if( nextPage!= _promotionArray.count)  {
        
        [_promotionsScrollView scrollRectToVisible:CGRectMake(nextPage * _promotionsScrollView.frame.size.width,0 , _promotionsScrollView.frame.size.width, _promotionsScrollView.frame.size.height) animated:YES];
    } else {
        
        [_promotionsScrollView scrollRectToVisible:CGRectMake(0, 0, _promotionsScrollView.frame.size.width, _promotionsScrollView.frame.size.height) animated:YES];
    }
}
- (void)scrollingTimer {
    // access the scroll view with the tag
    
    CGFloat contentOffset = self.headerScrollView.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/self.headerScrollView.frame.size.width) + 1 ;
    // if page is not 10, display it
    if( nextPage!= _imagesArray.count)  {
        
        [_headerScrollView scrollRectToVisible:CGRectMake(nextPage * _headerScrollView.frame.size.width,0 , _headerScrollView.frame.size.width, _headerScrollView.frame.size.height) animated:YES];
    } else {
        
        [_headerScrollView scrollRectToVisible:CGRectMake(0, 0, _headerScrollView.frame.size.width, _headerScrollView.frame.size.height) animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.hideView.hidden = false;
    [[AppDelegate sharedAppDelegate] hideTabbar:NO];
    self.footerLabel.text = @"";
    self.footerLabel.marqueeType = MLContinuous;
    self.footerLabel.scrollDuration = 15.0;
    self.footerLabel.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    self.footerLabel.fadeLength = 10.0f;
    self.footerLabel.leadingBuffer = [UIScreen mainScreen].bounds.size.width - 100;
//    self.footerLabel.trailingBuffer = 20.0f;
    [self getAllTestimonialsList];
    [self getAllBeautyTipsList];
}


- (void)getAllTestimonialsList {
    NSDictionary *dictionary = [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER];
    NSDictionary *params;
    if (dictionary) {
        params = @{@"user_id" : dictionary[@"user_id"]};
//        params = @{};
    }else {
        params = @{};
    }
    [[AppDelegate sharedAppDelegate].window addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] getTestimonialsWithURLString:GET_All_Testimonials withParametes:params withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.testiArray = response;
            [self.typesOfTableView reloadData];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}

- (void)getAllBeautyTipsList {
    [[AppDelegate sharedAppDelegate].window addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] getTipsWithURLString:GET_BEAUTY_TIPS withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.beautyArray = response;
            [self.typesOfTableView reloadData];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}


#pragma mark Menu Action
- (IBAction)menuButtonAction:(id)sender {
    
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    //     Present the view controller

    [self.frostedViewController presentMenuViewController];
}

- (IBAction)headerViewButtonAction:(id)sender {
    AppointmentController *appointment = [[AppointmentController alloc] initWithNibName:@"AppointmentController" bundle:nil];
    appointment.categoryID = @"1";//[NSString stringWithFormat:@"%d", (int)self.categoriesArray[indexPath.row][@"cat_id"]];
    appointment.greetingsArray = [AppDelegate sharedAppDelegate].categoriesArray;
    [self.navigationController pushViewController:appointment animated:YES];
}

- (IBAction)promotionCloseAction:(id)sender {
    [self promotionRemoveUpdates];
}

- (void)viewAllButtonAction:(UIButton *)sender {
    TestController *mainVC = [[TestController alloc] initWithNibName:@"TestController" bundle:nil];
    mainVC.testimonialsArray = self.testiArray;
    [self.navigationController pushViewController:mainVC animated:YES];
}

- (void)showPackagesButtonAction:(UIButton *)sender {
    [self promotionRemoveUpdates];
    PackagesController *mainVC = [[PackagesController alloc] initWithNibName:@"PackagesController" bundle:nil];
    mainVC.fromType = @"";
    [self.navigationController pushViewController:mainVC animated:YES];
}
- (IBAction)smpButtonActions:(UIButton *)sender {
    if (sender.tag == 100) {
       // [self headerViewButtonAction:nil];
        ServicesController *mainVC = [[ServicesController alloc] initWithNibName:@"ServicesController" bundle:nil];
        [self.navigationController pushViewController:mainVC animated:YES];
    }else if (sender.tag == 101) {
        MemberShipController *mainVC = [[MemberShipController alloc] initWithNibName:@"MemberShipController" bundle:nil];
        [self.navigationController pushViewController:mainVC animated:YES];
        
    }else if (sender.tag == 102) {
        PackagesController *mainVC = [[PackagesController alloc] initWithNibName:@"PackagesController" bundle:nil];
        mainVC.fromType = @"";
        [self.navigationController pushViewController:mainVC animated:YES];
    }
}

//MARK: - TableView Datasource And Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.hotDealsArray.count > 3 ? 3 : self.hotDealsArray.count;
    }else if (section == 1 || section == 2) {
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        PromotionsCell *pCell = [tableView dequeueReusableCellWithIdentifier:PromotionCellIdentifier];
        pCell.viewAllTopConstraint.constant = pCell.viewAllHeightConstraint.constant = pCell.viewAllBottomConstraint.constant = 0;
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [indicator startAnimating];
        [indicator setCenter:pCell.promotionCellImageView.center];
        [pCell.promotionCellImageView addSubview:indicator];
        [pCell.promotionCellImageView sd_setImageWithURL:[NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:self.hotDealsArray[indexPath.row][@"promotion_img"]]] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
            if (!error) {
                [indicator removeFromSuperview];
                pCell.promotionCellImageView.image = image;
            }else{
                [indicator removeFromSuperview];
            }
        }];
        if (indexPath.row == 2) {
            pCell.viewAllTopConstraint.constant = 8;
            pCell.viewAllHeightConstraint.constant = 30;
            pCell.viewAllBottomConstraint.constant = 16;
        }
        cell = pCell;
        
    }else if (indexPath.section == 1 || indexPath.section == 2) {
        NewTestMonielCell *nCell = [tableView dequeueReusableCellWithIdentifier:NEW_TEST_IDENTIFIER];
        nCell.collectionTestMonialArray = indexPath.section == 1 ? [self.testiArray mutableCopy] : [self.beautyArray mutableCopy];
        nCell.fromType = indexPath.section == 1 ? @"testM" : @"";
        nCell.headingTitleLabel.text = indexPath.section == 1 ? @"Testimonails" : @"Beauty Tips";
        nCell.sideIconImageView.image = indexPath.section == 1 ? [UIImage imageNamed:@"testimonialsSelect"] : [UIImage imageNamed:@"beauty-tips"];
        nCell.viewAllButton.hidden = indexPath.section == 1 ? NO : YES;
        nCell.viewAllButton.tag = indexPath.section;
        nCell.showBeautyDetials = ^(NSDictionary *beautyDetails) {
            [self showTipsControllerForDictionary:beautyDetails];
        };
        nCell.showTestimonialDetials = ^(NSDictionary *testimonial) {
            [self showTestimonialDetailsForDictionary:testimonial];
        };
        [nCell.viewAllButton addTarget:self action:@selector(viewAllButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [nCell.collectionView reloadData];
        cell = nCell;
    }
    return cell;
}

- (void)showTipsControllerForDictionary:(NSDictionary *)dictioanry {
    TipsController *tips = [[TipsController alloc] initWithNibName:@"TipsController" bundle:nil];
    tips.tipsDictionary = dictioanry;
    [self.navigationController pushViewController:tips animated:YES];
}

- (void)showTestimonialDetailsForDictionary:(NSDictionary *)dictionary {
    DetailTestimonialController *controller = [[DetailTestimonialController alloc] initWithNibName:@"DetailTestimonialController" bundle:nil];
    controller.detailsDic = dictionary;
    [self.navigationController pushViewController:controller animated:YES];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view;
    if (section == 0) {
        self.sectionOneView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 48);
        view = self.sectionOneView;
    }
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 48;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 2) {
            return 174;
        }
        return 120;
    }else if (indexPath.section == 1 || indexPath.section == 2) {
        if (indexPath.section == 1 && self.testiArray.count == 0) {
            return 77;
        }
        if (indexPath.section == 2 && self.beautyArray.count == 0) {
            return 77;
        }
        if (indexPath.section == 2) {
            return 198;
        }
        return 220;
    }
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == self.hotDealsArray.count - 1) {
        CAShapeLayer * maskLayer = [CAShapeLayer layer];
        
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: cell.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){10.0, 10.0}].CGPath;
        
        cell.contentView.layer.mask = maskLayer;
    }
}

//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    if (collectionView.tag ==1) {
//        return self.categoriesArray.count;
//    }else if(collectionView.tag == 2){
//        return 10;
//    }else{
//    return self.beautyArray.count;
//}
//}
//
//- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    if (collectionView.tag == 1) {
//        OursServicesCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:OursCellIdentifier forIndexPath:indexPath];
//        cell.categoryTitleLabel.text = self.categoriesArray[indexPath.row][@"category_name"];
//        [cell.categoryImageView sd_setImageWithURL:[NSURL URLWithString:self.categoriesArray[indexPath.row][@"cat_img"]] placeholderImage:[UIImage imageNamed:@""]];
//        return cell;
//
//    }else if (collectionView.tag == 2){
//        TestImageCell *testiCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TestImageCell" forIndexPath:indexPath];
//        return testiCell;
//
//    }else{
//        BeautyCell *BCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BeautyCell" forIndexPath:indexPath];
//        BCell.lblBTitle.text = _beautyArray[indexPath.row][@"tip_title"];
//        [BCell.beautyImage sd_setImageWithURL:[NSURL URLWithString:self.beautyArray[indexPath.row][@"tip_img"]] placeholderImage:[UIImage imageNamed:@""]];
//        return BCell;
//
//
//}
//}
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    if (collectionView.tag == 2) {
//        return CGSizeMake(collectionView.bounds.size.width/3-15, collectionView.bounds.size.height/3);
//    }else{
//        return CGSizeMake(collectionView.bounds.size.height, collectionView.bounds.size.height);
//}
//}
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    AppointmentController *appointment = [[AppointmentController alloc] initWithNibName:@"AppointmentController" bundle:nil];
//    appointment.categoryID = @"1";//[NSString stringWithFormat:@"%d", (int)self.categoriesArray[indexPath.row][@"cat_id"]];
//    [self.navigationController pushViewController:appointment animated:YES];
//}


@end
