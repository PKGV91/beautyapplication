//
//  SettingsController.m
//  BeautyApp
//
//  Created by Srikanthreddy on 23/10/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "SettingsController.h"
#import "AppDelegate.h"
@interface SettingsController ()

@end

@implementation SettingsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sideMenuButtonAction:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
    
}


@end
