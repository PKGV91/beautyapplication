//
//  LayerController.h
//  BeautyApp
//
//  Created by Pramod on 16/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "LayerCell.h"
#import "ALayerCell.h"
typedef enum TypeFromController : NSInteger {
    FromTypePackages,
    FromTypeMemberShip,
    FromTypeAppointment
}TypeFromController;
@interface LayerController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *layerTableView;
@property (nonatomic, weak) IBOutlet UILabel *topTitleLabel;
@property (nonatomic, strong) NSArray *cellsArray;
@property (nonatomic, assign) TypeFromController typeOfController;

@end
