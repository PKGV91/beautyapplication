//
//  LayerController.m
//  BeautyApp
//
//  Created by Pramod on 16/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "LayerController.h"

@interface LayerController ()

@end

@implementation LayerController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (_typeOfController == FromTypePackages) {
        self.topTitleLabel.text = @"Packages";
    }else if (_typeOfController == FromTypeMemberShip) {
        self.topTitleLabel.text = @"Memberships";
    }else if (_typeOfController == FromTypeAppointment) {
        self.topTitleLabel.text = @"Services";
    }
    [self.layerTableView registerNib:[LayerCell nib] forCellReuseIdentifier:LayerCellIdentifier];
    [self.layerTableView registerNib:[ALayerCell nib] forCellReuseIdentifier:ANewLayerCellIdentifier];
    self.layerTableView.estimatedRowHeight = 100;
    self.layerTableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self dismissViewControllerAnimated:NO completion:nil];
}

//MARK: - ButtonAction
- (IBAction)closeButtonAction:(id)sender {
   [self dismissViewControllerAnimated:NO completion:nil];
}
//MARK: - TableView Datasource And Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cellsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    LayerCell *lCell = [tableView dequeueReusableCellWithIdentifier:LayerCellIdentifier];
    ALayerCell *aNewCell = [tableView dequeueReusableCellWithIdentifier:ANewLayerCellIdentifier];
    NSDictionary *dictionary = self.cellsArray[indexPath.row];
    if (_typeOfController == FromTypePackages) {

        lCell.itemTitleLabel.text = dictionary[@"package_name"];
        lCell.itemDescLabel.text = dictionary[@"package_description"];
        lCell.itemPriceLabel.text = [NSString stringWithFormat:@"Rs. %@/-", dictionary[@"package_price"]];
        lCell.itemDateExpLabel.text = [NSString stringWithFormat:@"%@ to %@", [self getDateStringFromString:dictionary[@"package_start_date"]], [self getDateStringFromString:dictionary[@"package_end_date"]]];
        lCell.itemDateExpLabel.text = [lCell.itemDateExpLabel.text stringByReplacingOccurrencesOfString:@"/" withString:@"'"];
        cell = lCell;
    }else if (_typeOfController == FromTypeMemberShip) {
        
        lCell.itemTitleLabel.text = dictionary[@"membership_name"];
        lCell.itemDescLabel.text = dictionary[@"membership_description"];
        lCell.itemPriceLabel.text = [NSString stringWithFormat:@"Rs. %@/-", dictionary[@"membership_price"]];
        lCell.itemDateExpLabel.text = [NSString stringWithFormat:@"Valid: %ld Days", [dictionary[@"membership_validity_in_days"] integerValue]];
        lCell.itemDateExpLabel.text = [lCell.itemDateExpLabel.text stringByReplacingOccurrencesOfString:@"/" withString:@"'"];
        cell = lCell;
    }else if (_typeOfController == FromTypeAppointment) {
        aNewCell.itemTitleLabel.text = dictionary[@"service_name"];
        aNewCell.itemDateExpLabel.text = dictionary[@"service_duration"];
        aNewCell.itemPriceLabel.text = [NSString stringWithFormat:@"₹ %@ INR", dictionary[@"service_price"]];
        
        cell = aNewCell;
    }
    return cell;
}



- (NSString *)getDateStringFromString:(NSString *)dateStr {
    NSArray *splitArray = [dateStr componentsSeparatedByString:@"T"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [formatter dateFromString:splitArray[0]];
    [formatter setDateFormat:@"MMM/dd"];
    return [formatter stringFromDate:date];
}
@end
