//
//  SideMenuCell.m
//  Rxpress
//
//  Created by Rxpress on 11/02/17.
//  Copyright © 2017 rxpress.com. All rights reserved.
//

#import "SideMenuCell.h"

@implementation SideMenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+ (UINib *)nib{
    return [UINib nibWithNibName:@"SideMenuCell" bundle:nil];
}
@end
