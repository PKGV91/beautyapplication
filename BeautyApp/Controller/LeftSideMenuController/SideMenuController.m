//
//  SideMenuController.m
//  Rxpress
//
//  Created by Rxpress on 11/02/17.
//  Copyright © 2017 rxpress.com. All rights reserved.
//

#import "SideMenuController.h"
#import "ReferController.h"
#import "PackagesController.h"


@interface SideMenuController ()

@end

@implementation SideMenuController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.headerView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 100);
    
    self.menuTableView.tableHeaderView = self.headerView;
    [self.menuTableView registerNib:[SideMenuCell nib] forCellReuseIdentifier:SideMenuIdentifier];
    if ([UIScreen mainScreen].bounds.size.width >= 415.0) {
        self.menuTableView.scrollEnabled = NO;
    }else {
        self.menuTableView.scrollEnabled = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.menuTableView setContentOffset:CGPointZero animated:YES];
    self.navigationController.navigationBarHidden = YES;
    
    NSDictionary *dictionary = [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER];
    if (dictionary) {
        self.headerView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 150);
        self.headerButton.hidden = YES;
        self.walledView.hidden = NO;
        self.walletVeiwHeightConstraint.constant = 40;
        self.underLineLabelHeightConstraint.constant = 1;
        self.userNameLabel.text = dictionary[@"fullname"];
        self.walletLabel.text = [NSString stringWithFormat:@"Your Wallet Balance\n%@", dictionary[@"points"]];
        _menuItems = @[ @"Home",@"My Wallet", @"Update Profile", @"Transaction History", @"Notification Promotion", @"Testimonials", @"My Testimonials",@"Offers", @"Settings", @"Buy Packages", @"My Packages", @"Buy Services", @"Logout"];
        _menuImages = @[@"home",@"wallet", @"updateprofile", @"transactionhistoy", @"bell", @"Buyservices", @"home",@"settings",@"settings", @"pack", @"pack", @"Buyservices", @"logout"];
    }else {
        self.headerView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 100);
        self.headerButton.hidden = NO;
        self.walledView.hidden = YES;
        self.walletVeiwHeightConstraint.constant = self.underLineLabelHeightConstraint.constant = 0;
        self.userNameLabel.text = @"Login";
        _menuItems = @[ @"Home",@"Notification Promotion", @"Testimonials", @"Coupons", @"Buy Packages", @"Buy Services"];
        _menuImages = @[@"home",@"bell", @"settings", @"settings", @"pack", @"Buyservices"];
    }
    [self.menuTableView reloadData];
    //    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@", [dictionary[FIRST_NAME] length] > 0 ? dictionary[FIRST_NAME] : @"", [dictionary[LAST_NAME] length] > 0 ? dictionary[LAST_NAME] : @""];
    //    self.numberLabel.text = dictionary[@"Contact_Number"];
    //    if ([[dictionary allKeys] containsObject:@"userImage"]) {
    //        if (![dictionary[@"userImage"] isKindOfClass:[NSNull class]] && ![dictionary[@"userImage"] isEqual:@""]) {
    //
    //            self.profilePicImageView.image = [self decodeBase64ToImage:dictionary[@"userImage"]];
    //        }else {
    //            self.profilePicImageView.image = [UIImage imageNamed:@"dummyIcon"];
    //        }
    //    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//MARK: - ButtonActions
- (IBAction)headingButtonAction:(id)sender {
    [[AppDelegate sharedAppDelegate] showLoginController];
}

- (IBAction)closeMySideMenuAction:(id)sender {
    self.hideMyMenu(nil);
}

#pragma mark UITableViewDatasource & Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.menuItems.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:SideMenuIdentifier];
    UIImage *image = [UIImage imageNamed:_menuImages[indexPath.row]];
    cell.cellSideLabel.text = _menuItems[indexPath.row];
    cell.cellImageView.image = image;//[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    [cell.cellImageView setTintColor:[self colorFromHexString:@"#0097A7"]];
//    [UIColor colorWithRed:(29/255.0) green:(76/255.0) blue:(80/255.0) alpha:1.0f]
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIViewController *viewController;
    NSDictionary *dictionary = [[Constants sharedConstants] getDictioanryForKey:USER_MOBILE_NUMBER];
    if (dictionary) {
        viewController = [self selectControllerForIndex:indexPath.row];
    }else {
        viewController = [self showViewControllersForWithOutLoginForIndexPathRow:indexPath.row];
    }
    if (indexPath.row != 12) {
//        if (indexPath.row == 0) {
        if (viewController) {
            self.frostedViewController.contentViewController = viewController;
        }
        
//        }else {
//            [self.navigationController pushViewController:viewController animated:YES];
//        }
    }
    [self.frostedViewController hideMenuViewController];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 48;
}


- (IBAction)profileButtonActio:(id)sender {
//    ProfileFullViewController *profile = [[ProfileFullViewController alloc] initWithNibName:@"ProfileFullViewController" bundle:nil];
//    [self.frostedViewController hideMenuViewController];
//    [self.navigationController pushViewController:profile animated:YES];
}

- (UIViewController *)selectControllerForIndex:(NSInteger)row {
    UIViewController *viewController;
    
    if (row == 0) {
        HomeViewController *mainVC = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 1) {
        ReferController *mainVC = [[ReferController alloc] initWithNibName:@"ReferController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 2) {
        UpdateProfileController *mainVC = [[UpdateProfileController alloc] initWithNibName:@"UpdateProfileController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 3) {
        TransactionHisController *mainVC = [[TransactionHisController alloc] initWithNibName:@"TransactionHisController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 4) {
        NotificationController *mainVC = [[NotificationController alloc] initWithNibName:@"NotificationController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 5) {
        TestController *mainVC = [[TestController alloc] initWithNibName:@"TestController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 6) {
        TestController *mainVC = [[TestController alloc] initWithNibName:@"TestController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 7) {
        OfferViewController *mainVC = [[OfferViewController alloc] initWithNibName:@"OfferViewController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 8) {
        SettingsController *mainVC = [[SettingsController alloc] initWithNibName:@"SettingsController" bundle:nil];
        viewController = mainVC;
     
        
    }else if (row == 9) {
        PackagesController *mainVC = [[PackagesController alloc] initWithNibName:@"PackagesController" bundle:nil];
        mainVC.fromType = @"";
        viewController = mainVC;
        
    }else if (row == 10) {
        PackagesController *mainVC = [[PackagesController alloc] initWithNibName:@"PackagesController" bundle:nil];
        mainVC.fromType = @"My";
        viewController = mainVC;
        
    }else if (row == 11) {
        AppointmentController *appointment = [[AppointmentController alloc] initWithNibName:@"AppointmentController" bundle:nil];
        appointment.categoryID = @"1";
        appointment.greetingsArray = [AppDelegate sharedAppDelegate].categoriesArray;
        viewController = appointment;
        
    }else {
        [[Constants sharedConstants] removeStoredValueForKey:USER_MOBILE_NUMBER];
        return nil;
    }
    
    return viewController;
}

- (UIViewController *)showViewControllersForWithOutLoginForIndexPathRow:(NSInteger)row {
    UIViewController *viewController;
    if (row == 0) {
        HomeViewController *mainVC = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 1) {
        HomeViewController *mainVC = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 2) {
        TestController *mainVC = [[TestController alloc] initWithNibName:@"TestController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 3) {
        HomeViewController *mainVC = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        viewController = mainVC;
    }else if (row == 4) {
        PackagesController *mainVC = [[PackagesController alloc] initWithNibName:@"PackagesController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 5) {
        AppointmentController *appointment = [[AppointmentController alloc] initWithNibName:@"AppointmentController" bundle:nil];
        appointment.categoryID = @"1";
        appointment.greetingsArray = [AppDelegate sharedAppDelegate].categoriesArray;
        viewController = appointment;
        
    }else if (row == 6) {
        TestController *mainVC = [[TestController alloc] initWithNibName:@"TestController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 7) {
        SettingsController *mainVC = [[SettingsController alloc] initWithNibName:@"SettingsController" bundle:nil];
        viewController = mainVC;
        
    }else if (row == 8) {
        UpdateProfileController *mainVC = [[UpdateProfileController alloc] initWithNibName:@"UpdateProfileController" bundle:nil];
        viewController = mainVC;
        
    }
    return viewController;
}

@end
