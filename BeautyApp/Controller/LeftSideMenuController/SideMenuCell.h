//
//  SideMenuCell.h
//  Rxpress
//
//  Created by Rxpress on 11/02/17.
//  Copyright © 2017 rxpress.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#define SideMenuIdentifier @"SideMenuIdentity"

@interface SideMenuCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *cellImageView;
@property (nonatomic, weak) IBOutlet UILabel *cellSideLabel;


+ (UINib *)nib;
@end
