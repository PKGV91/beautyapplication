//
//  SideMenuController.h
//  Rxpress
//
//  Created by Rxpress on 11/02/17.
//  Copyright © 2017 rxpress.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideMenuCell.h"
#import <REFrostedViewController/REFrostedViewController.h>
#import "BaseViewController.h"
#import "UIColor+_Additions.h"
#import "UpdateProfileController.h"
#import "TransactionHisController.h"
#import "ReferController.h"
#import "NotificationController.h"
#import "SettingsController.h"
#import "TestController.h"
#import "LoginViewController.h"
#import "SignUpController.h"
#import "OTPController.h"
#import "PackagesController.h"

typedef void(^HideMyMenuController)(UIViewController *);
@interface SideMenuController : BaseViewController<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *menuTableView;
@property (nonatomic, strong) NSArray *menuItems, *menuImages;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *emailLabel;
@property (nonatomic, weak) IBOutlet UIImageView *profilePicImageView;
@property (weak, nonatomic) IBOutlet UIButton *headerButton;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UIView *walledView;
@property (weak, nonatomic) IBOutlet UILabel *walletLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *walletVeiwHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *underLineLabelHeightConstraint;

@property (nonatomic, strong) HideMyMenuController hideMyMenu;
@end
