//
//  MemberShipController.h
//  BeautyApp
//
//  Created by Pramod on 16/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"
#import "CatDetailsCell.h"
#import "LayerController.h"

@interface MemberShipController : BaseViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableMember;
@property (nonatomic, weak) IBOutlet UILabel *priceServiceLabel;
@property (nonatomic, strong) NSArray *searchArray;
@property (nonatomic, strong) NSMutableArray *membershipArray;
@property (nonatomic, strong) NSMutableArray *indexPathsArray;
@property (nonatomic, assign) BOOL isFromSearch;
@end
