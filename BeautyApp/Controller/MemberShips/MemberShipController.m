//
//  MemberShipController.m
//  BeautyApp
//
//  Created by Pramod on 16/12/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import "MemberShipController.h"

@interface MemberShipController ()

@end

@implementation MemberShipController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_tableMember registerNib:[UINib nibWithNibName:@"CatDetailsCell" bundle:nil] forCellReuseIdentifier:@"CatDetailsCell"];
    self.indexPathsArray = [[NSMutableArray alloc] init];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideTabbar:YES];
    [self getAllMemberShipList];
}

- (void)getAllMemberShipList {
    [self.view addSubview:[AppDelegate sharedAppDelegate].loadingView];
    [[AppDelegate sharedAppDelegate].loadingView setLoaderOnCurrentView];
    [[APIWrappers sharedAPIWrapperInstance] getAllPackagesListWithURLString:GET_ALL_MEMBERSHIP_LIST withComplitionBlock:^(NSArray *response, NSString *errorStr) {
        [[AppDelegate sharedAppDelegate].loadingView remveLoaderFromCurrentView];
        if (response) {
            self.membershipArray = [response mutableCopy];
            [self.tableMember reloadData];
        }else {
            [self showAlertWithErrorTitle:@"" andWithErrorMessage:errorStr];
        }
    }];
}

# pragma mark - Table Dalegates
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isFromSearch) {
        return self.searchArray.count;
    }
    return self.membershipArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
        CatDetailsCell *cell =[tableView dequeueReusableCellWithIdentifier:@"CatDetailsCell"];
    
    NSDictionary *dictionary;
    if (self.isFromSearch) {
        dictionary = self.searchArray[indexPath.row];
    }else {
        dictionary = self.membershipArray[indexPath.row];
    }
    cell.subCatTitleLabel.text = dictionary[@"membership_name"];
    cell.subCatDescLabel.text = dictionary[@"membership_description"];
    cell.subCatPriceLabel.text = [NSString stringWithFormat:@"Rs. %@/-", dictionary[@"membership_price"]];
    cell.subCatTimeLabel.text = [NSString stringWithFormat:@"Valid: %ld Days", [dictionary[@"membership_validity_in_days"] integerValue]];
    cell.subCatTimeLabel.text = [cell.subCatTimeLabel.text stringByReplacingOccurrencesOfString:@"/" withString:@"'"];
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
     [indicator startAnimating];
     [indicator setCenter:cell.pImageView.center];
     [cell.pImageView addSubview:indicator];
     NSURL *currentImageURL;
     NSString *currentImageSTR = dictionary[@"membership_img"];
     if ([currentImageSTR containsString:@"https://"]) {
     currentImageURL = [NSURL URLWithString:currentImageSTR];
     }else {
     currentImageURL = [NSURL URLWithString:[BASE_IMAGE_URL stringByAppendingString:currentImageSTR]];
     }
    [cell.pImageView sd_setImageWithURL:currentImageURL placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage*image,NSError*error,SDImageCacheType cacheType,NSURL*imageURL)  {
        if (!error) {
            [indicator removeFromSuperview];
            cell.pImageView.image = image;
        }else{
            [indicator removeFromSuperview];
        }
    }];
    
    if ([self.indexPathsArray containsObject:dictionary]) {
        cell.selectedButton.selected = YES;
        [cell.selectedButton setImage:[UIImage imageNamed:@"check-selected"] forState:UIControlStateNormal];
    }else {
        cell.selectedButton.selected = NO;
        [cell.selectedButton setImage:[UIImage imageNamed:@"check-box-empty"] forState:UIControlStateNormal];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dictionary;
    if (self.isFromSearch) {
        dictionary = self.searchArray[indexPath.row];
    }else {
        dictionary = self.membershipArray[indexPath.row];
    }
    if ([self.indexPathsArray containsObject:dictionary]) {
        [self.indexPathsArray removeObject:dictionary];
    }else {
        [self.indexPathsArray addObject:dictionary];
    }
    
    NSString *labelString;
    if (_indexPathsArray.count > 0) {
        int i = 0;
        for (NSDictionary *dic in self.indexPathsArray) {
            NSArray *priceArray = [dic[@"membership_price"] componentsSeparatedByString:@" "];
            i = i + [priceArray[0] intValue];
        }
        labelString = [NSString stringWithFormat:@"%d Memberships ₹ %d", (int)_indexPathsArray.count, i];
    }else {
        labelString = @"Select MemberShips";
    }
    self.priceServiceLabel.text = labelString;
    [tableView beginUpdates];
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [tableView endUpdates];
}

//MARK: - TextField Delegates

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *search = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (search.length > 0) {
        self.isFromSearch = YES;
    }else {
        self.isFromSearch = NO;
    }
    [self getAllSearchResultWithString:search];
    return YES;
}

- (void)getAllSearchResultWithString:(NSString *)searchStr {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"membership_name CONTAINS[C] %@ OR membership_description CONTAINS[C] %@", searchStr, searchStr];
    self.searchArray = [self.membershipArray filteredArrayUsingPredicate:predicate];
    [self.tableMember reloadData];
}

//MARK: - ButtonAction
- (IBAction)backAction:(id)sender {
    [[AppDelegate sharedAppDelegate] buildSideMenuViewController:0];
}

- (IBAction)selectedServicesList:(id)sender {
    if (_indexPathsArray.count > 0) {
        LayerController *login = [[LayerController alloc] initWithNibName:@"LayerController" bundle:nil];
        login.cellsArray = _indexPathsArray;
        login.typeOfController = FromTypeMemberShip;
        login.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        dispatch_async(dispatch_get_main_queue(), ^{
            [ROOTVIEW presentViewController:login animated:NO completion:nil];
        });
    }
}
@end
