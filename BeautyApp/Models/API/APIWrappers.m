//
//  APIWrappers.m
//  Rxpress
//
//  Created by Rxpress on 19/02/17.
//  Copyright © 2017 rxpress.com. All rights reserved.
//

#import "APIWrappers.h"

@implementation APIWrappers


static APIWrappers *wrapper = nil;

+ (APIWrappers *)sharedAPIWrapperInstance {
    if (!wrapper) {
        wrapper = [[self alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
    return wrapper;
}
//MARK:- Post Method
- (void)postResponseFromUrl:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(id response, NSError *error))complitionBlock{
    wrapper.requestSerializer = [AFJSONRequestSerializer serializer];
    [wrapper.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [wrapper POST:urlStr parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        complitionBlock(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complitionBlock(nil, error);
    }];
    
}

//MARK: - Post with Body Method

- (void)postWithBodyResponseFromURL:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(id response, NSError *error))complitionBlock {
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlStr parameters:nil error:nil];
    
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [[wrapper dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            complitionBlock(responseObject, nil);
        } else {
            complitionBlock(nil, error);
        }
    }] resume];
}
//MARK: - Get Method

- (void)getResponseFromUrl:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(id response, NSError *error))complitionBlock{
    [wrapper GET:urlStr parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        complitionBlock(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complitionBlock(nil, error);
    }];
}

//MARK: - POST_BOOKING_CONFIRMATION

- (void)confirmBookingWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock {
    [self postResponseFromUrl:urlStr withParameters:params withComplitionBlock:^(id response, NSError *error) {
        NSInteger status = [response[@"status"] integerValue];
        if (status == 1) {
            complitionBlock(response, nil);
        }else if ((status == 1 || status == 0) && [response count] > 0) {
            complitionBlock(nil, response[@"message"]);
        }else {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK: - POST_PAYMENT_CONFIRMATION

- (void)confirmBookingPayementWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock {
    [self postWithBodyResponseFromURL:urlStr withParameters:params withComplitionBlock:^(id response, NSError *error) {
        NSInteger status = [response[@"status"] integerValue];
        if (status == 1) {
            complitionBlock(response, nil);
        }else if ((status == 1 || status == 0) && [response count] > 0) {
            complitionBlock(nil, response[@"message"]);
        }else {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}
//MARK:LOGIN
- (void)userLoginServiceWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock {
    [self postWithBodyResponseFromURL:urlStr withParameters:params withComplitionBlock:^(id response, NSError *error) {
        NSInteger status = [response[@"status"] integerValue];
        if (status == 1 && [[response allKeys] containsObject:@"otp"]) {
            complitionBlock(response, nil);
        }else if (status == 1 || status == 0) {
            complitionBlock(nil, response[@"message"]);
        }else {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK:SIGN_UP
- (void)userSignUpServiceWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock {
    [self postWithBodyResponseFromURL:urlStr withParameters:params withComplitionBlock:^(id response, NSError *error) {
        NSInteger status = [response[@"status"] integerValue];
        if (status == 1 && [[response allKeys] containsObject:@"data"]) {
            complitionBlock(response[@"message"], nil);
        }else if (status == 1 || status == 0) {
            complitionBlock(nil, response[@"message"]);
        }else {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK:SIGN_UP_OTP
- (void)userOTPServiceWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock {
    [self postResponseFromUrl:urlStr withParameters:params withComplitionBlock:^(id response, NSError *error) {
        NSInteger status = [response[@"status"] integerValue];
        if (status == 1 && [[response allKeys] containsObject:@"data"]) {
            complitionBlock(response[@"data"][0], nil);
        }else if (status == 1 || status == 0) {
            complitionBlock(nil, response[@"message"]);
        }else {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK: - GET_ALL_PROMOTIONS
- (void)getPromotionsListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock {
    [self getResponseFromUrl:url withParameters:nil withComplitionBlock:^(id response, NSError *error) {
        if (response) {
            complitionBlock(response[@"data"], nil);
        }else{
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK : _ GET_ALL_TESTIMONIALS
- (void)getTestimonialsWithURLString:(NSString *)url withParametes:(NSDictionary *)params withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock {
    [self postResponseFromUrl:url withParameters:params withComplitionBlock:^(id response, NSError *error) {
        if (response) {
            complitionBlock(response[@"data"], nil);
        }else{
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK: - TESTIMONIAL_LIKE
- (void)userLikeTestimonialWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock {
    [self postResponseFromUrl:urlStr withParameters:params withComplitionBlock:^(id response, NSError *error) {
        NSInteger status = [response[@"status"] integerValue];
        if (status == 1) {
            complitionBlock(response, nil);
        }else if (status == 1 || status == 0) {
            complitionBlock(nil, response[@"message"]);
        }else {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK: - GET_ALL_BEAUTY_TIPS
- (void)getTipsWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock {
    [self getResponseFromUrl:url withParameters:nil withComplitionBlock:^(id response, NSError *error) {
        if (response) {
            complitionBlock(response[@"data"], nil);
        }else{
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}


//MARK: - GET_ALL_CATEGORY_LIST
- (void)getCategoryListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock {
    [self getResponseFromUrl:url withParameters:nil withComplitionBlock:^(id response, NSError *error) {
        if (response) {
            complitionBlock(response[@"data"], nil);
        }else{
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK: - GET_SUB_CATEGOTY_WITH_CATEGORY_ID
- (void)getSubCategoryListWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock {
    [self postWithBodyResponseFromURL:urlStr withParameters:params withComplitionBlock:^(id response, NSError *error) {
        NSInteger status = [response[@"status"] integerValue];
        if (status == 1 && [[response allKeys] containsObject:@"data"] && [response[@"data"] count] > 0) {
            complitionBlock(response[@"data"], nil);
        }else if (status == 1 || status == 0) {
            complitionBlock(nil, @"No Details found");
        }else {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK: - GET_ALL_PRODUCT_LIST
- (void)getAllProductListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock {
    [self getResponseFromUrl:url withParameters:nil withComplitionBlock:^(id response, NSError *error) {
        if (response) {
            complitionBlock(response[@"data"], nil);
        }else{
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK: - GET_ALL_PACKAGES
- (void)getAllPackagesListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock; {
    [self getResponseFromUrl:url withParameters:nil withComplitionBlock:^(id response, NSError *error) {
        if (response) {
            complitionBlock(response[@"data"], nil);
        }else{
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK: - GET_ALL_MEMBERSHIP
- (void)getAllMemberShipListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock {
    [self getResponseFromUrl:url withParameters:nil withComplitionBlock:^(id response, NSError *error) {
        if (response) {
            complitionBlock(response[@"data"], nil);
        }else{
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK:UPDATE_USER_PROFILE
- (void)updateUserProfileServiceWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock {
    [self postWithBodyResponseFromURL:urlStr withParameters:params withComplitionBlock:^(id response, NSError *error) {
        NSInteger status = [response[@"status"] integerValue];
        if (status == 1 && [[response allKeys] containsObject:@"message"]) {
            complitionBlock(response, nil);
        }else if (status == 1 || status == 0) {
            complitionBlock(nil, response[@"message"]);
        }else {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK: - POST_ALL_MY_PACKAGES
- (void)postMyPackagesListWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock {
    [self postWithBodyResponseFromURL:urlStr withParameters:params withComplitionBlock:^(id response, NSError *error) {
        NSInteger status = [response[@"status"] integerValue];
        if (status == 1 && [[response allKeys] containsObject:@"data"] && [response[@"data"] count] > 0) {
            complitionBlock(response[@"data"], nil);
        }else if (status == 1 || status == 0) {
            complitionBlock(nil, @"No Details found");
        }else {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK: - POST_MY_TESTIMONIALS
- (void)postAddMyTestimonialListWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock {
    [self postWithBodyResponseFromURL:urlStr withParameters:params withComplitionBlock:^(id response, NSError *error) {
        NSInteger status = [response[@"status"] integerValue];
        if (status == 1 && [[response allKeys] containsObject:@"message"]) {
            complitionBlock(response, nil);
        }else if (status == 1 || status == 0) {
            complitionBlock(nil, response[@"message"]);
        }else {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}
//MARK: - GET_BRANCHS
- (void)getBranchsListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock{
    [self getResponseFromUrl:url withParameters:nil withComplitionBlock:^(id response, NSError *error) {
        if (response) {
            complitionBlock(response[@"data"], nil);
        }else{
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

//MARK: - GET_SAMPLET_TEMPLATES
- (void)getSampleTemplatesListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock{
    [self getResponseFromUrl:url withParameters:nil withComplitionBlock:^(id response, NSError *error) {
        if (response) {
            complitionBlock(response[@"data"], nil);
        }else{
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}


//MARK: - POST_COUPON_VERIFY
- (void)couponVerifyWithUrlStr:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock {
    [self postResponseFromUrl:urlStr withParameters:params withComplitionBlock:^(id response, NSError *error) {
        NSInteger status = [response[@"status"] integerValue];
        if (status == 1) {
            complitionBlock(response, nil);
        }else if ((status == 1 || status == 0) && [response count] > 0) {
            complitionBlock(nil, response[@"message"]);
        }else {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

@end
