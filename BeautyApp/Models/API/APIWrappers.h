//
//  APIWrappers.h
//  Rxpress
//
//  Created by Rxpress on 19/02/17.
//  Copyright © 2017 rxpress.com. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>





@interface APIWrappers : AFHTTPSessionManager


+ (APIWrappers *)sharedAPIWrapperInstance;

//MARK:LOGIN
- (void)userLoginServiceWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock;

//MARK:SIGN_UP
- (void)userSignUpServiceWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock;

//MARK:SIGN_UP_OTP
- (void)userOTPServiceWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock;

//MARK: - GetPromotionsList
- (void)getPromotionsListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock;

//MARK: - GET_ALL_TESTIMONIALS
- (void)getTestimonialsWithURLString:(NSString *)url withParametes:(NSDictionary *)params withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock;

//MARK: - TESTIMONIAL_LIKE
- (void)userLikeTestimonialWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock;

//MARK: - GET_ALL_BEAUTY_TIPS
- (void)getTipsWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock;

//MARK: - GET_ALL_CATEGORY_LIST
- (void)getCategoryListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock;

//MARK: - GET_SUB_CATEGOTY_WITH_CATEGORY_ID
- (void)getSubCategoryListWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock;

//MARK: - GET_ALL_PRODUCT_LIST
- (void)getAllProductListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock;

//MARK: - GET_ALL_PACKAGES
- (void)getAllPackagesListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock;

//MARK: - POST_ALL_MY_PACKAGES
- (void)postMyPackagesListWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock;

//MARK: - GET_ALL_MEMBERSHIP
- (void)getAllMemberShipListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock;

//MARK:UPDATE_USER_PROFILE
- (void)updateUserProfileServiceWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock;

//MARK: - POST_MY_TESTIMONIALS
- (void)postAddMyTestimonialListWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock;


//MARK: - GET_BRANCHS
- (void)getBranchsListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock;

//MARK: - GET_SAMPLET_TEMPLATES
- (void)getSampleTemplatesListWithURLString:(NSString *)url withComplitionBlock:(void(^)(NSArray *response, NSString *errorStr))complitionBlock;


//MARK: - POST_BOOKING_CONFIRMATION
- (void)confirmBookingWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock;

//MARK: - POST_PAYMENT_CONFIRMATION
- (void)confirmBookingPayementWithURLString:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock;


//MARK: - POST_COUPON_VERIFY
- (void)couponVerifyWithUrlStr:(NSString *)urlStr withParameters:(NSDictionary *)params withComplitionBlock:(void(^)(NSDictionary *response, NSString *errorStr))complitionBlock;
@end

