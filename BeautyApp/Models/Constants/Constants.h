//
//  Constants.h
//  North
//
//  Created by way2online on 28/12/16.
//  Copyright © 2016 Way2Online. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"


#define MOBILE_NUMBER_ERROR @"Entered Mobile Number is Invalid. Please enter validate mobile number"

#define SHOW_PROMOTION_FIRST_TIME @"SHOW_PROMOTION_FIRST_TIME"

#define USER_MOBILE_NUMBER @"USER_MOBILE_NUMBER"


#define BASE_URL @"http://ec2-54-88-194-105.compute-1.amazonaws.com:3000/"

#define LOGIN_URL                   BASE_URL@"login"
#define SIGNUP_URL                  BASE_URL@"signup"
#define SIGN_UP_VERIFY_URL          BASE_URL@"login_verify_otp"
#define GET_ALL_PROMOTIONS_LIST     BASE_URL@"get_promotions"
#define GET_ALL_CATEGORY            BASE_URL@"get_cat_menu_list"
#define GET_ALL_SUB_CATEGORY        BASE_URL@"get_subcat_menu_list"
#define GET_BEAUTY_TIPS             BASE_URL@"get_beauty_tips"
#define GET_All_Testimonials        BASE_URL@"get_testimonials"
#define TESTIMONIAL_LIKE_UPDATE     BASE_URL@"update_testimonials"
#define GET_ALL_PRODUCT_LIST        BASE_URL@"get_products"
#define UPDATE_USER_PROFILE         BASE_URL@"profile_update"
#define GET_ALL_PACKAGES_LIST       BASE_URL@"get_packages"
#define POST_MY_PACKAGES_LIST       BASE_URL@"get_my_packages"
#define GET_ALL_MEMBERSHIP_LIST     BASE_URL@"get_memberships"
#define POST_CONFIRM_BOOKING        BASE_URL@"order"
#define GET_BRANCHS_LIST            BASE_URL@"get_branches"
#define POST_ADD_TESTIMONIALS       BASE_URL@"addTestimonial"
#define GET_SAMPLE_TEMPLATES        BASE_URL@"getSampleTestimonial"
#define POST_PAYMENT_CONFIRMATION   BASE_URL@"payment"
#define POST_COUPON_VERIFY          BASE_URL@"coupon_verify"



#define BASE_IMAGE_URL @"http://ec2-54-88-194-105.compute-1.amazonaws.com:3000/uploads/"
@interface Constants : NSObject


+ (Constants *)sharedConstants;
- (void)storeStringValue:(NSString *)string withKey:(NSString *)key;
- (NSString *)getstoredStringValuewithKey:(NSString *)key;
- (void)storeBoolFlagValue:(BOOL)boolValue withKey:(NSString *)key;
- (BOOL)getstoredBoolFlagValuewithKey:(NSString *)key;
- (void)removeStoredValueForKey:(NSString *)key;
- (void)storeDictionary:(NSDictionary *)dictionary forKey:(NSString *)key;
- (NSDictionary *)getDictioanryForKey:(NSString *)key;
- (void)storeDateValue:(NSDate *)date forKey:(NSString *)key;
- (NSDate *)getDateForKey:(NSString *)key;
- (NSDate *)addDays:(NSInteger)days toDate:(NSDate *)originalDate;


@end
