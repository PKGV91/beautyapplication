//
//  Constants.m
//  North
//
//  Created by way2online on 28/12/16.
//  Copyright © 2016 Way2Online. All rights reserved.
//

#import "Constants.h"

@implementation Constants

static Constants *sharedContant = nil;
+ (Constants *)sharedConstants{
    if (!sharedContant) {
        sharedContant = [[Constants alloc] init];
    }
    return sharedContant;
}

- (void)storeDateValue:(NSDate *)date forKey:(NSString *)key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:key];
    [defaults setObject:date forKey:key];
    [defaults synchronize];
}
- (NSDate *)getDateForKey:(NSString *)key{
    return (NSDate *)[[NSUserDefaults standardUserDefaults] valueForKey:key];
}
- (void)removeStoredValueForKey:(NSString *)key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:key];
    [defaults synchronize];
}

- (void)storeStringValue:(NSString *)string withKey:(NSString *)key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:key];
    [defaults setObject:string forKey:key];
    [defaults synchronize];
}
- (NSString *)getstoredStringValuewithKey:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] valueForKey:key];
}
- (void)storeBoolFlagValue:(BOOL)boolValue withKey:(NSString *)key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:key];
    [defaults setBool:boolValue forKey:key];
    [defaults synchronize];
}
- (BOOL)getstoredBoolFlagValuewithKey:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}

- (void)storeDictionary:(NSDictionary *)dictionary forKey:(NSString *)key{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    [defaults removeObjectForKey:key];
    [defaults setObject:dictionary forKey:key];
    [defaults synchronize];
}

- (NSDictionary *)getDictioanryForKey:(NSString *)key{
    //    NSData *dataResultStore = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    //    return (NSDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:dataResultStore];
    return (NSDictionary *)[[NSUserDefaults standardUserDefaults] objectForKey:key];
    
}

- (NSDate *)addDays:(NSInteger)days toDate:(NSDate *)originalDate {
    NSDateComponents *components= [[NSDateComponents alloc] init];
    [components setDay:days];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [calendar dateByAddingComponents:components toDate:originalDate options:0];
}


@end
