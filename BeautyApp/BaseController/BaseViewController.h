//
//  BaseViewController.h
//  BeautyApp
//
//  Created by Pramod on 25/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIWrappers.h"
#import "NSString+Additions.h"
#import "UIView+Additions.h"

#define ROOTVIEW [[[UIApplication sharedApplication] keyWindow] rootViewController]

#define IS_IPHONE4 (([[UIScreen mainScreen] bounds].size.height-480)?NO:YES)

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

#define IS_IPHONE6 (([[UIScreen mainScreen] bounds].size.height-667)?NO:YES)

#define IS_IPHONE6P (([[UIScreen mainScreen] bounds].size.height-736)?NO:YES)

#define IS_IPHONEX (([[UIScreen mainScreen] bounds].size.height-812)?NO:YES)


@interface BaseViewController : UIViewController


- (void)showAlertWithErrorTitle:(NSString *)title andWithErrorMessage:(NSString *)errorMsg;
@end
