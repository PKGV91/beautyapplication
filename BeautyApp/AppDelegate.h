//
//  AppDelegate.h
//  BeautyApp
//
//  Created by Pramod on 25/09/17.
//  Copyright © 2017 Pramod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "LoginViewController.h"
#import "SideMenuController.h"
#import "HomeViewController.h"
#import <REFrostedViewController/REFrostedViewController.h>
#import "LoadingView.h"
#import "Constants.h"
#import "OfferViewController.h"
#import "AppointmentController.h"
#import "PostPicController.h"
#import "PayBillController.h"
#import "APIWrappers.h"




//#define Dark_Blue_Color  [UIColor colorWithRed:234.0/255.0 green:108.0/255.0 blue:102.0/255.0 alpha:1.0]
#define Dark_Blue_Color [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:0.0]
#define Dark_Green_Color  [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:0.0]

#define Light_Gray_Color  [UIColor colorWithRed:143.0/255.0 green:143.0/255.0 blue:143.0/255.0 alpha:1.0]

@import Firebase;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) LoadingView *loadingView;
@property (nonatomic, strong) NSString *uniqueIdentifier;
@property (strong, nonatomic) NSString *socailStr;
@property (nonatomic, strong) UITabBarController *tabbarController;
@property (nonatomic, strong) NSArray *promotionsArray;
@property (nonatomic, strong) NSArray *categoriesArray;
@property (nonatomic, strong) NSArray *sampleTemplatesArray;
    
+ (AppDelegate *)sharedAppDelegate;
- (void)buildSideMenuViewController:(int)index;
- (UIViewController *)createTabbarWithIndex:(int)index;
- (void)hideTabbar:(BOOL)boolValue;
- (void)showLoginController;
@end

